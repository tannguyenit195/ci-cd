server {

    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;

    server_name localhost;
    root /var/www/html;
    index index.html index.htm;

    if ( $http_x_forwarded_proto != 'https' ) {
        # return 301 https://$host$request_uri;
    }

    add_header "Access-Control-Allow-Origin"  * always;
    add_header "Access-Control-Allow-Headers" "Authorization, Origin, X-Requested-With, Content-Type, Accept" always;
    add_header "Access-Control-Allow-Methods" "GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD" always;
    
    location / {
	
		if ($request_method = OPTIONS ) {
            add_header "Access-Control-Allow-Origin"  *;
            add_header "Access-Control-Allow-Methods" "GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD";
            add_header "Access-Control-Allow-Headers" "Authorization, Origin, X-Requested-With, Content-Type, Accept, X-PINGOTHER, x-xsrf-token";
            return 200;
        }
        try_files $uri $uri/ /index.html;
    }

    location ~ /\.ht {
        deny all;
    }

    location /.well-known/acme-challenge/ {
        root /var/www/letsencrypt/;
        log_not_found off;
    }

    error_log /var/log/nginx/errors.log;
    access_log  /var/log/nginx/access.log;
}
