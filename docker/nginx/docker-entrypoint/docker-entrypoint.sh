#!/bin/sh
# Note: I've written this using sh so it works in the busybox container too

# USE the trap if you need to also do manual cleanup after the service is stopped,
#     or need to start multiple services in the one container


# start service in background here
chown -R 1000:1000 /home/log/nginx/errors.log /home/log/nginx/access.log

echo "exited $0"