import { all, fork } from 'redux-saga/effects';
import userSaga from 'containers/App/saga';
import authSaga from 'containers/Author/saga';
import homeSaga from 'containers/Home/sagas';
import usersSaga from 'containers/Users/sagas';
import usersCreateSaga from 'containers/UserCreate/sagas';
import usersSettingSaga from 'containers/UserSetting/saga';
import companySaga from 'containers/Company/saga';

export default function* rootSagas() {
  yield all(
    [
      fork(userSaga),
      fork(authSaga),
      fork(homeSaga),
      fork(usersSaga),
      fork(usersCreateSaga),
      fork(usersSettingSaga),
      fork(companySaga),
    ]
  );
}
