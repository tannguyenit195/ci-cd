import { all, fork } from 'redux-saga/effects';
import userSaga from 'containers/App/saga';
import profileSaga from 'containers/UserSetting/saga';
import ownerAuthorSaga from 'containers/Owner/OwnerAuthor/saga';
import ownerCompanySaga from 'containers/Owner/Company/sagas';
import ownerCompanyCreateSaga from 'containers/Owner/CompanyCreate/sagas';
import ownerDronesSaga from 'containers/Owner/Drones/sagas';
import ownerUserListSaga from 'containers/Owner/UserList/sagas';
import ownerUserDetailSaga from 'containers/Owner/UserDetail/sagas';
import ownerDroneDetailSaga from 'containers/Owner/DroneDetail/sagas';
import ownerCompanyDetailSaga from 'containers/Owner/CompanyDetail/sagas';
import ownerFlightSaga from 'containers/Owner/Flights/sagas';
import ownerMissionSaga from 'containers/Owner/Missions/sagas';
import ownerPaymentSaga from 'containers/Owner/PaymentHistories/sagas';
import ownerDebitSaga from 'containers/Owner/DebitInvoices/sagas';

export default function* rootSagas() {
  yield all(
    [
      fork(userSaga),
      fork(profileSaga),
      fork(ownerAuthorSaga),
      fork(ownerCompanySaga),
      fork(ownerCompanyCreateSaga),
      fork(ownerUserListSaga),
      fork(ownerUserDetailSaga),
      fork(ownerDronesSaga),
      fork(ownerDroneDetailSaga),
      fork(ownerCompanyDetailSaga),
      fork(ownerMissionSaga),
      fork(ownerPaymentSaga),
      fork(ownerDebitSaga),
      fork(ownerFlightSaga),
    ]
  );
}
