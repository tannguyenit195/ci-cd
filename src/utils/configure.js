import ApiService from './axios';
import Cookie from 'js-cookie';

ApiService.init();
const token = Cookie.get('micro_token');

if (!!token) {
  ApiService.setHeader(token);
}
