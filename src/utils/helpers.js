function formatNumber(number, style = ',') {
  return number.toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, style);
}

export {
  formatNumber
};
