export const PAGE_NEIGHBOURS = 2;
export const PAGE_LIMIT = 10;

export const FIELDS = {
  USERS: 'email,id,first_name,last_name,last_login_at,phone,roles{display_name}',
  COMPANY: 'name,type,phone'
};

export const INVOICES_TYPE = {
  PAYMENT: 'PAYMENT',
  DEBIT: 'DEBIT',
}

export const LANGUAGES = [
  {
    id: 'en',
    text: 'ENGLISH'
  },
  {
    id: 'ja',
    text: '日本語'
  },
  {
    id: 'ko',
    text: '한국어'
  }
];

const TIMEZONES = [
  {
    groupId: 'Africa',
    times: [
      {
        id: 'Africa/Abidjan',
        text: '(GMT/UTC +0:00) Abidjan'
      },
      {
        id: 'Africa/Accra',
        text: '(GMT/UTC +0:00) Accra'
      },
      {
        id: 'Africa/Addis_Ababa',
        text: '(GMT/UTC +3:00) Addis Ababa'
      },
      {
        id: 'Africa/Algiers',
        text: '(GMT/UTC +1:00) Algiers'
      },
      {
        id: 'Africa/Asmara',
        text: '(GMT/UTC +3:00) Asmara'
      },
      {
        id: 'Africa/Bamako',
        text: '(GMT/UTC +0:00) Bamako'
      },
      {
        id: 'Africa/Bangui',
        text: '(GMT/UTC +1:00) Bangui'
      },
      {
        id: 'Africa/Banjul',
        text: '(GMT/UTC +0:00) Banjul'
      },
      {
        id: 'Africa/Bissau',
        text: '(GMT/UTC +0:00) Bissau'
      },
      {
        id: 'Africa/Blantyre',
        text: '(GMT/UTC +2:00) Blantyre'
      },
      {
        id: 'Africa/Brazzaville',
        text: '(GMT/UTC +1:00) Brazzaville'
      },
      {
        id: 'Africa/Bujumbura',
        text: '(GMT/UTC +2:00) Bujumbura'
      },
      {
        id: 'Africa/Cairo',
        text: '(GMT/UTC +2:00) Cairo'
      },
      {
        id: 'Africa/Casablanca',
        text: '(GMT/UTC +0:00) Casablanca'
      },
      {
        id: 'Africa/Ceuta',
        text: '(GMT/UTC +1:00) Ceuta'
      },
      {
        id: 'Africa/Conakry',
        text: '(GMT/UTC +0:00) Conakry'
      },
      {
        id: 'Africa/Dakar',
        text: '(GMT/UTC +0:00) Dakar'
      },
      {
        id: 'Africa/Dar_es_Salaam',
        text: '(GMT/UTC +3:00) Dar es Salaam'
      },
      {
        id: 'Africa/Djibouti',
        text: '(GMT/UTC +3:00) Djibouti'
      },
      {
        id: 'Africa/Douala',
        text: '(GMT/UTC +1:00) Douala'
      },
      {
        id: 'Africa/El_Aaiun',
        text: '(GMT/UTC +0:00) El Aaiun'
      },
      {
        id: 'Africa/Freetown',
        text: '(GMT/UTC +0:00) Freetown'
      },
      {
        id: 'Africa/Gaborone',
        text: '(GMT/UTC +2:00) Gaborone'
      },
      {
        id: 'Africa/Harare',
        text: '(GMT/UTC +2:00) Harare'
      },
      {
        id: 'Africa/Johannesburg',
        text: '(GMT/UTC +2:00) Johannesburg'
      },
      {
        id: 'Africa/Juba',
        text: '(GMT/UTC +3:00) Juba'
      },
      {
        id: 'Africa/Kampala',
        text: '(GMT/UTC +3:00) Kampala'
      },
      {
        id: 'Africa/Khartoum',
        text: '(GMT/UTC +3:00) Khartoum'
      },
      {
        id: 'Africa/Kigali',
        text: '(GMT/UTC +2:00) Kigali'
      },
      {
        id: 'Africa/Kinshasa',
        text: '(GMT/UTC +1:00) Kinshasa'
      },
      {
        id: 'Africa/Lagos',
        text: '(GMT/UTC +1:00) Lagos'
      },
      {
        id: 'Africa/Libreville',
        text: '(GMT/UTC +1:00) Libreville'
      },
      {
        id: 'Africa/Lome',
        text: '(GMT/UTC +0:00) Lome'
      },
      {
        id: 'Africa/Luanda',
        text: '(GMT/UTC +1:00) Luanda'
      },
      {
        id: 'Africa/Lubumbashi',
        text: '(GMT/UTC +2:00) Lubumbashi'
      },
      {
        id: 'Africa/Lusaka',
        text: '(GMT/UTC +2:00) Lusaka'
      },
      {
        id: 'Africa/Malabo',
        text: '(GMT/UTC +1:00) Malabo'
      },
      {
        id: 'Africa/Maputo',
        text: '(GMT/UTC +2:00) Maputo'
      },
      {
        id: 'Africa/Maseru',
        text: '(GMT/UTC +2:00) Maseru'
      },
      {
        id: 'Africa/Mbabane',
        text: '(GMT/UTC +2:00) Mbabane'
      },
      {
        id: 'Africa/Mogadishu',
        text: '(GMT/UTC +3:00) Mogadishu'
      },
      {
        id: 'Africa/Monrovia',
        text: '(GMT/UTC +0:00) Monrovia'
      },
      {
        id: 'Africa/Nairobi',
        text: '(GMT/UTC +3:00) Nairobi'
      },
      {
        id: 'Africa/Ndjamena',
        text: '(GMT/UTC +1:00) Ndjamena'
      },
      {
        id: 'Africa/Niamey',
        text: '(GMT/UTC +1:00) Niamey'
      },
      {
        id: 'Africa/Nouakchott',
        text: '(GMT/UTC +0:00) Nouakchott'
      },
      {
        id: 'Africa/Ouagadougou',
        text: '(GMT/UTC +0:00) Ouagadougou'
      },
      {
        id: 'Africa/Porto-Novo',
        text: '(GMT/UTC +1:00) Porto-Novo'
      },
      {
        id: 'Africa/Sao_Tome',
        text: '(GMT/UTC +0:00) Sao Tome'
      },
      {
        id: 'Africa/Tripoli',
        text: '(GMT/UTC +2:00) Tripoli'
      },
      {
        id: 'Africa/Tunis',
        text: '(GMT/UTC +1:00) Tunis'
      },
      {
        id: 'Africa/Windhoek',
        text: '(GMT/UTC +2:00) Windhoek'
      }
    ]
  },
  {
    groupId: 'America',
    times: [
      {
        id: 'America/Adak',
        text: '( GMT/UTC -10:00) Adak'
      },
      {
        id: 'America/Anchorage',
        text: '( GMT/UTC -9:00) Anchorage'
      },
      {
        id: 'America/Anguilla',
        text: '( GMT/UTC -4:00) Anguilla'
      },
      {
        id: 'America/Antigua',
        text: '( GMT/UTC -4:00) Antigua'
      },
      {
        id: 'America/Araguaina',
        text: '( GMT/UTC -3:00) Araguaina'
      },
      {
        id: 'America/Argentina/Buenos_Aires',
        text: '( GMT/UTC -3:00) Argentina/Buenos Aires'
      },
      {
        id: 'America/Argentina/Catamarca',
        text: '( GMT/UTC -3:00) Argentina/Catamarca'
      },
      {
        id: 'America/Argentina/Cordoba',
        text: '( GMT/UTC -3:00) Argentina/Cordoba'
      },
      {
        id: 'America/Argentina/Jujuy',
        text: '( GMT/UTC -3:00) Argentina/Jujuy'
      },
      {
        id: 'America/Argentina/La_Rioja',
        text: '( GMT/UTC -3:00) Argentina/La Rioja'
      },
      {
        id: 'America/Argentina/Mendoza',
        text: '( GMT/UTC -3:00) Argentina/Mendoza'
      },
      {
        id: 'America/Argentina/Rio_Gallegos',
        text: '( GMT/UTC -3:00) Argentina/Rio Gallegos'
      },
      {
        id: 'America/Argentina/Salta',
        text: '( GMT/UTC -3:00) Argentina/Salta'
      },
      {
        id: 'America/Argentina/San_Juan',
        text: '( GMT/UTC -3:00) Argentina/San Juan'
      },
      {
        id: 'America/Argentina/San_Luis',
        text: '( GMT/UTC -3:00) Argentina/San Luis'
      },
      {
        id: 'America/Argentina/Tucuman',
        text: '( GMT/UTC -3:00) Argentina/Tucuman'
      },
      {
        id: 'America/Argentina/Ushuaia',
        text: '( GMT/UTC -3:00) Argentina/Ushuaia'
      },
      {
        id: 'America/Aruba',
        text: '( GMT/UTC -4:00) Aruba'
      },
      {
        id: 'America/Asuncion',
        text: '( GMT/UTC -3:00) Asuncion'
      },
      {
        id: 'America/Atikokan',
        text: '( GMT/UTC -5:00) Atikokan'
      },
      {
        id: 'America/Bahia',
        text: '( GMT/UTC -3:00) Bahia'
      },
      {
        id: 'America/Bahia_Banderas',
        text: '( GMT/UTC -6:00) Bahia Banderas'
      },
      {
        id: 'America/Barbados',
        text: '( GMT/UTC -4:00) Barbados'
      },
      {
        id: 'America/Belem',
        text: '( GMT/UTC -3:00) Belem'
      },
      {
        id: 'America/Belize',
        text: '( GMT/UTC -6:00) Belize'
      },
      {
        id: 'America/Blanc-Sablon',
        text: '( GMT/UTC -4:00) Blanc-Sablon'
      },
      {
        id: 'America/Boa_Vista',
        text: '( GMT/UTC -4:00) Boa Vista'
      },
      {
        id: 'America/Bogota',
        text: '( GMT/UTC -5:00) Bogota'
      },
      {
        id: 'America/Boise',
        text: '( GMT/UTC -7:00) Boise'
      },
      {
        id: 'America/Cambridge_Bay',
        text: '( GMT/UTC -7:00) Cambridge Bay'
      },
      {
        id: 'America/Campo_Grande',
        text: '( GMT/UTC -3:00) Campo Grande'
      },
      {
        id: 'America/Cancun',
        text: '( GMT/UTC -5:00) Cancun'
      },
      {
        id: 'America/Caracas',
        text: '( GMT/UTC -4:30) Caracas'
      },
      {
        id: 'America/Cayenne',
        text: '( GMT/UTC -3:00) Cayenne'
      },
      {
        id: 'America/Cayman',
        text: '( GMT/UTC -5:00) Cayman'
      },
      {
        id: 'America/Chicago',
        text: '( GMT/UTC -6:00) Chicago'
      },
      {
        id: 'America/Chihuahua',
        text: '( GMT/UTC -7:00) Chihuahua'
      },
      {
        id: 'America/Costa_Rica',
        text: '( GMT/UTC -6:00) Costa Rica'
      },
      {
        id: 'America/Creston',
        text: '( GMT/UTC -7:00) Creston'
      },
      {
        id: 'America/Cuiaba',
        text: '( GMT/UTC -3:00) Cuiaba'
      },
      {
        id: 'America/Curacao',
        text: '( GMT/UTC -4:00) Curacao'
      },
      {
        id: 'America/Danmarkshavn',
        text: '( GMT/UTC +0:00) Danmarkshavn'
      },
      {
        id: 'America/Dawson',
        text: '( GMT/UTC -8:00) Dawson'
      },
      {
        id: 'America/Dawson_Creek',
        text: '( GMT/UTC -7:00) Dawson Creek'
      },
      {
        id: 'America/Denver',
        text: '( GMT/UTC -7:00) Denver'
      },
      {
        id: 'America/Detroit',
        text: '( GMT/UTC -5:00) Detroit'
      },
      {
        id: 'America/Dominica',
        text: '( GMT/UTC -4:00) Dominica'
      },
      {
        id: 'America/Edmonton',
        text: '( GMT/UTC -7:00) Edmonton'
      },
      {
        id: 'America/Eirunepe',
        text: '( GMT/UTC -5:00) Eirunepe'
      },
      {
        id: 'America/El_Salvador',
        text: '( GMT/UTC -6:00) El Salvador'
      },
      {
        id: 'America/Fort_Nelson',
        text: '( GMT/UTC -7:00) Fort Nelson'
      },
      {
        id: 'America/Fortaleza',
        text: '( GMT/UTC -3:00) Fortaleza'
      },
      {
        id: 'America/Glace_Bay',
        text: '( GMT/UTC -4:00) Glace Bay'
      },
      {
        id: 'America/Godthab',
        text: '( GMT/UTC -3:00) Godthab'
      },
      {
        id: 'America/Goose_Bay',
        text: '( GMT/UTC -4:00) Goose Bay'
      },
      {
        id: 'America/Grand_Turk',
        text: '( GMT/UTC -4:00) Grand Turk'
      },
      {
        id: 'America/Grenada',
        text: '( GMT/UTC -4:00) Grenada'
      },
      {
        id: 'America/Guadeloupe',
        text: '( GMT/UTC -4:00) Guadeloupe'
      },
      {
        id: 'America/Guatemala',
        text: '( GMT/UTC -6:00) Guatemala'
      },
      {
        id: 'America/Guayaquil',
        text: '( GMT/UTC -5:00) Guayaquil'
      },
      {
        id: 'America/Guyana',
        text: '( GMT/UTC -4:00) Guyana'
      },
      {
        id: 'America/Halifax',
        text: '( GMT/UTC -4:00) Halifax'
      },
      {
        id: 'America/Havana',
        text: '( GMT/UTC -5:00) Havana'
      },
      {
        id: 'America/Hermosillo',
        text: '( GMT/UTC -7:00) Hermosillo'
      },
      {
        id: 'America/Indiana/Indianapolis',
        text: '( GMT/UTC -5:00) Indiana/Indianapolis'
      },
      {
        id: 'America/Indiana/Knox',
        text: '( GMT/UTC -6:00) Indiana/Knox'
      },
      {
        id: 'America/Indiana/Marengo',
        text: '( GMT/UTC -5:00) Indiana/Marengo'
      },
      {
        id: 'America/Indiana/Petersburg',
        text: '( GMT/UTC -5:00) Indiana/Petersburg'
      },
      {
        id: 'America/Indiana/Tell_City',
        text: '( GMT/UTC -6:00) Indiana/Tell City'
      },
      {
        id: 'America/Indiana/Vevay',
        text: '( GMT/UTC -5:00) Indiana/Vevay'
      },
      {
        id: 'America/Indiana/Vincennes',
        text: '( GMT/UTC -5:00) Indiana/Vincennes'
      },
      {
        id: 'America/Indiana/Winamac',
        text: '( GMT/UTC -5:00) Indiana/Winamac'
      },
      {
        id: 'America/Inuvik',
        text: '( GMT/UTC -7:00) Inuvik'
      },
      {
        id: 'America/Iqaluit',
        text: '( GMT/UTC -5:00) Iqaluit'
      },
      {
        id: 'America/Jamaica',
        text: '( GMT/UTC -5:00) Jamaica'
      },
      {
        id: 'America/Juneau',
        text: '( GMT/UTC -9:00) Juneau'
      },
      {
        id: 'America/Kentucky/Louisville',
        text: '( GMT/UTC -5:00) Kentucky/Louisville'
      },
      {
        id: 'America/Kentucky/Monticello',
        text: '( GMT/UTC -5:00) Kentucky/Monticello'
      },
      {
        id: 'America/Kralendijk',
        text: '( GMT/UTC -4:00) Kralendijk'
      },
      {
        id: 'America/La_Paz',
        text: '( GMT/UTC -4:00) La Paz'
      },
      {
        id: 'America/Lima',
        text: '( GMT/UTC -5:00) Lima'
      },
      {
        id: 'America/Los_Angeles',
        text: '( GMT/UTC -8:00) Los Angeles'
      },
      {
        id: 'America/Lower_Princes',
        text: '( GMT/UTC -4:00) Lower Princes'
      },
      {
        id: 'America/Maceio',
        text: '( GMT/UTC -3:00) Maceio'
      },
      {
        id: 'America/Managua',
        text: '( GMT/UTC -6:00) Managua'
      },
      {
        id: 'America/Manaus',
        text: '( GMT/UTC -4:00) Manaus'
      },
      {
        id: 'America/Marigot',
        text: '( GMT/UTC -4:00) Marigot'
      },
      {
        id: 'America/Martinique',
        text: '( GMT/UTC -4:00) Martinique'
      },
      {
        id: 'America/Matamoros',
        text: '( GMT/UTC -6:00) Matamoros'
      },
      {
        id: 'America/Mazatlan',
        text: '( GMT/UTC -7:00) Mazatlan'
      },
      {
        id: 'America/Menominee',
        text: '( GMT/UTC -6:00) Menominee'
      },
      {
        id: 'America/Merida',
        text: '( GMT/UTC -6:00) Merida'
      },
      {
        id: 'America/Metlakatla',
        text: '( GMT/UTC -9:00) Metlakatla'
      },
      {
        id: 'America/Mexico_City',
        text: '( GMT/UTC -6:00) Mexico City'
      },
      {
        id: 'America/Miquelon',
        text: '( GMT/UTC -3:00) Miquelon'
      },
      {
        id: 'America/Moncton',
        text: '( GMT/UTC -4:00) Moncton'
      },
      {
        id: 'America/Monterrey',
        text: '( GMT/UTC -6:00) Monterrey'
      },
      {
        id: 'America/Montevideo',
        text: '( GMT/UTC -3:00) Montevideo'
      },
      {
        id: 'America/Montserrat',
        text: '( GMT/UTC -4:00) Montserrat'
      },
      {
        id: 'America/Nassau',
        text: '( GMT/UTC -5:00) Nassau'
      },
      {
        id: 'America/New_York',
        text: '( GMT/UTC -5:00) New York'
      },
      {
        id: 'America/Nipigon',
        text: '( GMT/UTC -5:00) Nipigon'
      },
      {
        id: 'America/Nome',
        text: '( GMT/UTC -9:00) Nome'
      },
      {
        id: 'America/Noronha',
        text: '( GMT/UTC -2:00) Noronha'
      },
      {
        id: 'America/North_Dakota/Beulah',
        text: '( GMT/UTC -6:00) North Dakota/Beulah'
      },
      {
        id: 'America/North_Dakota/Center',
        text: '( GMT/UTC -6:00) North Dakota/Center'
      },
      {
        id: 'America/North_Dakota/New_Salem',
        text: '( GMT/UTC -6:00) North Dakota/New Salem'
      },
      {
        id: 'America/Ojinaga',
        text: '( GMT/UTC -7:00) Ojinaga'
      },
      {
        id: 'America/Panama',
        text: '( GMT/UTC -5:00) Panama'
      },
      {
        id: 'America/Pangnirtung',
        text: '( GMT/UTC -5:00) Pangnirtung'
      },
      {
        id: 'America/Paramaribo',
        text: '( GMT/UTC -3:00) Paramaribo'
      },
      {
        id: 'America/Phoenix',
        text: '( GMT/UTC -7:00) Phoenix'
      },
      {
        id: 'America/Port-au-Prince',
        text: '( GMT/UTC -5:00) Port-au-Prince'
      },
      {
        id: 'America/Port_of_Spain',
        text: '( GMT/UTC -4:00) Port of Spain'
      },
      {
        id: 'America/Porto_Velho',
        text: '( GMT/UTC -4:00) Porto Velho'
      },
      {
        id: 'America/Puerto_Rico',
        text: '( GMT/UTC -4:00) Puerto Rico'
      },
      {
        id: 'America/Rainy_River',
        text: '( GMT/UTC -6:00) Rainy River'
      },
      {
        id: 'America/Rankin_Inlet',
        text: '( GMT/UTC -6:00) Rankin Inlet'
      },
      {
        id: 'America/Recife',
        text: '( GMT/UTC -3:00) Recife'
      },
      {
        id: 'America/Regina',
        text: '( GMT/UTC -6:00) Regina'
      },
      {
        id: 'America/Resolute',
        text: '( GMT/UTC -6:00) Resolute'
      },
      {
        id: 'America/Rio_Branco',
        text: '( GMT/UTC -5:00) Rio Branco'
      },
      {
        id: 'America/Santarem',
        text: '( GMT/UTC -3:00) Santarem'
      },
      {
        id: 'America/Santiago',
        text: '( GMT/UTC -3:00) Santiago'
      },
      {
        id: 'America/Santo_Domingo',
        text: '( GMT/UTC -4:00) Santo Domingo'
      },
      {
        id: 'America/Sao_Paulo',
        text: '( GMT/UTC -2:00) Sao Paulo'
      },
      {
        id: 'America/Scoresbysund',
        text: '( GMT/UTC -1:00) Scoresbysund'
      },
      {
        id: 'America/Sitka',
        text: '( GMT/UTC -9:00) Sitka'
      },
      {
        id: 'America/St_Barthelemy',
        text: '( GMT/UTC -4:00) St. Barthelemy'
      },
      {
        id: 'America/St_Johns',
        text: '( GMT/UTC -3:30) St. Johns'
      },
      {
        id: 'America/St_Kitts',
        text: '( GMT/UTC -4:00) St. Kitts'
      },
      {
        id: 'America/St_Lucia',
        text: '( GMT/UTC -4:00) St. Lucia'
      },
      {
        id: 'America/St_Thomas',
        text: '( GMT/UTC -4:00) St. Thomas'
      },
      {
        id: 'America/St_Vincent',
        text: '( GMT/UTC -4:00) St. Vincent'
      },
      {
        id: 'America/Swift_Current',
        text: '( GMT/UTC -6:00) Swift Current'
      },
      {
        id: 'America/Tegucigalpa',
        text: '( GMT/UTC -6:00) Tegucigalpa'
      },
      {
        id: 'America/Thule',
        text: '( GMT/UTC -4:00) Thule'
      },
      {
        id: 'America/Thunder_Bay',
        text: '( GMT/UTC -5:00) Thunder Bay'
      },
      {
        id: 'America/Tijuana',
        text: '( GMT/UTC -8:00) Tijuana'
      },
      {
        id: 'America/Toronto',
        text: '( GMT/UTC -5:00) Toronto'
      },
      {
        id: 'America/Tortola',
        text: '( GMT/UTC -4:00) Tortola'
      },
      {
        id: 'America/Vancouver',
        text: '( GMT/UTC -8:00) Vancouver'
      },
      {
        id: 'America/Whitehorse',
        text: '( GMT/UTC -8:00) Whitehorse'
      },
      {
        id: 'America/Winnipeg',
        text: '( GMT/UTC -6:00) Winnipeg'
      },
      {
        id: 'America/Yakutat',
        text: '( GMT/UTC -9:00) Yakutat'
      },
      {
        id: 'America/Yellowknife',
        text: '( GMT/UTC -7:00) Yellowknife'
      }
    ]
  },
  {
    groupId: 'Antarctica',
    times: [
      {
        id: 'Antarctica/Casey',
        text: '(GMT/UTC +8:00) Casey'
      },
      {
        id: 'Antarctica/Davis',
        text: '(GMT/UTC +7:00) Davis'
      },
      {
        id: 'Antarctica/DumontDUrville',
        text: '(GMT/UTC +10:00) DumontDUrville'
      },
      {
        id: 'Antarctica/Macquarie',
        text: '(GMT/UTC +11:00) Macquarie'
      },
      {
        id: 'Antarctica/Mawson',
        text: '(GMT/UTC +5:00) Mawson'
      },
      {
        id: 'Antarctica/McMurdo',
        text: '(GMT/UTC +13:00) McMurdo'
      },
      {
        id: 'Antarctica/Palmer',
        text: '(GMT/UTC -3:00) Palmer'
      },
      {
        id: 'Antarctica/Rothera',
        text: '(GMT/UTC -3:00) Rothera'
      },
      {
        id: 'Antarctica/Syowa',
        text: '(GMT/UTC +3:00) Syowa'
      },
      {
        id: 'Antarctica/Troll',
        text: '(GMT/UTC +0:00) Troll'
      },
      {
        id: 'Antarctica/Vostok',
        text: '(GMT/UTC +6:00) Vostok'
      }
    ]
  },
  {
    groupId: 'Arctic',
    times: [
      {
        id: 'Arctic/Longyearbyen',
        text: '(GMT/UTC +1:00) Longyearbyen'
      }
    ]
  },
  {
    groupId: 'Asia',
    times: [
      {
        id: 'Asia/Aden',
        text: '(GMT/UTC +3:00) Aden'
      },
      {
        id: 'Asia/Almaty',
        text: '(GMT/UTC +6:00) Almaty'
      },
      {
        id: 'Asia/Amman',
        text: '(GMT/UTC +2:00) Amman'
      },
      {
        id: 'Asia/Anadyr',
        text: '(GMT/UTC +12:00) Anadyr'
      },
      {
        id: 'Asia/Aqtau',
        text: '(GMT/UTC +5:00) Aqtau'
      },
      {
        id: 'Asia/Aqtobe',
        text: '(GMT/UTC +5:00) Aqtobe'
      },
      {
        id: 'Asia/Ashgabat',
        text: '(GMT/UTC +5:00) Ashgabat'
      },
      {
        id: 'Asia/Baghdad',
        text: '(GMT/UTC +3:00) Baghdad'
      },
      {
        id: 'Asia/Bahrain',
        text: '(GMT/UTC +3:00) Bahrain'
      },
      {
        id: 'Asia/Baku',
        text: '(GMT/UTC +4:00) Baku'
      },
      {
        id: 'Asia/Bangkok',
        text: '(GMT/UTC +7:00) Bangkok'
      },
      {
        id: 'Asia/Barnaul',
        text: '(GMT/UTC +7:00) Barnaul'
      },
      {
        id: 'Asia/Beirut',
        text: '(GMT/UTC +2:00) Beirut'
      },
      {
        id: 'Asia/Bishkek',
        text: '(GMT/UTC +6:00) Bishkek'
      },
      {
        id: 'Asia/Brunei',
        text: '(GMT/UTC +8:00) Brunei'
      },
      {
        id: 'Asia/Chita',
        text: '(GMT/UTC +9:00) Chita'
      },
      {
        id: 'Asia/Choibalsan',
        text: '(GMT/UTC +8:00) Choibalsan'
      },
      {
        id: 'Asia/Colombo',
        text: '(GMT/UTC +5:30) Colombo'
      },
      {
        id: 'Asia/Damascus',
        text: '(GMT/UTC +2:00) Damascus'
      },
      {
        id: 'Asia/Dhaka',
        text: '(GMT/UTC +6:00) Dhaka'
      },
      {
        id: 'Asia/Dili',
        text: '(GMT/UTC +9:00) Dili'
      },
      {
        id: 'Asia/Dubai',
        text: '(GMT/UTC +4:00) Dubai'
      },
      {
        id: 'Asia/Dushanbe',
        text: '(GMT/UTC +5:00) Dushanbe'
      },
      {
        id: 'Asia/Gaza',
        text: '(GMT/UTC +2:00) Gaza'
      },
      {
        id: 'Asia/Hebron',
        text: '(GMT/UTC +2:00) Hebron'
      },
      {
        id: 'Asia/Ho_Chi_Minh',
        text: '(GMT/UTC +7:00) Ho Chi Minh'
      },
      {
        id: 'Asia/Hong_Kong',
        text: '(GMT/UTC +8:00) Hong Kong'
      },
      {
        id: 'Asia/Hovd',
        text: '(GMT/UTC +7:00) Hovd'
      },
      {
        id: 'Asia/Irkutsk',
        text: '(GMT/UTC +8:00) Irkutsk'
      },
      {
        id: 'Asia/Jakarta',
        text: '(GMT/UTC +7:00) Jakarta'
      },
      {
        id: 'Asia/Jayapura',
        text: '(GMT/UTC +9:00) Jayapura'
      },
      {
        id: 'Asia/Jerusalem',
        text: '(GMT/UTC +2:00) Jerusalem'
      },
      {
        id: 'Asia/Kabul',
        text: '(GMT/UTC +4:30) Kabul'
      },
      {
        id: 'Asia/Kamchatka',
        text: '(GMT/UTC +12:00) Kamchatka'
      },
      {
        id: 'Asia/Karachi',
        text: '(GMT/UTC +5:00) Karachi'
      },
      {
        id: 'Asia/Kathmandu',
        text: '(GMT/UTC +5:45) Kathmandu'
      },
      {
        id: 'Asia/Khandyga',
        text: '(GMT/UTC +9:00) Khandyga'
      },
      {
        id: 'Asia/Kolkata',
        text: '(GMT/UTC +5:30) Kolkata'
      },
      {
        id: 'Asia/Krasnoyarsk',
        text: '(GMT/UTC +7:00) Krasnoyarsk'
      },
      {
        id: 'Asia/Kuala_Lumpur',
        text: '(GMT/UTC +8:00) Kuala Lumpur'
      },
      {
        id: 'Asia/Kuching',
        text: '(GMT/UTC +8:00) Kuching'
      },
      {
        id: 'Asia/Kuwait',
        text: '(GMT/UTC +3:00) Kuwait'
      },
      {
        id: 'Asia/Macau',
        text: '(GMT/UTC +8:00) Macau'
      },
      {
        id: 'Asia/Magadan',
        text: '(GMT/UTC +10:00) Magadan'
      },
      {
        id: 'Asia/Makassar',
        text: '(GMT/UTC +8:00) Makassar'
      },
      {
        id: 'Asia/Manila',
        text: '(GMT/UTC +8:00) Manila'
      },
      {
        id: 'Asia/Muscat',
        text: '(GMT/UTC +4:00) Muscat'
      },
      {
        id: 'Asia/Nicosia',
        text: '(GMT/UTC +2:00) Nicosia'
      },
      {
        id: 'Asia/Novokuznetsk',
        text: '(GMT/UTC +7:00) Novokuznetsk'
      },
      {
        id: 'Asia/Novosibirsk',
        text: '(GMT/UTC +6:00) Novosibirsk'
      },
      {
        id: 'Asia/Omsk',
        text: '(GMT/UTC +6:00) Omsk'
      },
      {
        id: 'Asia/Oral',
        text: '(GMT/UTC +5:00) Oral'
      },
      {
        id: 'Asia/Phnom_Penh',
        text: '(GMT/UTC +7:00) Phnom Penh'
      },
      {
        id: 'Asia/Pontianak',
        text: '(GMT/UTC +7:00) Pontianak'
      },
      {
        id: 'Asia/Pyongyang',
        text: '(GMT/UTC +8:30) Pyongyang'
      },
      {
        id: 'Asia/Qatar',
        text: '(GMT/UTC +3:00) Qatar'
      },
      {
        id: 'Asia/Qyzylorda',
        text: '(GMT/UTC +6:00) Qyzylorda'
      },
      {
        id: 'Asia/Rangoon',
        text: '(GMT/UTC +6:30) Rangoon'
      },
      {
        id: 'Asia/Riyadh',
        text: '(GMT/UTC +3:00) Riyadh'
      },
      {
        id: 'Asia/Sakhalin',
        text: '(GMT/UTC +11:00) Sakhalin'
      },
      {
        id: 'Asia/Samarkand',
        text: '(GMT/UTC +5:00) Samarkand'
      },
      {
        id: 'Asia/Seoul',
        text: '(GMT/UTC +9:00) Seoul'
      },
      {
        id: 'Asia/Shanghai',
        text: '(GMT/UTC +8:00) Shanghai'
      },
      {
        id: 'Asia/Singapore',
        text: '(GMT/UTC +8:00) Singapore'
      },
      {
        id: 'Asia/Srednekolymsk',
        text: '(GMT/UTC +11:00) Srednekolymsk'
      },
      {
        id: 'Asia/Taipei',
        text: '(GMT/UTC +8:00) Taipei'
      },
      {
        id: 'Asia/Tashkent',
        text: '(GMT/UTC +5:00) Tashkent'
      },
      {
        id: 'Asia/Tbilisi',
        text: '(GMT/UTC +4:00) Tbilisi'
      },
      {
        id: 'Asia/Tehran',
        text: '(GMT/UTC +3:30) Tehran'
      },
      {
        id: 'Asia/Thimphu',
        text: '(GMT/UTC +6:00) Thimphu'
      },
      {
        id: 'Asia/Tokyo',
        text: '(GMT/UTC +9:00) Tokyo'
      },
      {
        id: 'Asia/Ulaanbaatar',
        text: '(GMT/UTC +8:00) Ulaanbaatar'
      },
      {
        id: 'Asia/Urumqi',
        text: '(GMT/UTC +6:00) Urumqi'
      },
      {
        id: 'Asia/Ust-Nera',
        text: '(GMT/UTC +10:00) Ust-Nera'
      },
      {
        id: 'Asia/Vientiane',
        text: '(GMT/UTC +7:00) Vientiane'
      },
      {
        id: 'Asia/Vladivostok',
        text: '(GMT/UTC +10:00) Vladivostok'
      },
      {
        id: 'Asia/Yakutsk',
        text: '(GMT/UTC +9:00) Yakutsk'
      },
      {
        id: 'Asia/Yekaterinburg',
        text: '(GMT/UTC +5:00) Yekaterinburg'
      },
      {
        id: 'Asia/Yerevan',
        text: '(GMT/UTC +4:00) Yerevan'
      }
    ]
  },
  {
    groupId: 'Atlantic',
    times: [
      {
        id: 'Atlantic/Azores',
        text: '(GMT/UTC -1:00) Azores'
      },
      {
        id: 'Atlantic/Bermuda',
        text: '(GMT/UTC -4:00) Bermuda'
      },
      {
        id: 'Atlantic/Canary',
        text: '(GMT/UTC +1:00) Canary'
      },
      {
        id: 'Atlantic/Cape_Verde',
        text: '(GMT/UTC -1:00) Cape Verde'
      },
      {
        id: 'Atlantic/Faroe',
        text: '(GMT/UTC +0:00) Faroe'
      },
      {
        id: 'Atlantic/Madeira',
        text: '(GMT/UTC +0:00) Madeira'
      },
      {
        id: 'Atlantic/Reykjavik',
        text: '(GMT/UTC +0:00) Reykjavik'
      },
      {
        id: 'Atlantic/South_Georgia',
        text: '(GMT/UTC -2:00) South Georgia'
      },
      {
        id: 'Atlantic/St_Helena',
        text: '(GMT/UTC +0:00) St. Helena'
      },
      {
        id: 'Atlantic/Stanley',
        text: '(GMT/UTC -3:00) Stanley'
      }
    ]
  },
  {
    groupId: 'Australia',
    times: [
      {
        id: 'Australia/Adelaide',
        text: '(GMT/UTC +10:30) Adelaide'
      },
      {
        id: 'Australia/Brisbane',
        text: '(GMT/UTC +10:00) Brisbane'
      },
      {
        id: 'Australia/Broken_Hill',
        text: '(GMT/UTC +10:30) Broken Hill'
      },
      {
        id: 'Australia/Currie',
        text: '(GMT/UTC +11:00) Currie'
      },
      {
        id: 'Australia/Darwin',
        text: '(GMT/UTC +9:30) Darwin'
      },
      {
        id: 'Australia/Eucla',
        text: '(GMT/UTC +8:45) Eucla'
      },
      {
        id: 'Australia/Hobart',
        text: '(GMT/UTC +11:00) Hobart'
      },
      {
        id: 'Australia/Lindeman',
        text: '(GMT/UTC +10:00) Lindeman'
      },
      {
        id: 'Australia/Lord_Howe',
        text: '(GMT/UTC +11:00) Lord Howe'
      },
      {
        id: 'Australia/Melbourne',
        text: '(GMT/UTC +11:00) Melbourne'
      },
      {
        id: 'Australia/Perth',
        text: '(GMT/UTC +8:00) Perth'
      },
      {
        id: 'Australia/Sydney',
        text: '(GMT/UTC +11:00) Sydney'
      }
    ]
  },
  {
    groupId: 'Europe',
    times: [
      {
        id: 'Europe/Amsterdam',
        text: '(GMT/UTC +1:00) Amsterdam'
      },
      {
        id: 'Europe/Andorra',
        text: '(GMT/UTC +1:00) Andorra'
      },
      {
        id: 'Europe/Astrakhan',
        text: '(GMT/UTC +4:00) Astrakhan'
      },
      {
        id: 'Europe/Athens',
        text: '(GMT/UTC +2:00) Athens'
      },
      {
        id: 'Europe/Belgrade',
        text: '(GMT/UTC +1:00) Belgrade'
      },
      {
        id: 'Europe/Berlin',
        text: '(GMT/UTC +1:00) Berlin'
      },
      {
        id: 'Europe/Bratislava',
        text: '(GMT/UTC +1:00) Bratislava'
      },
      {
        id: 'Europe/Brussels',
        text: '(GMT/UTC +1:00) Brussels'
      },
      {
        id: 'Europe/Bucharest',
        text: '(GMT/UTC +2:00) Bucharest'
      },
      {
        id: 'Europe/Budapest',
        text: '(GMT/UTC +1:00) Budapest'
      },
      {
        id: 'Europe/Busingen',
        text: '(GMT/UTC +1:00) Busingen'
      },
      {
        id: 'Europe/Chisinau',
        text: '(GMT/UTC +2:00) Chisinau'
      },
      {
        id: 'Europe/Copenhagen',
        text: '(GMT/UTC +1:00) Copenhagen'
      },
      {
        id: 'Europe/Dublin',
        text: '(GMT/UTC +0:00) Dublin'
      },
      {
        id: 'Europe/Gibraltar',
        text: '(GMT/UTC +1:00) Gibraltar'
      },
      {
        id: 'Europe/Guernsey',
        text: '(GMT/UTC +0:00) Guernsey'
      },
      {
        id: 'Europe/Helsinki',
        text: '(GMT/UTC +2:00) Helsinki'
      },
      {
        id: 'Europe/Isle_of_Man',
        text: '(GMT/UTC +0:00) Isle of Man'
      },
      {
        id: 'Europe/Istanbul',
        text: '(GMT/UTC +2:00) Istanbul'
      },
      {
        id: 'Europe/Jersey',
        text: '(GMT/UTC +0:00) Jersey'
      },
      {
        id: 'Europe/Kaliningrad',
        text: '(GMT/UTC +2:00) Kaliningrad'
      },
      {
        id: 'Europe/Kiev',
        text: '(GMT/UTC +2:00) Kiev'
      },
      {
        id: 'Europe/Lisbon',
        text: '(GMT/UTC +0:00) Lisbon'
      },
      {
        id: 'Europe/Ljubljana',
        text: '(GMT/UTC +1:00) Ljubljana'
      },
      {
        id: 'Europe/London',
        text: '(GMT/UTC +0:00) London'
      },
      {
        id: 'Europe/Luxembourg',
        text: '(GMT/UTC +1:00) Luxembourg'
      },
      {
        id: 'Europe/Madrid',
        text: '(GMT/UTC +1:00) Madrid'
      },
      {
        id: 'Europe/Malta',
        text: '(GMT/UTC +1:00) Malta'
      },
      {
        id: 'Europe/Mariehamn',
        text: '(GMT/UTC +2:00) Mariehamn'
      },
      {
        id: 'Europe/Minsk',
        text: '(GMT/UTC +3:00) Minsk'
      },
      {
        id: 'Europe/Monaco',
        text: '(GMT/UTC +1:00) Monaco'
      },
      {
        id: 'Europe/Moscow',
        text: '(GMT/UTC +3:00) Moscow'
      },
      {
        id: 'Europe/Oslo',
        text: '(GMT/UTC +1:00) Oslo'
      },
      {
        id: 'Europe/Paris',
        text: '(GMT/UTC +1:00) Paris'
      },
      {
        id: 'Europe/Podgorica',
        text: '(GMT/UTC +1:00) Podgorica'
      },
      {
        id: 'Europe/Prague',
        text: '(GMT/UTC +1:00) Prague'
      },
      {
        id: 'Europe/Riga',
        text: '(GMT/UTC +2:00) Riga'
      },
      {
        id: 'Europe/Rome',
        text: '(GMT/UTC +1:00) Rome'
      },
      {
        id: 'Europe/Samara',
        text: '(GMT/UTC +4:00) Samara'
      },
      {
        id: 'Europe/San_Marino',
        text: '(GMT/UTC +1:00) San Marino'
      },
      {
        id: 'Europe/Sarajevo',
        text: '(GMT/UTC +1:00) Sarajevo'
      },
      {
        id: 'Europe/Simferopol',
        text: '(GMT/UTC +3:00) Simferopol'
      },
      {
        id: 'Europe/Skopje',
        text: '(GMT/UTC +1:00) Skopje'
      },
      {
        id: 'Europe/Sofia',
        text: '(GMT/UTC +2:00) Sofia'
      },
      {
        id: 'Europe/Stockholm',
        text: '(GMT/UTC +1:00) Stockholm'
      },
      {
        id: 'Europe/Tallinn',
        text: '(GMT/UTC +2:00) Tallinn'
      },
      {
        id: 'Europe/Tirane',
        text: '(GMT/UTC +1:00) Tirane'
      },
      {
        id: 'Europe/Ulyanovsk',
        text: '(GMT/UTC +4:00) Ulyanovsk'
      },
      {
        id: 'Europe/Uzhgorod',
        text: '(GMT/UTC +2:00) Uzhgorod'
      },
      {
        id: 'Europe/Vaduz',
        text: '(GMT/UTC +1:00) Vaduz'
      },
      {
        id: 'Europe/Vatican',
        text: '(GMT/UTC +1:00) Vatican'
      },
      {
        id: 'Europe/Vienna',
        text: '(GMT/UTC +1:00) Vienna'
      },
      {
        id: 'Europe/Vilnius',
        text: '(GMT/UTC +2:00) Vilnius'
      },
      {
        id: 'Europe/Volgograd',
        text: '(GMT/UTC +3:00) Volgograd'
      },
      {
        id: 'Europe/Warsaw',
        text: '(GMT/UTC +1:00) Warsaw'
      },
      {
        id: 'Europe/Zagreb',
        text: '(GMT/UTC +1:00) Zagreb'
      },
      {
        id: 'Europe/Zaporozhye',
        text: '(GMT/UTC +2:00) Zaporozhye'
      },
      {
        id: 'Europe/Zurich',
        text: '(GMT/UTC +1:00) Zurich'
      }
    ]
  },
  {
    groupId: 'Indian',
    times: [
      {
        id: 'Indian/Antananarivo',
        text: '(GMT/UTC +3:00) Antananarivo'
      },
      {
        id: 'Indian/Chagos',
        text: '(GMT/UTC +6:00) Chagos'
      },
      {
        id: 'Indian/Christmas',
        text: '(GMT/UTC +7:00) Christmas'
      },
      {
        id: 'Indian/Cocos',
        text: '(GMT/UTC +6:30) Cocos'
      },
      {
        id: 'Indian/Comoro',
        text: '(GMT/UTC +3:00) Comoro'
      },
      {
        id: 'Indian/Kerguelen',
        text: '(GMT/UTC +5:00) Kerguelen'
      },
      {
        id: 'Indian/Mahe',
        text: '(GMT/UTC +4:00) Mahe'
      },
      {
        id: 'Indian/Maldives',
        text: '(GMT/UTC +5:00) Maldives'
      },
      {
        id: 'Indian/Mauritius',
        text: '(GMT/UTC +4:00) Mauritius'
      },
      {
        id: 'Indian/Mayotte',
        text: '(GMT/UTC +3:00) Mayotte'
      },
      {
        id: 'Indian/Reunion',
        text: '(GMT/UTC +4:00) Reunion'
      }
    ]
  },
  {
    groupId: 'Pacific',
    times: [
      {
        id: 'Pacific/Apia',
        text: '(GMT/UTC +14:00) Apia'
      },
      {
        id: 'Pacific/Auckland',
        text: '(GMT/UTC +13:00) Auckland'
      },
      {
        id: 'Pacific/Bougainville',
        text: '(GMT/UTC +11:00) Bougainville'
      },
      {
        id: 'Pacific/Chatham',
        text: '(GMT/UTC +13:45) Chatham'
      },
      {
        id: 'Pacific/Chuuk',
        text: '(GMT/UTC +10:00) Chuuk'
      },
      {
        id: 'Pacific/Easter',
        text: '(GMT/UTC -5:00) Easter'
      },
      {
        id: 'Pacific/Efate',
        text: '(GMT/UTC +11:00) Efate'
      },
      {
        id: 'Pacific/Enderbury',
        text: '(GMT/UTC +13:00) Enderbury'
      },
      {
        id: 'Pacific/Fakaofo',
        text: '(GMT/UTC +13:00) Fakaofo'
      },
      {
        id: 'Pacific/Fiji',
        text: '(GMT/UTC +12:00) Fiji'
      },
      {
        id: 'Pacific/Funafuti',
        text: '(GMT/UTC +12:00) Funafuti'
      },
      {
        id: 'Pacific/Galapagos',
        text: '(GMT/UTC -6:00) Galapagos'
      },
      {
        id: 'Pacific/Gambier',
        text: '(GMT/UTC -9:00) Gambier'
      },
      {
        id: 'Pacific/Guadalcanal',
        text: '(GMT/UTC +11:00) Guadalcanal'
      },
      {
        id: 'Pacific/Guam',
        text: '(GMT/UTC +10:00) Guam'
      },
      {
        id: 'Pacific/Honolulu',
        text: '(GMT/UTC -10:00) Honolulu'
      },
      {
        id: 'Pacific/Johnston',
        text: '(GMT/UTC -10:00) Johnston'
      },
      {
        id: 'Pacific/Kiritimati',
        text: '(GMT/UTC +14:00) Kiritimati'
      },
      {
        id: 'Pacific/Kosrae',
        text: '(GMT/UTC +11:00) Kosrae'
      },
      {
        id: 'Pacific/Kwajalein',
        text: '(GMT/UTC +12:00) Kwajalein'
      },
      {
        id: 'Pacific/Majuro',
        text: '(GMT/UTC +12:00) Majuro'
      },
      {
        id: 'Pacific/Marquesas',
        text: '(GMT/UTC -9:30) Marquesas'
      },
      {
        id: 'Pacific/Midway',
        text: '(GMT/UTC -11:00) Midway'
      },
      {
        id: 'Pacific/Nauru',
        text: '(GMT/UTC +12:00) Nauru'
      },
      {
        id: 'Pacific/Niue',
        text: '(GMT/UTC -11:00) Niue'
      },
      {
        id: 'Pacific/Norfolk',
        text: '(GMT/UTC +11:00) Norfolk'
      },
      {
        id: 'Pacific/Noumea',
        text: '(GMT/UTC +11:00) Noumea'
      },
      {
        id: 'Pacific/Pago_Pago',
        text: '(GMT/UTC -11:00) Pago Pago'
      },
      {
        id: 'Pacific/Palau',
        text: '(GMT/UTC +9:00) Palau'
      },
      {
        id: 'Pacific/Pitcairn',
        text: '(GMT/UTC -8:00) Pitcairn'
      },
      {
        id: 'Pacific/Pohnpei',
        text: '(GMT/UTC +11:00) Pohnpei'
      },
      {
        id: 'Pacific/Port_Moresby',
        text: '(GMT/UTC +10:00) Port Moresby'
      },
      {
        id: 'Pacific/Rarotonga',
        text: '(GMT/UTC -10:00) Rarotonga'
      },
      {
        id: 'Pacific/Saipan',
        text: '(GMT/UTC +10:00) Saipan'
      },
      {
        id: 'Pacific/Tahiti',
        text: '(GMT/UTC -10:00) Tahiti'
      },
      {
        id: 'Pacific/Tarawa',
        text: '(GMT/UTC +12:00) Tarawa'
      },
      {
        id: 'Pacific/Tongatapu',
        text: '(GMT/UTC +13:00) Tongatapu'
      },
      {
        id: 'Pacific/Wake',
        text: '(GMT/UTC +12:00) Wake'
      },
      {
        id: 'Pacific/Wallis',
        text: '(GMT/UTC +12:00) Wallis'
      }
    ]
  }
];

const TIMES = [];

TIMEZONES.map(item => TIMES.push(...item.times));

export const TIME_ZONE_ALL = TIMES;

export const ROLE_TYPE = {
  ADMIN: 'ADMIN',
  OWNER: 'OWNER'
}
