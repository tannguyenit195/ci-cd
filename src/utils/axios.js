import axios from 'axios';
const baseURLAPI = `${process.env.API_URL}/${process.env.API_VERSION}`;

const ApiService = {
  init() {
    axios.defaults.baseURL = baseURLAPI;
  },

  async setHeader(userToken = null) {
    axios.defaults.headers.common.Authorization = `Bearer ${userToken}`;
  },

  get(resource, config = {}) {
    return axios.get(resource, config);
  },

  post(resource, params) {
    return axios.post(`${resource}`, params);
  },

  update(resource, slug = '', params) {
    return axios.put(`${resource}/${slug}`, params);
  },

  put(resource, params) {
    return axios.put(`${resource}`, params);
  },

  patch(resource, params) {
    return axios.patch(`${resource}`, params);
  },

  delete(resource, config = {}) {
    return axios.delete(resource, config).catch(error => {
      throw new Error(`[API] ${error}`);
    });
  }
};

export default ApiService;
