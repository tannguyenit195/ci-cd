import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';

import OwnerRoutes from 'routes/owner';

export default function OwnerApp() {
  return (
    <Fragment>
      <Helmet titleTemplate="%s - An ....." defaultTitle="Terra Drone">
        <link rel="icon" href="https://service-dev.terra-utm.com/build/img/favicon/favicon.ico" sizes="32x32"/>
      </Helmet>
      <OwnerRoutes/>
    </Fragment>
  );
}
