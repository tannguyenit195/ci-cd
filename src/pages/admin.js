import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';

import AdminRoutes from 'routes/admin';

export default function AdminApp() {
  return (
    <Fragment>
      <Helmet titleTemplate="%s - An ....." defaultTitle="Terra Drone">
        <link rel="icon" href="https://service-dev.terra-utm.com/build/img/favicon/favicon.ico" sizes="32x32"/>
      </Helmet>
      <AdminRoutes/>
    </Fragment>
  );
}
