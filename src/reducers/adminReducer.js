/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';
import { LOCATION_CHANGE } from 'react-router-redux';
import globalReducer from 'containers/App/reducers';
import languageProviderReducer from 'containers/LanguageProvider/reducers';
import usersReducer from 'containers/Users/reducers';
import companyReducer from 'containers/Company/reducer';
import userCreateReducer from 'containers/UserCreate/reducers';

// Initial routing state
const routeInitialState = {
  location: null
};

/**
 * Merge route into the global application state
 */
export function routeReducer(state = routeInitialState, action) {
  switch (action.type) {
    /* istanbul ignore next */
    case LOCATION_CHANGE:
      return {
        ...state,
        location: action.payload
      };
    default:
      return state;
  }
}

/**
 * Creates the main reducer with the dynamically injected ones
 */
export default function createReducer() {
  return combineReducers({
    route: routeReducer,
    global: globalReducer,
    language: languageProviderReducer,
    users: usersReducer,
    company: companyReducer,
    userCreate: userCreateReducer,
  });
}
