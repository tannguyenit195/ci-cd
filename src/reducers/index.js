/**
 * Combine all reducers in this file and export the combined reducers.
 */

import { combineReducers } from 'redux';
import { LOCATION_CHANGE } from 'react-router-redux';
import globalReducer from 'containers/App/reducers';
import languageProviderReducer from 'containers/LanguageProvider/reducers';
import usersReducer from 'containers/Users/reducers';
import companyReducer from 'containers/Company/reducer';
import userCreateReducer from 'containers/UserCreate/reducers';

import ownerCompanyReducer from 'containers/Owner/Company/reducers';
import ownerCompanyDetailReducer from 'containers/Owner/CompanyDetail/reducers';
import ownerDronesReducer from 'containers/Owner/Drones/reducers';
import ownerDroneDetailReducer from 'containers/Owner/DroneDetail/reducers';
import ownerUserListReducer from 'containers/Owner/UserList/reducers';
import ownerUserDetailReducer from 'containers/Owner/UserDetail/reducers';
import ownerPaymentHistoriesReducer from 'containers/Owner/PaymentHistories/reducers';
import ownerDebitInvoicesReducer from 'containers/Owner/DebitInvoices/reducers';
import ownerMissionReducer from 'containers/Owner/Missions/reducers';
import ownerFlightReducer from 'containers/Owner/Flights/reducers';


// Initial routing state
const routeInitialState = {
  location: null
};

/**
 * Merge route into the global application state
 */
export function routeReducer(state = routeInitialState, action) {
  switch (action.type) {
    /* istanbul ignore next */
    case LOCATION_CHANGE:
      return {
        ...state,
        location: action.payload
      };
    default:
      return state;
  }
}

/**
 * Creates the main reducer with the dynamically injected ones
 */
export default function createReducer() {
  return combineReducers({
    route: routeReducer,
    global: globalReducer,
    language: languageProviderReducer,
    users: usersReducer,
    company: companyReducer,
    userCreate: userCreateReducer,
    drones: ownerDronesReducer,
    ownerCompany: ownerCompanyReducer,
    droneDetail: ownerDroneDetailReducer,
    userList: ownerUserListReducer,
    userDetail: ownerUserDetailReducer,
    companyDetail: ownerCompanyDetailReducer,
    paymentHistories: ownerPaymentHistoriesReducer,
    debitInvoices: ownerDebitInvoicesReducer,
    missions: ownerMissionReducer,
    flights: ownerFlightReducer,
  });
}
