import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Cookie from 'js-cookie';

import { MasterLayout } from 'layouts';
import Header from 'components/Header';
import Footer from 'components/Footer';
import Sidebar from 'components/SidebarOwner';
import { getUserInfoRequest } from 'containers/App/action';

class OwnerLayout extends Component {
  static propTypes = {
    children: PropTypes.node,
    user: PropTypes.any,
    history: PropTypes.any,
    getUserInfoRequest: PropTypes.func
  };

  componentDidMount() {
    const { getUserInfoRequest } = this.props;

    if (Cookie.get('micro_token')) {
      getUserInfoRequest();
    }
  }

  render() {
    const { history, children } = this.props;
    const childrenWithProps = React.Children.map(children, (child) => React.cloneElement(child, {}));
    return (
      <MasterLayout>
        <Header history={history}/>
        <Sidebar history={history}/>
        <div className="content-wrapper">{childrenWithProps}</div>
        <Footer/>
      </MasterLayout>
    );
  }
}

const mapStateToProps = ({ global: { user } }) => ({ user });
const mapDispatchToProps = dispatch => ({
  getUserInfoRequest: () => dispatch(getUserInfoRequest())
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default withConnect(OwnerLayout);
