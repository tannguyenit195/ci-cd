import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { MasterLayout } from 'layouts';
import AuthHeader from 'components/Header/AuthHeader';
import 'styles/scss/auth-layout.scss';

export default class AuthLayout extends Component {
  static propTypes = {
    type: PropTypes.string,
    children: PropTypes.node
  };

  render() {
    const { children, type } = this.props;
    const childrenWithProps = React.Children.map(children, child => React.cloneElement(child, {}));
    return (
      <MasterLayout className={type}>
        <AuthHeader type={type}/>
        <div className={`auth-content ${type}`}>{childrenWithProps}</div>
      </MasterLayout>
    );
  }
}
