import {
  GET_DRONE_LIST_SUCCESS
} from '../constants';

const dronesState = {
  current_page: 0,
  data: [],
  total: 0,
  per_page: 10
};

function dronesReducer(state = dronesState, action) {
  switch (action.type) {
    case GET_DRONE_LIST_SUCCESS: {
      const { data, current_page, per_page, total } = action.payload.data;
      return {
        ...state,
        total,
        data,
        current_page,
        per_page: parseInt(per_page, 10)
      };
    }
    default:
      return state;
  }
}

export default dronesReducer;
