import {
  GET_DRONE_LIST_REQUEST,
  GET_DRONE_LIST_SUCCESS,
  GET_DRONE_LIST_FAILED
} from '../constants';

export function getDroneListRequest(id) {
  return {
    type: GET_DRONE_LIST_REQUEST,
    payload: { id }
  };
}

export function getDroneListSuccess(data) {
  return {
    type: GET_DRONE_LIST_SUCCESS,
    payload: { data }
  };
}

export function getDroneListFailed(error) {
  return {
    type: GET_DRONE_LIST_FAILED,
    payload: { error }
  };
}
