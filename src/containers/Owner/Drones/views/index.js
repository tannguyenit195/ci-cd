import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { People } from '@material-ui/icons';

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messages from 'components/OwnerComponents/OwnerDrones/messages';

import Breadcrumbs from 'components/Breadcrumbs';
import BreadCrumbItem from 'components/BreadcrumbItem';
import DroneList from 'components/OwnerComponents/OwnerDrones/Drones';

import { FIELDS } from 'utils/constants';
import { getDroneListRequest } from '../actions';
import messagesCommon from 'translations/messagesCommon';

export class OwnerDrones extends React.Component {
  state = {
    search: ''
  };

  componentDidMount() {
    const {
      history: { location: { pathname } },
      getDroneList
    } = this.props;
    const companyId = pathname.split('/')[3];

    getDroneList(companyId);
  }

  onPageChanged = data => {
    const { currentPage } = data;
    const request = {
      page: currentPage
    };

    if (this.state.search) {
      request.fields = `email.search(${this.state.search}),id,first_name,last_name,last_login_at,phone,roles{display_name}`;
    }

    this.props.getDroneList(request);
  };

  search = (data) => {
    let fields = FIELDS.USERS;
    this.setState({ search: data });

    if (data) {
      fields = `email.search(${data}),id,first_name,last_name,last_login_at,phone,roles{display_name}`;
    }
    const request = {
      fields: fields
    };
    this.props.getDroneList(request);
  };

  goToDetail = id => {
    const { history } = this.props;
    history.push(`${history.location.pathname}/${id}`);
  };

  render() {
    const {
      drones: {
        data,
        total,
        per_page,
        current_page
      }
    } = this.props;

    return [
      <Breadcrumbs icon={<People/>} key='Breadcrumbs' headerTitle={intl.formatMessage(messages.drone_list)}>
        <BreadCrumbItem to="/" title={intl.formatMessage(messagesCommon.home)} icon="fa-dashboard"/>
        <BreadCrumbItem active title={intl.formatMessage(messages.drone_list)}/>
      </Breadcrumbs>,
      <DroneList
        key='DroneList'
        total={total}
        currentPage={current_page}
        perPage={per_page}
        data={data}
        goToDetail={this.goToDetail}
        search={this.search}
        onPageChanged={this.onPageChanged}
      />
    ];
  }
}

OwnerDrones.propTypes = {
  history: PropTypes.object,
  drones: PropTypes.object,
  getDroneList: PropTypes.func
};

const mapDispatchToProps = (dispatch) => ({
  getDroneList: (companyId) => dispatch(getDroneListRequest(companyId))
});

const mapStateToProps = (state) => ({
  drones: state.drones
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default withConnect(OwnerDrones);
