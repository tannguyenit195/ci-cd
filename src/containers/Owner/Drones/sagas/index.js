import { takeLatest, put } from 'redux-saga/effects';

// import ApiService from 'utils/axios';
import { GET_DRONE_LIST_REQUEST } from '../constants';
import { getDroneListFailed, getDroneListSuccess } from '../actions';
// import { FIELDS, PAGE_LIMIT } from 'utils/constants';
import { uniqueId } from 'lodash';
import toastr from 'helpers/notifications';

const DRONES = [
  {
    id: uniqueId('drone_'),
    name: uniqueId('drone_name_'),
    manufacturer: 'DJI',
    product_url: 'https://www.dji.com/jp/phantom-4',
    serial_number: '07JDD7F00100LH',
    updated: '2019/03/20 18:00'
  },
  {
    id: uniqueId('drone_'),
    name: uniqueId('drone_name_'),
    manufacturer: 'DJI',
    product_url: 'https://www.dji.com/jp/phantom-4',
    serial_number: '07JDD7F00100LH',
    updated: '2019/03/20 18:00'
  },
  {
    id: uniqueId('drone_'),
    name: uniqueId('drone_name_'),
    manufacturer: 'DJI',
    product_url: 'https://www.dji.com/jp/phantom-4',
    serial_number: '07JDD7F00100LH',
    updated: '2019/03/20 18:00'
  },
  {
    id: uniqueId('drone_'),
    name: uniqueId('drone_name_'),
    manufacturer: 'DJI',
    product_url: 'https://www.dji.com/jp/phantom-4',
    serial_number: '07JDD7F00100LH',
    updated: '2019/03/20 18:00'
  },
  {
    id: uniqueId('drone_'),
    name: uniqueId('drone_name_'),
    manufacturer: 'DJI',
    product_url: 'https://www.dji.com/jp/phantom-4',
    serial_number: '07JDD7F00100LH',
    updated: '2019/03/20 18:00'
  },
  {
    id: uniqueId('drone_'),
    name: uniqueId('drone_name_'),
    manufacturer: 'DJI',
    product_url: 'https://www.dji.com/jp/phantom-4',
    serial_number: '07JDD7F00100LH',
    updated: '2019/03/20 18:00'
  },
  {
    id: uniqueId('drone_'),
    name: uniqueId('drone_name_'),
    manufacturer: 'DJI',
    product_url: 'https://www.dji.com/jp/phantom-4',
    serial_number: '07JDD7F00100LH',
    updated: '2019/03/20 18:00'
  },
  {
    id: uniqueId('drone_'),
    name: uniqueId('drone_name_'),
    manufacturer: 'DJI',
    product_url: 'https://www.dji.com/jp/phantom-4',
    serial_number: '07JDD7F00100LH',
    updated: '2019/03/20 18:00'
  },
  {
    id: uniqueId('drone_'),
    name: uniqueId('drone_name_'),
    manufacturer: 'DJI',
    product_url: 'https://www.dji.com/jp/phantom-4',
    serial_number: '07JDD7F00100LH',
    updated: '2019/03/20 18:00'
  },
  {
    id: uniqueId('drone_'),
    name: uniqueId('drone_name_'),
    manufacturer: 'DJI',
    product_url: 'https://www.dji.com/jp/phantom-4',
    serial_number: '07JDD7F00100LH',
    updated: '2019/03/20 18:00'
  }
];

const FAKE_DATA = {
  current_page: 1,
  data: DRONES,
  total: DRONES.length,
  per_page: 10
};

function* getDronesSaga() {
  // const { page = 1, limit = PAGE_LIMIT, fields = FIELDS.USERS } = action.request || {};
  // const path = `/users?page=${page}&limit=${limit}&fields=${fields}`;
  try {
    //   const { status, data } = yield call(ApiService.get, path);
    //   if (status === 200) {
    //     yield put(getDroneListSuccess(data.data));
    //   }
    yield put(getDroneListSuccess(FAKE_DATA));
  } catch (err) {
    const { response } = { ...err };
    toastr.error(response.data.error.message);
    yield put(getDroneListFailed(response.data.error.message));
  }
}

export default function* ownerDronesWatch() {
  yield takeLatest(GET_DRONE_LIST_REQUEST, getDronesSaga);
}
