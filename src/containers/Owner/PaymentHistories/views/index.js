import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Payment } from '@material-ui/icons';

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messages from 'components/OwnerComponents/Users/messages';

import Breadcrumbs from 'components/Breadcrumbs';
import BreadCrumbItem from 'components/BreadcrumbItem';
import InvoicesComponent from 'components/OwnerComponents/Invoices';

import { getPaymentHistoriesRequest } from '../actions';
import { INVOICES_TYPE } from 'utils/constants';
import messagesCommon from 'translations/messagesCommon';

export class PaymentHistories extends React.Component {
  componentDidMount() {
    const { getPaymentHistoriesRequest, match: { params } } = this.props;
    this.companyId = params.id;

    getPaymentHistoriesRequest({
      id: this.companyId
    });
  }

  onPageChanged = data => {
    const { currentPage } = data;

    this.props.getPaymentHistoriesRequest({
      id: this.companyId,
      currentPage
    });
  };

  render() {
    const {
      paymentHistories: {
        data,
        total,
        per_page,
        current_page
      }
    } = this.props;

    return [
      <Breadcrumbs icon={<Payment/>} key='Breadcrumbs' headerTitle={intl.formatMessage(messages.payment_histories)}>
        <BreadCrumbItem to="/owner" title={intl.formatMessage(messagesCommon.home)} icon="fa-dashboard"/>
        <BreadCrumbItem active title={intl.formatMessage(messages.payment_histories)}/>
      </Breadcrumbs>,
      <InvoicesComponent
        key='InvoicesComponent'
        type={INVOICES_TYPE.PAYMENT}
        total={total}
        currentPage={current_page}
        perPage={per_page}
        data={data}
        onPageChanged={this.onPageChanged}
      />
    ];
  }
}

PaymentHistories.propTypes = {
  match: PropTypes.object,
  history: PropTypes.object,
  paymentHistories: PropTypes.object,
  getPaymentHistoriesRequest: PropTypes.func
};

const mapDispatchToProps = (dispatch) => ({
  getPaymentHistoriesRequest: (companyId) => dispatch(getPaymentHistoriesRequest(companyId))
});

const mapStateToProps = (state) => ({
  paymentHistories: state.paymentHistories
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default withConnect(PaymentHistories);
