import { takeLatest, put } from 'redux-saga/effects';

// import ApiService from 'utils/axios';
import { GET_PAYMENT_HISTORIES_REQUEST } from '../constants';
import { getPaymentHistoriesSuccess, getPaymentHistoriesFailed } from '../actions';
import { uniqueId } from 'lodash';


const DATA = [
  {
    id: uniqueId(),
    month: '2019/03',
    price: 5000,
    due_date: '2019/05/31'
  },
  {
    id: uniqueId(),
    month: '2019/04',
    price: 5000,
    due_date: '2019/05/31'
  },
  {
    id: uniqueId(),
    month: '2019/05',
    price: 5000,
    due_date: '2019/05/31'
  },
  {
    id: uniqueId(),
    month: '2019/06',
    price: 5000,
    due_date: '2019/05/31'
  },
  {
    id: uniqueId(),
    month: '2019/07',
    price: 5000,
    due_date: '2019/05/31'
  }
];

const FAKE_DATA = {
  current_page: 1,
  data: DATA,
  total: 1,
  per_page: 10
};

function* getPaymentHistoriesSaga() {
  // const { companyId } = action.payload;
  // const path = `/organizations/${companyId}?fields=${fields}`;
  try {
    // const { status, data: { data } } = yield call(ApiService.get, path);
    // if (status === 200) {
    //   yield put(getPaymentHistoriesSuccess(data.users));
    // }
    yield put(getPaymentHistoriesSuccess(FAKE_DATA));
  } catch (err) {
    const { response } = { ...err };
    yield put(getPaymentHistoriesFailed(response.data.error.message));
  }
}

export default function* ownerDronesWatch() {
  yield takeLatest(GET_PAYMENT_HISTORIES_REQUEST, getPaymentHistoriesSaga);
}
