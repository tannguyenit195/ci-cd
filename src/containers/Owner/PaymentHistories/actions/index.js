import {
  GET_PAYMENT_HISTORIES_REQUEST,
  GET_PAYMENT_HISTORIES_SUCCESS,
  GET_PAYMENT_HISTORIES_FAILED
} from '../constants';

export function getPaymentHistoriesRequest(request) {
  return {
    type: GET_PAYMENT_HISTORIES_REQUEST,
    payload: { ...request }
  };
}

export function getPaymentHistoriesSuccess(data) {
  return {
    type: GET_PAYMENT_HISTORIES_SUCCESS,
    payload: { data }
  };
}

export function getPaymentHistoriesFailed(error) {
  return {
    type: GET_PAYMENT_HISTORIES_FAILED,
    payload: { error }
  };
}
