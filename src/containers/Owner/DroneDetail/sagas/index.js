import { takeLatest, put } from 'redux-saga/effects';

// import ApiService from 'utils/axios';
import { GET_DRONE_DETAIL_REQUEST } from '../constants';
import { uniqueId } from 'lodash';
import { getDroneDetailFailed, getDroneDetailSuccess } from '../actions';
import toastr from 'helpers/notifications';

const FAKE_DRONE = {
  id: uniqueId('drone_'),
  name: uniqueId('Drone Name '),
  manufacturer: 'DJI',
  product_url: 'https://www.dji.com/jp/phantom-4',
  serial_number: '07JDD7F00100LH',
  updated: '2019/03/20 18:00'
};

function* getDroneDetailSaga() {
  // const { page = 1, limit = PAGE_LIMIT, fields = FIELDS.USERS } = action.request || {};
  // const path = `/users?page=${page}&limit=${limit}&fields=${fields}`;
  try {
    // const { status, data } = yield call(ApiService.get, path);
    // if (status === 200) {
    //   yield put(getDroneDetailSuccess(data.data));
    // }
    yield put(getDroneDetailSuccess(FAKE_DRONE));
  } catch (err) {
    const { response } = { ...err };
    toastr.error(response.data.error.message);
    yield put(getDroneDetailFailed(response.data.error.message));
  }
}

export default function* ownerDroneDetailWatch() {
  yield takeLatest(GET_DRONE_DETAIL_REQUEST, getDroneDetailSaga);
}
