import {
  GET_DRONE_DETAIL_REQUEST,
  GET_DRONE_DETAIL_SUCCESS,
  GET_DRONE_DETAIL_FAILED
} from '../constants';

export function getDroneDetailRequest(id) {
  return {
    type: GET_DRONE_DETAIL_REQUEST,
    payload: { id }
  };
}

export function getDroneDetailSuccess(data) {
  return {
    type: GET_DRONE_DETAIL_SUCCESS,
    payload: { data }
  };
}

export function getDroneDetailFailed(error) {
  return {
    type: GET_DRONE_DETAIL_FAILED,
    payload: { error }
  };
}
