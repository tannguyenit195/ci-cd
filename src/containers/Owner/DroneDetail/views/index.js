import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { People } from '@material-ui/icons';

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messages from 'components/OwnerComponents/OwnerDrones/messages';

import Breadcrumbs from 'components/Breadcrumbs';
import BreadCrumbItem from 'components/BreadcrumbItem';
import DroneDetailComponent from 'components/OwnerComponents/OwnerDrones/DroneDetail';

import { getDroneDetailRequest } from '../actions';
import messagesCommon from 'translations/messagesCommon';

class OwnerDroneDetail extends React.Component {
  componentDidMount() {
    const id = 'xxxx';
    this.props.getDrone(id);
  }

  goBack = () => this.props.history.goBack();

  render() {
    const { drone } = this.props;

    return [
      <Breadcrumbs icon={<People/>} key='Breadcrumbs' headerTitle={intl.formatMessage(messages.drone_detail)}>
        <BreadCrumbItem to="/" title={intl.formatMessage(messagesCommon.home)} icon="fa-dashboard"/>
        <BreadCrumbItem active title={intl.formatMessage(messages.drone_detail)}/>
      </Breadcrumbs>,
      <DroneDetailComponent
        key="DroneDetailComponent"
        drone={drone}
        goBack={this.goBack}
      />
    ];
  }
}

OwnerDroneDetail.propTypes = {
  history: PropTypes.object,
  drone: PropTypes.object,
  getDrone: PropTypes.func
};

const mapDispatchToProps = (dispatch) => ({
  getDrone: (droneId) => dispatch(getDroneDetailRequest(droneId))
});

const mapStateToProps = (state) => ({
  drone: state.droneDetail
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default withConnect(OwnerDroneDetail);
