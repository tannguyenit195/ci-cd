import {
  GET_DRONE_DETAIL_SUCCESS
} from '../constants';

const droneDetail = null;

function droneDetailReducer(state = droneDetail, action) {
  switch (action.type) {
    case GET_DRONE_DETAIL_SUCCESS: {
      return action.payload.data;
    }
    default:
      return state;
  }
}

export default droneDetailReducer;
