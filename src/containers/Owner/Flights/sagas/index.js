import { put, takeLatest } from 'redux-saga/effects';

// import ApiService from 'utils/axios';
import { GET_FLIGHT_LIST_REQUEST } from '../constants';
import { getFlightListSuccess } from 'containers/Owner/Flights/actions';
// import { getDroneListFailed, getDroneListSuccess } from '../actions';
// import { FIELDS, PAGE_LIMIT } from 'utils/constants';

function* getFlightSage(action) {
  console.log('>> getDronesSage', action);
  const initData = [
    {
      id: 'Vl0XDJ7vJ7vaN62y',
      name: 'flight 1',
      time: '00 minute 21 second',
      distance: 40.05,
      status: 'fail'
    },
    {
      id: 'pJ4nNZjPM75w68eL',
      name: 'flight 2',
      time: '01 minute 21 second',
      distance: 50,
      status: 'fail'
    },
    {
      id: 'oAYR957LrBgKbONv',
      name: 'flight 3',
      time: '02 minute 21 second',
      distance: 90,
      status: 'fail'
    },
    {
      id: 'oleDgREd0EzMOarv',
      name: 'flight 4',
      time: '04 minute 21 second',
      distance: 105,
      status: 'fail'
    }
  ];
  const data = {
    data: initData,
    total: 12,
    per_page: 10,
    current_page: 1
  };
  yield put(getFlightListSuccess(data));

  // const { page = 1, limit = PAGE_LIMIT, fields = FIELDS.USERS } = action.request || {};
  // const path = `/users?page=${page}&limit=${limit}&fields=${fields}`;
  // try {
  //   const { status, data } = yield call(ApiService.get, path);
  //   if (status === 200) {
  //     yield put(getDroneListSuccess(data.data));
  //   }
  // } catch (err) {
  //   const { response } = { ...err };
  //   console.log('>>> getDronesSage error', response);
  //   yield put(getDroneListFailed(response.data.error.message));
  // }
}

export default function* ownerFlightsWatch() {
  yield takeLatest(GET_FLIGHT_LIST_REQUEST, getFlightSage);
}
