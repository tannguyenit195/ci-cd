import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messages from 'components/OwnerMissions/messages';

import Breadcrumbs from 'components/Breadcrumbs';
import BreadCrumbItem from 'components/BreadcrumbItem';
import FlightList from 'components/OwnerMissions/Flights';
import { getFlightListRequest } from '../actions';
import messagesCommon from 'translations/messagesCommon';

const initData = [
  { id: '1' },
  { id: '2' },
  { id: '3' },
  { id: '4' }
];

export class OwnerFlights extends React.Component {
  state = {
    search: '',
    flights: {
      data: initData,
      total: 12,
      per_page: 10,
      current_page: 1
    }
  };

  componentDidMount() {
    const {
      history: { location: { pathname } },
      getFlightList
    } = this.props;
    const companyId = pathname.split('/')[3];

    getFlightList(companyId);
  }

  onPageChanged = data => {
    const { currentPage } = data;
    const request = {
      page: currentPage
    };
    console.log('CHANGE_PAGE', request);
  };

  search = (data) => {
    console.info('SEARCH', data);
  };

  goDetail = flightId => {
    const { match: { params: { id } }, history } = this.props;
    history.push(`/owner/companies/${id}/logs/${flightId}`);
  };

  render() {
    const {
      match: { params: { id } },
      history, flights: {
        data,
        total,
        per_page,
        current_page
      }
    } = this.props;

    return [
      <Breadcrumbs icon={<i className="fa fa-fighter-jet" aria-hidden="true"/>} key='Breadcrumbs' headerTitle={intl.formatMessage(messages.flights_list)}>
        <BreadCrumbItem to="/" title={intl.formatMessage(messagesCommon.home)} icon="fa-dashboard"/>
        <BreadCrumbItem to={`/companies/${id}/missions`} title={intl.formatMessage(messages.mission_list)}/>
        <BreadCrumbItem active title={intl.formatMessage(messages.flights_list)}/>
      </Breadcrumbs>,
      <FlightList
        key='FlightList'
        total={total}
        history={history}
        currentPage={current_page}
        perPage={per_page}
        data={data}
        search={this.search}
        goDetail={this.goDetail}
        onPageChanged={this.onPageChanged}
      />
    ];
  }
}

OwnerFlights.propTypes = {
  history: PropTypes.object,
  match: PropTypes.object,
  flights: PropTypes.object,
  getFlightList: PropTypes.func
};

const mapDispatchToProps = (dispatch) => ({
  getFlightList: (companyId) => dispatch(getFlightListRequest(companyId))
});

const mapStateToProps = (state) => ({
  flights: state.flights
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default withConnect(OwnerFlights);
