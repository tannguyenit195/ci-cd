import {
  GET_FLIGHT_LIST_REQUEST,
  GET_FLIGHT_LIST_SUCCESS,
  GET_FLIGHT_LIST_FAILED
} from '../constants';

export function getFlightListRequest(id) {
  return {
    type: GET_FLIGHT_LIST_REQUEST,
    payload: { id }
  };
}

export function getFlightListSuccess(data) {
  return {
    type: GET_FLIGHT_LIST_SUCCESS,
    payload: { data }
  };
}

export function getFlightListFailed(error) {
  return {
    type: GET_FLIGHT_LIST_FAILED,
    payload: { error }
  };
}
