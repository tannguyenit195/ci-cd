import LazyLoading from 'components/LazyLoading';

export default LazyLoading({
  loader: () => import('containers/UserSetting/views')
});
