import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import Breadcrumbs from 'components/Breadcrumbs';
import BreadCrumbItem from 'components/BreadcrumbItem';
import messages from 'components/OwnerComponents/Company/messages';
import { FIELDS } from 'utils/constants';
import OwnerCompanies from 'components/OwnerComponents/Company';
import { acceptDelete, getCompany, openDialog } from 'containers/Owner/Company/actions';
import messagesCommon from 'translations/messagesCommon';

export class OwnerCompany extends React.Component {
  state = {
    search: ''
  };

  componentDidMount() {
    const { company: { data } } = this.props;
    if (data && data.length) {
      return;
    }
    this.props.getCompanyRequest();
  }

  onPageChanged = data => {
    const { currentPage } = data;
    const request = {
      page: currentPage
    };

    if (this.state.search) {
      request.fields = `name.search(${this.state.search.toString()}),type,phone`;
    }

    this.props.getCompanyRequest(request);
  };

  search = (data) => {
    this.setState({ search: data });
    const request = {
      fields: data ? `name.search(${data.toString()}),type,phone` : FIELDS.COMPANY
    };

    this.props.getCompanyRequest(request);
  };

  render() {
    const {
      company: {
        data,
        total,
        per_page,
        current_page,
        openDialogDelete
      }
    } = this.props;

    return [
      <Breadcrumbs icon={<i className="fa fa-building"/>} key='Breadcrumbs' headerTitle={intl.formatMessage(messages.company_list)}>
        <BreadCrumbItem to="/owner" title={intl.formatMessage(messagesCommon.home)} icon="fa-dashboard"/>
        <BreadCrumbItem active title={intl.formatMessage(messages.company_list)}/>
      </Breadcrumbs>,
      <OwnerCompanies
        key='CompanyList'
        total={total}
        currentPage={current_page}
        perPage={per_page}
        data={data}
        openDialogDelete={openDialogDelete}
        onPageChanged={this.onPageChanged}
        search={this.search}
        acceptDelete={this.props.acceptDelete}
        openDialog={this.props.openDialog}
      />
    ];
  }
}

OwnerCompany.propTypes = {
  company: PropTypes.object,
  getCompanyRequest: PropTypes.func,
  acceptDelete: PropTypes.func,
  openDialog: PropTypes.func
};

export function mapDispatchToProps(dispatch) {
  return {
    getCompanyRequest: (request) => {
      dispatch(getCompany(request));
    },
    acceptDelete: (data) => {
      dispatch(acceptDelete(data));
    },
    openDialog: (data) => {
      dispatch(openDialog(data));
    }
  };
}

const mapStateToProps = (state) => ({
  company: state.ownerCompany
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);


export default compose(
  withConnect
)(OwnerCompany);
