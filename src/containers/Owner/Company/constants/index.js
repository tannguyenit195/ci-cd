/*
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const GET_COMPANY_REQUEST = 'OwnerCompany/GET_COMPANY_REQUEST';
export const GET_COMPANY_SUCCESS = 'OwnerCompany/GET_COMPANY_SUCCESS';
export const GET_COMPANY_ERROR = 'OwnerCompany/GET_COMPANY_ERROR';
export const OPEN_DIALOG = 'OwnerCompany/OPEN_DIALOG';
export const ACCEPT_DELETE = 'OwnerCompany/ACCEPT_DELETE';
