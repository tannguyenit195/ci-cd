/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { GET_COMPANY_SUCCESS, OPEN_DIALOG } from 'containers/Owner/Company/constants';

export const initialState = {
  current_page: 0,
  data: [],
  total: 0,
  per_page: 10,
  openDialogDelete: false,
  currentCompany: {
    id: '',
    data: {}
  }
};

function ownerCompanyReducer(state = initialState, action) {
  switch (action.type) {
    case GET_COMPANY_SUCCESS: {
      const { data, current_page, per_page, total } = action.payload.data;
      return {
        ...state,
        total,
        data,
        current_page,
        per_page: parseInt(per_page, 10)
      };
    }
    case OPEN_DIALOG: {
      return {
        ...state,
        openDialogDelete: !state.openDialogDelete
      }
    }
    default:
      return state;
  }
}

export default ownerCompanyReducer;
