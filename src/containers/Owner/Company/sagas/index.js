/**
 * Root saga manages watcher lifecycle
 */
import ApiService from 'utils/axios';
import { call, put, takeLatest } from 'redux-saga/effects';
import { FIELDS, PAGE_LIMIT } from 'utils/constants';
import { ACCEPT_DELETE, GET_COMPANY_REQUEST } from 'containers/Owner/Company/constants';
import { getCompany, getCompanyError, getCompanySuccess, openDialog } from 'containers/Owner/Company/actions';
import { DELETE_DIALOG } from 'containers/Users/constants';
import toastr from 'helpers/notifications';

function* listCompany(action) {
  const { page = 1, limit = PAGE_LIMIT, fields = FIELDS.COMPANY } = action.request || {};
  const path = encodeURI(`/organizations?page=${page}&limit=${limit}&fields=${fields},name,users{email}`);
  try {
    const { status, data } = yield call(ApiService.get, path);
    if (status === 200) {
      yield put(getCompanySuccess(data));
    }
  } catch (e) {
    const { response } = { ...e };
    toastr.error(response.data.error.message);
    yield put(getCompanyError(response));
  }
}

function* deleteCompany(action) {
  const { id, page } = action.payload;
  const path = `/organizations/${id}`;
  const request = { page };

  try {
    const { status } = yield call(ApiService.delete, path);
    if (status === 200) {
      yield put(getCompany(request));
      yield put(openDialog(DELETE_DIALOG));
    }
  } catch (e) {
    const { response } = { ...e };
    toastr.error(response.data.error.message);
    yield put(getCompanyError(response));
  }
}

export default function* ownerCompanyWatch() {
  yield takeLatest(GET_COMPANY_REQUEST, listCompany);
  yield takeLatest(ACCEPT_DELETE, deleteCompany);
}
