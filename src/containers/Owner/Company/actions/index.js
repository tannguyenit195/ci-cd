import {
  ACCEPT_DELETE,
  GET_COMPANY_ERROR,
  GET_COMPANY_REQUEST,
  GET_COMPANY_SUCCESS,
  OPEN_DIALOG
} from 'containers/Owner/Company/constants';

export function getCompany(request) {
  return {
    type: GET_COMPANY_REQUEST,
    request
  };
}

export function getCompanySuccess(data) {
  return {
    type: GET_COMPANY_SUCCESS,
    payload: data
  };
}

export function getCompanyError(data) {
  return {
    type: GET_COMPANY_ERROR,
    payload: data
  };
}

export function openDialog(data) {
  return {
    type: OPEN_DIALOG,
    payload: data
  };
}

export function acceptDelete(data) {
  return {
    type: ACCEPT_DELETE,
    payload: data
  };
}
