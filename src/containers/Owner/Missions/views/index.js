import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messages from 'components/OwnerMissions/messages';

import Breadcrumbs from 'components/Breadcrumbs';
import BreadCrumbItem from 'components/BreadcrumbItem';
import MissionList from 'components/OwnerMissions/Missions';
import { getMissionListRequest } from '../actions';
import messagesCommon from 'translations/messagesCommon';

export class OwnerDrones extends React.Component {
  state = {
    search: ''
  };

  componentDidMount() {
    const {
      match: { params: { id } },
      getMissionList
    } = this.props;

    getMissionList(id);
  }

  onPageChanged = data => {
    const { currentPage } = data;
    const request = {
      page: currentPage
    };
    console.log('CHANGE_PAGE', request);
  };

  search = (data) => {
    console.info('SEARCH', data);
  };

  goDetail = missionId => {
    const { match: { params: { id } }, history } = this.props;
    history.push(`/owner/companies/${id}/flights/${missionId}`);
  };

  render() {
    const {
      history, missions: {
        data,
        total,
        per_page,
        current_page
      }
    } = this.props;

    return [
      <Breadcrumbs icon={<i className="fa fa-fighter-jet" aria-hidden="true"/>} key='Breadcrumbs' headerTitle={intl.formatMessage(messages.mission_list)}>
        <BreadCrumbItem to="/" title={intl.formatMessage(messagesCommon.home)} icon="fa-dashboard"/>
        <BreadCrumbItem active title={intl.formatMessage(messages.mission_list)}/>
      </Breadcrumbs>,
      <MissionList
        key='MissionList'
        total={total}
        history={history}
        currentPage={current_page}
        perPage={per_page}
        data={data}
        search={this.search}
        goDetail={this.goDetail}
        onPageChanged={this.onPageChanged}
      />
    ];
  }
}

OwnerDrones.propTypes = {
  history: PropTypes.object,
  match: PropTypes.object,
  missions: PropTypes.object,
  getMissionList: PropTypes.func
};

const mapDispatchToProps = (dispatch) => ({
  getMissionList: (companyId) => dispatch(getMissionListRequest(companyId))
});

const mapStateToProps = (state) => ({
  missions: state.missions
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default withConnect(OwnerDrones);
