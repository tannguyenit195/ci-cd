import { put, takeLatest } from 'redux-saga/effects';

// import ApiService from 'utils/axios';
import { GET_MISSION_LIST_REQUEST } from '../constants';
import { getMissionListSuccess } from 'containers/Owner/Missions/actions';
// import { getDroneListFailed, getDroneListSuccess } from '../actions';
// import { FIELDS, PAGE_LIMIT } from 'utils/constants';

function* getMissionSaga(action) {
  console.log('>> getMissionSaga', action);
  const initData = [
    { id: 'Vl0XDJ7vJ7vaN62y', name: 'mission 1' },
    { id: 'pJ4nNZjPM75w68eL', name: 'mission 2' },
    { id: 'oAYR957LrBgKbONv', name: 'mission 3' },
    { id: 'oleDgREd0EzMOarv', name: 'mission 4' },
  ];
  const data = {
    data: initData,
    total: 12,
    per_page: 10,
    current_page: 1
  };
  yield put(getMissionListSuccess(data));
  // const { page = 1, limit = PAGE_LIMIT, fields = FIELDS.USERS } = action.request || {};
  // const path = `/users?page=${page}&limit=${limit}&fields=${fields}`;
  // try {
  //   const { status, data } = yield call(ApiService.get, path);
  //   if (status === 200) {
  //    yield put(getDroneListSuccess(data.data));
  //   }
  // } catch (err) {
  //   const { response } = { ...err };
  //   console.log('>>> getDronesSage error', response);
  //   yield put(getDroneListFailed(response.data.error.message));
  // }
}

export default function* ownerMissionWatch() {
  yield takeLatest(GET_MISSION_LIST_REQUEST, getMissionSaga);
}
