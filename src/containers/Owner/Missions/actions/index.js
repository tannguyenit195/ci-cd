import {
  GET_MISSION_LIST_REQUEST,
  GET_MISSION_LIST_SUCCESS,
  GET_MISSION_LIST_FAILED
} from '../constants';

export function getMissionListRequest(id) {
  return {
    type: GET_MISSION_LIST_REQUEST,
    payload: { id }
  };
}

export function getMissionListSuccess(data) {
  return {
    type: GET_MISSION_LIST_SUCCESS,
    payload: { data }
  };
}

export function getMissionListFailed(error) {
  return {
    type: GET_MISSION_LIST_FAILED,
    payload: { error }
  };
}
