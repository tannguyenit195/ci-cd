import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { People } from '@material-ui/icons';

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messages from 'components/OwnerComponents/Users/messages';

import Breadcrumbs from 'components/Breadcrumbs';
import BreadCrumbItem from 'components/BreadcrumbItem';
import UserListComponent from 'components/OwnerComponents/Users/UserList';

import { FIELDS } from 'utils/constants';
import { getUserListRequest } from '../actions';
import messagesCommon from 'translations/messagesCommon';

function formatFields(page = 1, searchText) {
  return `users.page(${page}){email${searchText ? `.search(${searchText})` : ''},id,first_name,authority,last_name,last_login_at,phone,roles{display_name}}`;
}

export class OwnerUserList extends React.Component {
  state = {
    search: ''
  };

  componentDidMount() {
    const { getUserListRequest, match: { params }, users: { data } } = this.props;
    if (data && data.length) {
      return;
    }
    this.setState({
      companyId: params.id
    });
    const fields = formatFields(1);

    getUserListRequest({
      id: params.id,
      fields
    });
  }

  onPageChanged = data => {
    const { search } = this.state;
    const { currentPage } = data;
    const fields = formatFields(currentPage, search);
    const { match: { params: { id } } } = this.props;

    this.props.getUserListRequest({
      id,
      fields
    });
  };

  search = (data) => {
    let fields = FIELDS.USERS;
    this.setState({ search: data });
    const { match: { params: { id } } } = this.props;

    if (data) {
      fields = formatFields(1, data);
    }

    const request = {
      id,
      fields: fields
    };
    this.props.getUserListRequest(request);
  };

  gotoDetail = (id) => {
    const { history } = this.props;
    history.push(`${history.location.pathname}/${id}`);
  };

  render() {
    const {
      users: {
        data,
        total,
        per_page,
        current_page
      }
    } = this.props;

    return [
      <Breadcrumbs icon={<People/>} key='Breadcrumbs' headerTitle={intl.formatMessage(messages.user_list)}>
        <BreadCrumbItem to="/owner" title={intl.formatMessage(messagesCommon.home)} icon="fa-dashboard"/>
        <BreadCrumbItem active title={intl.formatMessage(messages.user_list)}/>
      </Breadcrumbs>,
      <UserListComponent
        key='UserListComponent'
        total={total}
        currentPage={current_page}
        perPage={per_page}
        data={data}
        gotoDetail={this.gotoDetail}
        search={this.search}
        onPageChanged={this.onPageChanged}
      />
    ];
  }
}

OwnerUserList.propTypes = {
  match: PropTypes.object,
  history: PropTypes.object,
  users: PropTypes.object,
  getUserListRequest: PropTypes.func
};

const mapDispatchToProps = (dispatch) => ({
  getUserListRequest: (companyId) => dispatch(getUserListRequest(companyId))
});

const mapStateToProps = (state) => ({
  users: state.userList
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default withConnect(OwnerUserList);
