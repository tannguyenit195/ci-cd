import {
  GET_USER_LIST_SUCCESS
} from '../constants';

const stateDefault = {
  current_page: 0,
  data: [],
  total: 0,
  per_page: 10
};

function ownerUserListReducer(state = stateDefault, action) {
  switch (action.type) {
    case GET_USER_LIST_SUCCESS: {
      const { data, current_page, per_page, total } = action.payload.data;
      return {
        ...state,
        total,
        data,
        current_page,
        per_page: parseInt(per_page, 10)
      };
    }
    default:
      return state;
  }
}

export default ownerUserListReducer;
