import { takeLatest, call, put } from 'redux-saga/effects';

import ApiService from 'utils/axios';
import { GET_USER_LIST_REQUEST } from '../constants';
import { getUserListSuccess, getUserListFailed } from '../actions';
import toastr from 'helpers/notifications';

function* getUserListSaga(action) {
  const { id, fields } = action.payload;
  const path = encodeURI(`/organizations/${id}?fields=${fields}`);
  try {
    const { status, data: { data } } = yield call(ApiService.get, path);
    if (status === 200) {
      yield put(getUserListSuccess(data.users));
    }
  } catch (err) {
    const { response } = { ...err };
    toastr.error(response.data.error.message);
    yield put(getUserListFailed(response.data.error.message));
  }
}

export default function* ownerDronesWatch() {
  yield takeLatest(GET_USER_LIST_REQUEST, getUserListSaga);
}
