import {
  GET_USER_LIST_REQUEST,
  GET_USER_LIST_SUCCESS,
  GET_USER_LIST_FAILED
} from '../constants';

export function getUserListRequest(data) {
  return {
    type: GET_USER_LIST_REQUEST,
    payload: { ...data }
  };
}

export function getUserListSuccess(data) {
  return {
    type: GET_USER_LIST_SUCCESS,
    payload: { data }
  };
}

export function getUserListFailed(error) {
  return {
    type: GET_USER_LIST_FAILED,
    payload: { error }
  };
}
