/**
 * Root saga manages watcher lifecycle
 */
import ApiService from 'utils/axios';
import { call, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { CREATE_COMPANY_REQUEST } from 'containers/Owner/CompanyCreate/constants';
import { createCompanyError } from 'containers/Owner/CompanyCreate/actions';
import toastr from 'helpers/notifications';

function* createCompany(action) {
  const path = '/organizations';
  try {
    const { status } = yield call(ApiService.post, path, action.payload);
    if (status === 200) {
      yield put(push('/companies'));
    }
  } catch (e) {
    const { response } = { ...e };
    toastr.error(response.data.error.message);
    yield put(createCompanyError(response));
  }
}

export default function* ownerCompanyCreateWatch() {
  yield takeLatest(CREATE_COMPANY_REQUEST, createCompany);
}
