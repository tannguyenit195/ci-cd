/*
 * UsersPage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import Breadcrumbs from 'components/Breadcrumbs';
import BreadCrumbItem from 'components/BreadcrumbItem';
import messages from 'components/OwnerComponents/Company/messages';
import OwnerCompaniesCreate from 'components/OwnerComponents/CompanyCreate';
import { createCompany } from 'containers/Owner/CompanyCreate/actions';
import messagesCommon from 'translations/messagesCommon';

export class OwnerCompanyCreate extends React.Component {
  render() {
    return [
      <Breadcrumbs icon={<i className="fa fa-building" key="building"/>} key='Breadcrumbs' headerTitle={intl.formatMessage(messages.company_create)}>
        <BreadCrumbItem to="/owner" title={intl.formatMessage(messagesCommon.home)} icon="fa-dashboard"/>
        <BreadCrumbItem to="/owner/companies" title={intl.formatMessage(messages.company)}/>
        <BreadCrumbItem active title={intl.formatMessage(messages.company_create)}/>
      </Breadcrumbs>,
      <OwnerCompaniesCreate key='CompanyCreate' createCompany={this.props.createCompanyRequest}/>
    ];
  }
}

OwnerCompanyCreate.propTypes = {
  createCompanyRequest: PropTypes.func
};

export function mapDispatchToProps(dispatch) {
  return {
    createCompanyRequest: (request) => {
      dispatch(createCompany(request));
    }
  };
}

const mapStateToProps = (state) => ({
  company: state.ownerCompany
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);


export default compose(
  withConnect
)(OwnerCompanyCreate);
