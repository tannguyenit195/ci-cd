import { CREATE_COMPANY_ERROR, CREATE_COMPANY_REQUEST } from 'containers/Owner/CompanyCreate/constants';

export function createCompany(data) {
  return {
    type: CREATE_COMPANY_REQUEST,
    payload: data
  };
}

export function createCompanyError(data) {
  return {
    type: CREATE_COMPANY_ERROR,
    payload: data
  };
}
