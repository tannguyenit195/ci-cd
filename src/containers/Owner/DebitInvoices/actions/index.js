import {
  GET_DEBIT_INVOICES_REQUEST,
  GET_DEBIT_INVOICES_SUCCESS,
  GET_DEBIT_INVOICES_FAILED
} from '../constants';

export function getDebitInvoicesRequest(request) {
  return {
    type: GET_DEBIT_INVOICES_REQUEST,
    payload: { ...request }
  };
}

export function getDebitInvoicesSuccess(data) {
  return {
    type: GET_DEBIT_INVOICES_SUCCESS,
    payload: { data }
  };
}

export function getDebitInvoicesFailed(error) {
  return {
    type: GET_DEBIT_INVOICES_FAILED,
    payload: { error }
  };
}
