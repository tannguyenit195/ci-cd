import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Payment } from '@material-ui/icons';

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import msg from 'components/OwnerComponents/InvoicesModal/messages';

import Breadcrumbs from 'components/Breadcrumbs';
import BreadCrumbItem from 'components/BreadcrumbItem';
import InvoicesComponent from 'components/OwnerComponents/Invoices';

import { getDebitInvoicesRequest } from '../actions';
import { INVOICES_TYPE } from 'utils/constants';
import messagesCommon from 'translations/messagesCommon';

const { formatMessage: trans } = intl;

export class DebitInvoices extends React.Component {
  componentDidMount() {
    const { getDebitInvoicesRequest, match: { params } } = this.props;
    this.companyId = params.id;

    getDebitInvoicesRequest({
      id: this.companyId
    });
  }

  onPageChanged = data => {
    const { currentPage } = data;

    this.props.getDebitInvoicesRequest({
      id: this.companyId,
      currentPage
    });
  };

  render() {
    const {
      debitInvoices: {
        data,
        total,
        per_page,
        current_page
      }
    } = this.props;

    return [
      <Breadcrumbs icon={<Payment/>} key='Breadcrumbs' headerTitle={trans(msg.debit_invoices)}>
        <BreadCrumbItem to="/owner" title={intl.formatMessage(messagesCommon.home)} icon="fa-dashboard"/>
        <BreadCrumbItem active title={trans(msg.debit_invoices)}/>
      </Breadcrumbs>,
      <InvoicesComponent
        key='InvoicesComponent'
        type={INVOICES_TYPE.DEBIT}
        total={total}
        currentPage={current_page}
        perPage={per_page}
        data={data}
        onPageChanged={this.onPageChanged}
      />
    ];
  }
}

DebitInvoices.propTypes = {
  match: PropTypes.object,
  history: PropTypes.object,
  debitInvoices: PropTypes.object,
  getDebitInvoicesRequest: PropTypes.func
};

const mapDispatchToProps = (dispatch) => ({
  getDebitInvoicesRequest: (companyId) => dispatch(getDebitInvoicesRequest(companyId))
});

const mapStateToProps = (state) => ({
  debitInvoices: state.debitInvoices
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default withConnect(DebitInvoices);
