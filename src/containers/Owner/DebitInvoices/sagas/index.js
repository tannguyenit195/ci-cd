import { takeLatest, put } from 'redux-saga/effects';

// import ApiService from 'utils/axios';
import { GET_DEBIT_INVOICES_REQUEST } from '../constants';
import { getDebitInvoicesSuccess, getDebitInvoicesFailed } from '../actions';
import { uniqueId } from 'lodash';
import toastr from 'helpers/notifications';


const DATA = [
  {
    id: uniqueId(),
    month: '2019/03',
    price: 115000,
    due_date: '2019/05/31'
  },
  {
    id: uniqueId(),
    month: '2019/04',
    price: 5000,
    due_date: '2019/05/31'
  },
  {
    id: uniqueId(),
    month: '2019/05',
    price: 5000,
    due_date: '2019/05/31'
  },
  {
    id: uniqueId(),
    month: '2019/06',
    price: 5000,
    due_date: '2019/05/31'
  },
  {
    id: uniqueId(),
    month: '2019/07',
    price: 5000,
    due_date: '2019/05/31'
  },
  {
    id: uniqueId(),
    month: '2019/06',
    price: 5000,
    due_date: '2019/05/31'
  },
  {
    id: uniqueId(),
    month: '2019/07',
    price: 5000,
    due_date: '2019/05/31'
  }
];

const FAKE_DATA = {
  current_page: 1,
  data: DATA,
  total: 1,
  per_page: 10
};

function* getDebitInvoicesSaga() {
  // const { companyId } = action.payload;
  // const path = `/organizations/${companyId}?fields=${fields}`;
  try {
    // const { status, data: { data } } = yield call(ApiService.get, path);
    // if (status === 200) {
    //   yield put(getDebitInvoicesSuccess(data.users));
    // }
    yield put(getDebitInvoicesSuccess(FAKE_DATA));
  } catch (err) {
    const { response } = { ...err };
    toastr.error(response.data.error.message);
    yield put(getDebitInvoicesFailed(response.data.error.message));
  }
}

export default function* ownerDronesWatch() {
  yield takeLatest(GET_DEBIT_INVOICES_REQUEST, getDebitInvoicesSaga);
}
