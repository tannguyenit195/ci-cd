import {
  GET_COMPANY_DETAIL_ERROR,
  GET_COMPANY_DETAIL_REQUEST,
  GET_COMPANY_DETAIL_SUCCESS,
  UPDATE_COMPANY_DETAIL_REQUEST
} from 'containers/Owner/CompanyDetail/constants';

export function getCompanyDetail(id) {
  return {
    type: GET_COMPANY_DETAIL_REQUEST,
    payload: id
  };
}

export function getCompanyDetailSuccess(data) {
  return {
    type: GET_COMPANY_DETAIL_SUCCESS,
    payload: data
  };
}

export function getCompanyDetailError(data) {
  return {
    type: GET_COMPANY_DETAIL_ERROR,
    payload: data
  };
}

export function updateCompanyDetail(data) {
  return {
    type: UPDATE_COMPANY_DETAIL_REQUEST,
    payload: data
  };
}
