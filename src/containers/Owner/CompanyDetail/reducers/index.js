/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { GET_COMPANY_DETAIL_SUCCESS } from 'containers/Owner/CompanyDetail/constants';

export const initialState = {
  id: '',
  name: '',
  phone: '',
  type: '',
  url: '',
  address: '',
  description: '',
  users: []
};

function ownerCompanyDetailReducer(state = initialState, action) {
  switch (action.type) {
    case GET_COMPANY_DETAIL_SUCCESS: {
      const {
        address,
        description,
        id,
        name,
        phone,
        type,
        url,
        users: { data }
      } = action.payload.data;

      return {
        ...state,
        address,
        description,
        id,
        name,
        type,
        url,
        phone,
        users: data
      };
    }
    default:
      return state;
  }
}

export default ownerCompanyDetailReducer;
