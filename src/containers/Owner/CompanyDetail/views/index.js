import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import Breadcrumbs from 'components/Breadcrumbs';
import BreadCrumbItem from 'components/BreadcrumbItem';
import messages from 'components/OwnerComponents/Company/messages';
import OwnerCompaniesDetail from 'components/OwnerComponents/CompanyDetail';
import { getCompanyDetail, updateCompanyDetail } from 'containers/Owner/CompanyDetail/actions';
import messagesCommon from 'translations/messagesCommon';

export class OwnerCompanyDetail extends React.Component {
  componentDidMount() {
    const { match: { params: { id } } } = this.props;
    this.props.getCompanyDetailRequest(id);
  }

  render() {
    const {
      company,
      isFetching
    } = this.props;

    return [
      <Breadcrumbs icon={<i className="fa fa-building"/>} key='Breadcrumbs' headerTitle={intl.formatMessage(messages.company_detail)}>
        <BreadCrumbItem to="/owner" title={intl.formatMessage(messagesCommon.home)} icon="fa-dashboard"/>
        <BreadCrumbItem to="/owner/companies" title={intl.formatMessage(messages.company)}/>
        <BreadCrumbItem active title={intl.formatMessage(messages.company_detail)}/>
      </Breadcrumbs>,
      <OwnerCompaniesDetail
        key='CompanyList'
        data={company}
        isFetching={isFetching}
        submitEditCompany={this.props.submitEditCompany}
        search={this.search}
      />
    ];
  }
}

OwnerCompanyDetail.propTypes = {
  company: PropTypes.object,
  isFetching: PropTypes.bool,
  match: PropTypes.object,
  getCompanyDetailRequest: PropTypes.func,
  getCompanyRequest: PropTypes.func,
  submitEditCompany: PropTypes.func
};

export function mapDispatchToProps(dispatch) {
  return {
    getCompanyDetailRequest: (request) => {
      dispatch(getCompanyDetail(request));
    },
    submitEditCompany: (id, data) => {
      dispatch(updateCompanyDetail({
        id,
        data
      }));
    }
  };
}

const mapStateToProps = ({ companyDetail, global: { isFetching } }) => ({
  isFetching,
  company: companyDetail
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);


export default compose(
  withConnect
)(OwnerCompanyDetail);
