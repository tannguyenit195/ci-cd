/**
 * Root saga manages watcher lifecycle
 */
import ApiService from 'utils/axios';
import { call, put, takeLatest } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { getCompanyDetailError, getCompanyDetailSuccess } from 'containers/Owner/CompanyDetail/actions';
import { GET_COMPANY_DETAIL_REQUEST, UPDATE_COMPANY_DETAIL_REQUEST } from 'containers/Owner/CompanyDetail/constants';
import toastr from 'helpers/notifications';

function* getCompanyDetail(action) {
  const { payload } = action;
  const path = encodeURI(`/organizations/${payload}?fields=name,url,phone,type,description,address,users{email,is_admin.search(1)}`);
  try {
    const { status, data } = yield call(ApiService.get, path);
    if (status === 200) {
      yield put(getCompanyDetailSuccess(data));
    }
  } catch (e) {
    const { response } = { ...e };
    toastr.error(response.data.error.message);
    yield put(getCompanyDetailError(response));
  }
}

function* updateCompanyDetail(action) {
  const { payload: { id, data } } = action;
  const path = `/organizations/${id}`;
  try {
    const response = yield call(ApiService.put, path, data);
    if (response.status === 200) {
      yield put(push('/owner/companies'));
    }
  } catch (e) {
    const { response } = { ...e };
    toastr.error(response.data.error.message);
    yield put(getCompanyDetailError(response));
  }
}

export default function* ownerCompanyDetailWatch() {
  yield takeLatest(GET_COMPANY_DETAIL_REQUEST, getCompanyDetail);
  yield takeLatest(UPDATE_COMPANY_DETAIL_REQUEST, updateCompanyDetail);
}
