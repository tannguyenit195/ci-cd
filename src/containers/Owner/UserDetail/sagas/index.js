import { takeLatest, put, call } from 'redux-saga/effects';

import ApiService from 'utils/axios';
import { GET_USER_DETAIL_REQUEST } from '../constants';
import { getUserDetailSuccess, getUserDetailFailed } from '../actions';
import { FIELDS } from 'utils/constants';
import toastr from 'helpers/notifications';

function* getUserDetailSaga(action) {
  const { userId } = action.payload;
  const path = `/users/${userId}?fields=${FIELDS.USERS}`;

  try {
    const { status, data: { data } } = yield call(ApiService.get, path);
    if (status === 200) {
      const { roles, ...rest } = data;
      yield put(getUserDetailSuccess({
        ...rest,
        display_name: roles.data[0].display_name
      }));
    }
  } catch (err) {
    const { response } = { ...err };
    toastr.error(response.data.error.message);
    yield put(getUserDetailFailed(response.data.error.message));
  }
}

export default function* ownerDroneDetailWatch() {
  yield takeLatest(GET_USER_DETAIL_REQUEST, getUserDetailSaga);
}
