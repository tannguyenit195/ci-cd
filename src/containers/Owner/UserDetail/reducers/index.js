import { GET_USER_DETAIL_SUCCESS } from '../constants';

const userDetail = null;

function userDetailReducer(state = userDetail, action) {
  switch (action.type) {
    case GET_USER_DETAIL_SUCCESS: {
      return action.payload.data;
    }
    default:
      return state;
  }
}

export default userDetailReducer;
