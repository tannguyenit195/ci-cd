import {
  GET_USER_DETAIL_REQUEST,
  GET_USER_DETAIL_SUCCESS,
  GET_USER_DETAIL_FAILED
} from '../constants';

export function getUserDetailRequest(params) {
  return {
    type: GET_USER_DETAIL_REQUEST,
    payload: { ...params }
  };
}

export function getUserDetailSuccess(data) {
  return {
    type: GET_USER_DETAIL_SUCCESS,
    payload: { data }
  };
}

export function getUserDetailFailed(error) {
  return {
    type: GET_USER_DETAIL_FAILED,
    payload: { error }
  };
}
