import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { People } from '@material-ui/icons';

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messages from 'components/OwnerComponents/Users/messages';

import Breadcrumbs from 'components/Breadcrumbs';
import BreadCrumbItem from 'components/BreadcrumbItem';
import UserDetailComponent from 'components/OwnerComponents/Users/UserDetail';
import { getUserDetailRequest } from '../actions';
import messagesCommon from 'translations/messagesCommon';

class UserDetail extends React.Component {
  componentDidMount() {
    const { match: { params }, getUserDetailRequest } = this.props;
    getUserDetailRequest(params)
  }

  goBack = () => this.props.history.goBack();

  render() {
    const { user } = this.props;

    return [
      <Breadcrumbs icon={<People/>} key='Breadcrumbs' headerTitle={intl.formatMessage(messages.user_detail)}>
        <BreadCrumbItem to="/" title={intl.formatMessage(messagesCommon.home)} icon="fa-dashboard"/>
        <BreadCrumbItem to="" title={intl.formatMessage(messages.user_list)}/>
        <BreadCrumbItem active title={intl.formatMessage(messages.user_detail)}/>
      </Breadcrumbs>,
      <UserDetailComponent
        key="UserDetailComponent"
        user={user}
        goBack={this.goBack}
      />
    ];
  }
}

UserDetail.propTypes = {
  match: PropTypes.object,
  history: PropTypes.object,
  user: PropTypes.object,
  getUserDetailRequest: PropTypes.func
};

const mapDispatchToProps = (dispatch) => ({
  getUserDetailRequest: (droneId) => dispatch(getUserDetailRequest(droneId))
});

const mapStateToProps = (state) => ({
  user: state.userDetail
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default withConnect(UserDetail);
