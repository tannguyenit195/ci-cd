import {
  GET_LOG_DETAIL_REQUEST,
  GET_LOG_DETAIL_SUCCESS,
  GET_LOG_DETAIL_FAILED
} from '../constants';

export function getLogDetailRequest(id) {
  return {
    type: GET_LOG_DETAIL_REQUEST,
    payload: { id }
  };
}

export function getLogDetailSuccess(data) {
  return {
    type: GET_LOG_DETAIL_SUCCESS,
    payload: { data }
  };
}

export function getLogDetailFailed(error) {
  return {
    type: GET_LOG_DETAIL_FAILED,
    payload: { error }
  };
}
