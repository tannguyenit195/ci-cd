import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messages from 'components/OwnerMissions/messages';

import Breadcrumbs from 'components/Breadcrumbs';
import BreadCrumbItem from 'components/BreadcrumbItem';
import LogDetail from 'components/OwnerMissions/Logs';
import { getLogDetailRequest } from '../actions';
import messagesCommon from 'translations/messagesCommon';

export class OwnerLogs extends React.Component {
  state = {
    search: ''
  };

  componentDidMount() {
    const {
      history: { location: { pathname } },
      getLogDetail
    } = this.props;
    const companyId = pathname.split('/')[3];

    getLogDetail(companyId);
  }

  render() {
    const { history } = this.props;

    return [
      <Breadcrumbs icon={<i className="fa fa-history" aria-hidden="true"/>} key='Breadcrumbs' headerTitle={intl.formatMessage(messages.log_detail)}>
        <BreadCrumbItem to="/owner" title={intl.formatMessage(messagesCommon.home)} icon="fa-dashboard"/>
        <BreadCrumbItem active title={intl.formatMessage(messages.log_detail)}/>
      </Breadcrumbs>,
      <LogDetail
        key='LogDetail'
        history={history}
      />
    ];
  }
}

OwnerLogs.propTypes = {
  history: PropTypes.object,
  getLogDetail: PropTypes.func
};

const mapDispatchToProps = (dispatch) => ({
  getLogDetail: (companyId) => dispatch(getLogDetailRequest(companyId))
});

const mapStateToProps = (state) => ({ state });

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default withConnect(OwnerLogs);
