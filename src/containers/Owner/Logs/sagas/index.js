import { takeLatest } from 'redux-saga/effects';

// import ApiService from 'utils/axios';
import { GET_FLIGHT_LIST_SUCCESS } from '../constants';
// import { getDroneListFailed, getDroneListSuccess } from '../actions';
// import { FIELDS, PAGE_LIMIT } from 'utils/constants';

function getDronesSage(action) {
  console.log('>> getDronesSage', action)
  // const { page = 1, limit = PAGE_LIMIT, fields = FIELDS.USERS } = action.request || {};
  // const path = `/users?page=${page}&limit=${limit}&fields=${fields}`;
  // try {
  //   const { status, data } = yield call(ApiService.get, path);
  //   if (status === 200) {
  //     yield put(getDroneListSuccess(data.data));
  //   }
  // } catch (err) {
  //   const { response } = { ...err };
  //   console.log('>>> getDronesSage error', response);
  //   yield put(getDroneListFailed(response.data.error.message));
  // }
}

export default function* ownerDronesWatch() {
  yield takeLatest(GET_FLIGHT_LIST_SUCCESS, getDronesSage);
}
