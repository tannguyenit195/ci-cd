import { takeLatest, put, call } from 'redux-saga/effects';
import ApiService from 'utils/axios';
import Cookie from 'utils/CookieHelper';

import {
  OWNER_LOGIN_REQUEST,
  OWNER_RESET_PASSWORD_REQUEST,
  OWNER_CHANGE_PASSWORD_REQUEST
} from '../constants';
import {
  ownerLoginFailed,
  ownerResetPasswordSuccess,
  ownerResetPasswordFailed,
  ownerChangePasswordSuccess,
  ownerChangePasswordFailed
} from '../action';
import { getOwnerInfoRequest } from 'containers/App/action';
import toastr from 'helpers/notifications';

function* ownerLoginSaga(action) {
  try {
    const { data: { data } } = yield call(ApiService.post, '/login', action.payload);

    if (data) {
      const { access_token, time_expire = 1 } = data;
      Cookie.setCookie('micro_token', access_token, time_expire);
      ApiService.setHeader(access_token);
      window.location.href = '/';
      yield put(getOwnerInfoRequest());
    }
  } catch (e) {
    const { response } = { ...e };
    toastr.error(response.data.error.message);
    yield put(ownerLoginFailed(response.data.error.message));
  }
}

function* ownerResetPasswordSaga(action) {
  try {
    const { email, callback } = action.payload;
    const dataReq = {
      email,
      type: 'owner',
      device: 'web'
    };
    const { data } = yield call(ApiService.post, '/passwords/email', dataReq);

    if (data) {
      yield put(ownerResetPasswordSuccess());
      callback();
    }
  } catch (e) {
    const { response } = { ...e };
    yield put(ownerResetPasswordFailed(response.data.error.message));
  }
}

function* ownerChangePasswordSaga(action) {
  try {
    const { data: { token, ...dataBody }, callback } = action.payload;
    const { data } = yield call(ApiService.post, `/passwords/reset/${token}`, dataBody);

    if (data) {
      yield put(ownerChangePasswordSuccess());
      callback();
    }
  } catch (e) {
    const { response } = { ...e };
    yield put(ownerChangePasswordFailed(response.data.error.message));
  }
}

export default function* ownerAuthWatch() {
  yield takeLatest(OWNER_LOGIN_REQUEST, ownerLoginSaga);
  yield takeLatest(OWNER_RESET_PASSWORD_REQUEST, ownerResetPasswordSaga);
  yield takeLatest(OWNER_CHANGE_PASSWORD_REQUEST, ownerChangePasswordSaga);
}
