import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { ownerLoginRequest, ownerResetPasswordRequest, ownerChangePasswordRequest } from './action';
import AuthLayout from 'layouts/AuthLayout';
import LoginForm from 'components/LoginForm';
import ResetPasswordForm from 'components/ResetPasswordForm';
import { ROLE_TYPE } from 'utils/constants';

class OwnerAuthor extends Component {
  handleLogin = data => this.props.loginSubmit({
    ...data,
    role: 'owner'
  });

  handleResetPassword = (email, callback) => this.props.resetPasswordSubmit(email, callback);

  handleChangePassword = (password, callback) => this.props.changePassword(password, callback);

  render() {
    const { history, error, isFetching } = this.props;
    const isLogin = history.location.pathname.includes('login');
    const isInputPassword = history.location.pathname.includes('/reset-password');

    return (
      <AuthLayout className="AuthLayout" type={ROLE_TYPE.OWNER}>
        {isLogin
          ? <LoginForm
            submit={this.handleLogin}
            error={error}
            isFetching={isFetching}
            type={ROLE_TYPE.OWNER}
          />
          : <ResetPasswordForm
            isInputPassword={isInputPassword}
            submit={isInputPassword ? this.handleChangePassword : this.handleResetPassword}
            error={error}
            history={history}
            isFetching={isFetching}
            type={ROLE_TYPE.OWNER}
          />
        }
      </AuthLayout>
    );
  }
}

OwnerAuthor.propTypes = {
  isFetching: PropTypes.bool,
  error: PropTypes.string,
  user: PropTypes.any,
  history: PropTypes.any,
  loginSubmit: PropTypes.func,
  changePassword: PropTypes.func,
  resetPasswordSubmit: PropTypes.func
};

const mapDispatchToProps = dispatch => ({
  loginSubmit: data => dispatch(ownerLoginRequest(data)),
  resetPasswordSubmit: (email, callback) => dispatch(ownerResetPasswordRequest(email, callback)),
  changePassword: (data, callback) => dispatch(ownerChangePasswordRequest(data, callback))
});

const withConnect = connect(
  state => ({
    isFetching: state.global.isFetching,
    error: state.global.error,
    user: state.global.user
  }),
  mapDispatchToProps
);

export default withConnect(OwnerAuthor);
