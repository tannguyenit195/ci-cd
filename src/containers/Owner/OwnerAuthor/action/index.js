import {
  OWNER_LOGIN_REQUEST,
  OWNER_LOGIN_SUCCESS,
  OWNER_LOGIN_FAILED,
  OWNER_RESET_PASSWORD_REQUEST,
  OWNER_RESET_PASSWORD_SUCCESS,
  OWNER_RESET_PASSWORD_FAILED,
  OWNER_CHANGE_PASSWORD_REQUEST,
  OWNER_CHANGE_PASSWORD_SUCCESS,
  OWNER_CHANGE_PASSWORD_FAILED
} from '../constants';

export const ownerChangePasswordRequest = (data, callback) => ({
  type: OWNER_CHANGE_PASSWORD_REQUEST,
  payload: {
    data,
    callback
  }
});

export const ownerChangePasswordSuccess = () => ({
  type: OWNER_CHANGE_PASSWORD_SUCCESS
});

export const ownerChangePasswordFailed = error => ({
  type: OWNER_CHANGE_PASSWORD_FAILED,
  payload: { error }
});

export const ownerLoginRequest = data => ({
  type: OWNER_LOGIN_REQUEST,
  payload: {
    ...data
  }
});

export const ownerLoginSuccess = () => ({
  type: OWNER_LOGIN_SUCCESS
});

export const ownerLoginFailed = error => ({
  type: OWNER_LOGIN_FAILED,
  payload: {
    error
  }
});

export const ownerResetPasswordRequest = (email, callback) => ({
  type: OWNER_RESET_PASSWORD_REQUEST,
  payload: {
    email,
    callback
  }
});

export const ownerResetPasswordSuccess = () => ({
  type: OWNER_RESET_PASSWORD_SUCCESS
});

export const ownerResetPasswordFailed = error => ({
  type: OWNER_RESET_PASSWORD_FAILED,
  payload: {
    error
  }
});
