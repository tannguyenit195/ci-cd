/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';

export class Home extends React.Component {
  render() {
    const utmControlURL = process.env.UTM_CONTROL_URL;
    return (
      <article className="home-admin-article">
        <Helmet>
          <title>Home Page</title>
          <meta
            name="description"
            content="A React.js Boilerplate application homepage"
          />
        </Helmet>
        <div id="app-layout">
          <main>
            <div className="container-fluid">
              <div className="row">
                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 dashboard-content">
                  <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 block block-utm ">
                    <div className="container">
                      <a href={utmControlURL}>
                        <div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-utm-en col-sm-offset-3"/>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </main>
        </div>
      </article>
    );
  }
}

Home.propTypes = {
  loading: PropTypes.bool,
  user: PropTypes.object,
  history: PropTypes.any
};

export function mapDispatchToProps() {
  return {};
}

const mapStateToProps = (state) => ({
  user: state.global.user
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default withConnect(Home);
