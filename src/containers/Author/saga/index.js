import { takeLatest, put, call } from 'redux-saga/effects';
import Cookie from 'js-cookie';
import { push } from 'react-router-redux';

import ApiService from 'utils/axios';
import toast from 'helpers/notifications';
import {
  LOGIN_REQUEST,
  RESET_PASSWORD_REQUEST,
  CHANGE_PASSWORD_REQUEST
} from '../constants';
import {
  loginFailed,
  resetPasswordSuccess,
  resetPasswordFailed,
  changePasswordSuccess,
  changePasswordFailed
} from '../action';

const UNKNOW_ERROR = 'Unknow Error!!!';

function* loginSaga(action) {
  try {
    const { data: { data } } = yield call(ApiService.post, '/login', action.payload);
    console.log('>>>>>>>>> loginSaga data', data);
    if (data) {
      const { token: access_token, time_expire = 10, type = 'ADMIN' } = data;
      const redirectUrl = type === 'OWNER' ? '/owner' : '/';
      const expires = Math.floor(time_expire * (1 / 1440)); // days expired
      console.log(Cookie.set('micro_token', access_token, { expires }))
      Cookie.set('micro_token', access_token, { expires });
      Cookie.set('micro_role', type, { expires });
      ApiService.setHeader(access_token);
      yield put(push(redirectUrl));
    }
  } catch (e) {
    const { response: { data } } = { ...e };
    const message = (data && data.error && data.error && data.error.message) || UNKNOW_ERROR;
    toast.error(message);
    yield put(loginFailed(message));
  }
}

function* resetPasswordSaga(action) {
  try {
    const { email, callback } = action.payload;
    const dataReq = {
      email,
      type: 'auth',
      device: 'web'
    };
    const { data } = yield call(ApiService.post, '/passwords/email', dataReq);

    if (data) {
      yield put(resetPasswordSuccess());
      callback();
    }
  } catch (e) {
    const { response: { data } } = { ...e };
    const message = (data && data.error && data.error && data.error.message) || UNKNOW_ERROR;
    toast.error(message);
    yield put(resetPasswordFailed(message));
  }
}

function* changePasswordSaga(action) {
  try {
    const { data: { token, ...dataBody }, callback } = action.payload;
    const { data } = yield call(ApiService.post, `/passwords/reset/${token}`, dataBody);

    if (data) {
      yield put(changePasswordSuccess());
      callback();
    }
  } catch (e) {
    const { response: { data } } = { ...e };
    const message = (data && data.error && data.error && data.error.message) || UNKNOW_ERROR;
    toast.error(message);
    yield put(changePasswordFailed(message));
  }
}

export default function* authWatch() {
  yield takeLatest(LOGIN_REQUEST, loginSaga);
  yield takeLatest(RESET_PASSWORD_REQUEST, resetPasswordSaga);
  yield takeLatest(CHANGE_PASSWORD_REQUEST, changePasswordSaga);
}
