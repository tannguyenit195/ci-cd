import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Cookie from 'js-cookie';

import { loginRequest, resetPasswordRequest, changePasswordRequest } from './action';
import AuthLayout from 'layouts/AuthLayout';
import LoginForm from 'components/LoginForm';
import ResetPasswordForm from 'components/ResetPasswordForm';
import { ROLE_TYPE } from 'utils/constants';

class LoginContainer extends Component {
  handleLogin = data => this.props.loginSubmit(data);

  handleResetPassword = (email, callback) => this.props.resetPasswordSubmit(email, callback);

  handleChangePassword = (password, callback) => this.props.changePassword(password, callback);

  componentDidMount() {
    const { history } = this.props;
    const token = Cookie.get('micro_token');
    const role = Cookie.get('micro_role');

    if (token && role) {
      history.push(role === ROLE_TYPE.OWNER ? '/owner' : '/');
    }
  }

  render() {
    const { history, error, isFetching } = this.props;
    const isLogin = history.location.pathname.includes('login');
    const isInputPassword = history.location.pathname.includes('/reset-password');

    return (
      <AuthLayout className="AuthLayout">
        {isLogin
          ? <LoginForm
            submit={this.handleLogin}
            error={error}
            isFetching={isFetching}
          />
          : <ResetPasswordForm
            isInputPassword={isInputPassword}
            submit={isInputPassword ? this.handleChangePassword : this.handleResetPassword}
            error={error}
            history={history}
            isFetching={isFetching}
          />
        }
      </AuthLayout>
    );
  }
}

LoginContainer.propTypes = {
  isFetching: PropTypes.bool,
  error: PropTypes.string,
  user: PropTypes.any,
  history: PropTypes.any,
  loginSubmit: PropTypes.func,
  changePassword: PropTypes.func,
  resetPasswordSubmit: PropTypes.func
};

const mapDispatchToProps = dispatch => ({
  loginSubmit: data => dispatch(loginRequest(data)),
  resetPasswordSubmit: (email, callback) => dispatch(resetPasswordRequest(email, callback)),
  changePassword: (data, callback) => dispatch(changePasswordRequest(data, callback))
});

const withConnect = connect(
  state => ({
    isFetching: state.global.isFetching,
    error: state.global.error,
    user: state.global.user
  }),
  mapDispatchToProps
);

export default withConnect(LoginContainer);
