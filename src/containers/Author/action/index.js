import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  RESET_PASSWORD_REQUEST,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAILED,
  CHANGE_PASSWORD_REQUEST,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_FAILED
} from '../constants';

export const changePasswordRequest = (data, callback) => ({
  type: CHANGE_PASSWORD_REQUEST,
  payload: {
    data,
    callback
  }
});

export const changePasswordSuccess = () => ({
  type: CHANGE_PASSWORD_SUCCESS
});

export const changePasswordFailed = error => ({
  type: CHANGE_PASSWORD_FAILED,
  payload: { error }
});

export const loginRequest = data => ({
  type: LOGIN_REQUEST,
  payload: {
    ...data
  }
});

export const loginSuccess = () => ({
  type: LOGIN_SUCCESS
});

export const loginFailed = error => ({
  type: LOGIN_FAILED,
  payload: {
    error
  }
});

export const resetPasswordRequest = (email, callback) => ({
  type: RESET_PASSWORD_REQUEST,
  payload: {
    email,
    callback
  }
});

export const resetPasswordSuccess = () => ({
  type: RESET_PASSWORD_SUCCESS
});

export const resetPasswordFailed = error => ({
  type: RESET_PASSWORD_FAILED,
  payload: {
    error
  }
});
