/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  home: {
    id: 'components.Header.home',
    defaultMessage: 'Home'
  },
  features: {
    id: 'components.Header.features',
    defaultMessage: 'Features'
  },
  login_title: {
    id: 'auth.login.login_title',
    defaultMessage: 'LOG IN'
  },
  email: {
    id: 'commons.email',
    defaultMessage: 'Email'
  },
  email_address: {
    id: 'commons.email_address',
    defaultMessage: 'Email Address'
  },
  password: {
    id: 'commons.password',
    defaultMessage: 'Password'
  },
  password_confirmation: {
    id: 'commons.password_confirmation',
    defaultMessage: 'Confirm Password'
  },
  remember: {
    id: 'auth.login.remember',
    defaultMessage: 'Remember me'
  },
  forgot_password: {
    id: 'auth.login.forgot_password',
    defaultMessage: 'Forgot your password'
  },
  privacy: {
    id: 'auth.login.privacy',
    defaultMessage: 'Email'
  },
  term: {
    id: 'auth.login.term',
    defaultMessage: 'Term of Use'
  },
  operation: {
    id: 'auth.login.operation',
    defaultMessage: 'Operation Company Info'
  },
  the_given_data_was_invalid: {
    id: 'auth.login.the_given_data_was_invalid',
    defaultMessage: 'Email or password is incorrect!'
  },
  email_the_given_data_was_invalid: {
    id: 'auth.login.email_not_existed',
    defaultMessage: 'Email is not existed in the system'
  },
  server_login_password_wrong: {
    id: 'auth.login.server_login_password_wrong',
    defaultMessage: 'Email or password is incorrect!'
  },
  reset_password: {
    id: 'auth.login.reset_password',
    defaultMessage: 'Reset password',
  },
  send_button: {
    id: 'commons.send',
    defaultMessage: 'Send',
  },
  reset_password_success: {
    id: 'auth.login.reset_password_success',
    defaultMessage: 'We have e-mailed your password reset link!'
  }
});
