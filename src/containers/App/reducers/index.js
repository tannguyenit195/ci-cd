/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import {
  GET_USER_INFO_FAILED,
  GET_USER_INFO_SUCCESS,
  USER_LOGOUT_SUCCESS,
  USER_LOGOUT_FAILED,
  REQUEST_ERROR,
  TOGGLE_FETCHING,
  GET_OWNER_INFO_SUCCESS
} from 'containers/App/constants';
import { LOGIN_FAILED, RESET_PASSWORD_FAILED } from 'containers/Author/constants';

// The initial state of the App
const initialState = {
  isFetching: false,
  error: null,
  user: null,
  role: null
};

function appReducer(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_FETCHING: {
      const { isFetching } = action.payload;
      return {
        ...state,
        isFetching: isFetching,
        error: isFetching ? null : state.error
      };
    }
    case USER_LOGOUT_SUCCESS:
      return {
        error: null,
        user: null,
        role: null
      };
    case LOGIN_FAILED: {
      return {
        ...state,
        error: action.payload.error
      };
    }
    case GET_USER_INFO_SUCCESS:
      return {
        ...state,
        error: null,
        user: action.payload.user,
        role: action.payload.role
      };
    case GET_USER_INFO_FAILED: {
      return {
        ...state,
        error: action.payload.error
      };
    }
    case USER_LOGOUT_FAILED: {
      return {
        ...state,
        error: action.payload.error
      };
    }
    case RESET_PASSWORD_FAILED:
      return {
        ...state,
        error: `email_${action.payload.error}`
      };
    case REQUEST_ERROR:
      return {
        ...state,
        error: action.payload.error
      };
    case GET_OWNER_INFO_SUCCESS: {
      return {
        isFetching: false,
        error: null,
        role: action.payload.role,
        user: action.payload.user
      };
    }
    default:
      return state;
  }
}

export default appReducer;
