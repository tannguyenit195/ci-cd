import {
  GET_USER_INFO_FAILED,
  GET_USER_INFO_REQUEST,
  GET_USER_INFO_SUCCESS,
  GET_OWNER_INFO_FAILED,
  GET_OWNER_INFO_REQUEST,
  GET_OWNER_INFO_SUCCESS,
  USER_LOGOUT_REQUEST,
  USER_LOGOUT_SUCCESS,
  USER_LOGOUT_FAILED,
  REQUEST_ERROR,
  TOGGLE_FETCHING
} from 'containers/App/constants';

export const requestHasError = (error) => ({
  type: REQUEST_ERROR,
  payload: { error }
});

export const toggleFetching = (isFetching = false) => ({
  type: TOGGLE_FETCHING,
  payload: { isFetching }
});

export const userLogoutRequest = () => ({
  type: USER_LOGOUT_REQUEST
});

export const userLogoutSuccess = () => ({
  type: USER_LOGOUT_SUCCESS
});

export const userLogoutFailed = (error) => ({
  type: USER_LOGOUT_FAILED,
  payload: { error }
});

export const getUserInfoRequest = () => ({
  type: GET_USER_INFO_REQUEST
});

export const getUserInfoSuccess = (data) => ({
  type: GET_USER_INFO_SUCCESS,
  payload: { ...data }
});

export const getUserInfoFailed = (error) => ({
  type: GET_USER_INFO_FAILED,
  payload: { error }
});

export const getOwnerInfoRequest = () => ({
  type: GET_OWNER_INFO_REQUEST
});

export const getOwnerInfoSuccess = (data) => ({
  type: GET_OWNER_INFO_SUCCESS,
  payload: { ...data }
});

export const getOwnerInfoFailed = (error) => ({
  type: GET_OWNER_INFO_FAILED,
  payload: { error }
});
