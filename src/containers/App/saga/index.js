import { takeEvery, put, call } from 'redux-saga/effects';
import ApiService from 'utils/axios';
import Cookie from 'utils/CookieHelper';
import toastr from 'helpers/notifications';
import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import {
  GET_USER_INFO_REQUEST, USER_LOGOUT_REQUEST,
  GET_OWNER_INFO_REQUEST
} from 'containers/App/constants';
import {
  getOwnerInfoSuccess,
  getOwnerInfoFailed,
  getUserInfoSuccess,
  getUserInfoFailed,
  userLogoutSuccess,
  userLogoutFailed
} from '../action';
import { changeLocale } from 'containers/LanguageProvider/actions';
import msg from 'translations/notification';

const { formatMessage: trans } = intl;

function* getUserInfoSaga() {
  try {
    const { data: { data } } = yield call(ApiService.post, '/me');
    if (data) {
      const { user: { language } } = data;
      localStorage.setItem('lang', language === 'jp' ? 'ja' : language);
      yield put(getUserInfoSuccess(data));
      yield put(changeLocale(language));
    }
  } catch (e) {
    toastr.error(trans(msg.get_data_error));
    yield put(getUserInfoFailed('Get user info failed'));
  }
}

function* getOwnerInfoSaga() {
  try {
    const { data: { data } } = yield call(ApiService.post, '/me');
    if (data) {
      yield put(getOwnerInfoSuccess(data));
    }
  } catch (e) {
    toastr.error(trans(msg.get_data_error));
    yield put(getOwnerInfoFailed('Get owner info failed'));
  }
}

function* userLogoutSaga() {
  try {
    yield call(ApiService.post, '/logout');
    Cookie.removeCookie('micro_token');
    yield put(userLogoutSuccess());
  } catch (e) {
    const { response } = { ...e };
    toastr.error(trans(msg.something_error_request));
    yield put(userLogoutFailed(response.data.error.message));
  }
}

export default function* userWatch() {
  yield takeEvery(GET_USER_INFO_REQUEST, getUserInfoSaga);
  yield takeEvery(GET_OWNER_INFO_REQUEST, getOwnerInfoSaga);
  yield takeEvery(USER_LOGOUT_REQUEST, userLogoutSaga);
}
