/**
 *
 * App
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 */

import React, { Fragment } from 'react';
import { Helmet } from 'react-helmet';
import Routes from 'routes'

export default function App() {
  return (
    <Fragment>
      <Helmet titleTemplate="%s - An ....." defaultTitle="Terra Drone">
        <link rel="icon" href="https://service-dev.terra-utm.com/build/img/favicon/favicon.ico" sizes="32x32"/>
      </Helmet>
      <Routes />
    </Fragment>
  );
}
