/**
 * Root saga manages watcher lifecycle
 */
import { CREATE_USER_REQUEST } from 'containers/UserCreate/constants';
import ApiService from 'utils/axios';
import { call, put, takeLatest } from 'redux-saga/effects';
import { updateStatusCreateUser } from 'containers/UserCreate/actions';
import toastr from 'helpers/notifications';

function* createUser(action) {
  const path = '/users';
  try {
    const { status } = yield call(ApiService.post, path, action.payload);
    if (status === 200) {
      yield put(updateStatusCreateUser(true));
    }
  } catch (e) {
    const { response } = { ...e };
    toastr.error(response.data.error.message);
  }
}

export default function* userCreateWatch() {
  yield takeLatest(CREATE_USER_REQUEST, createUser);
}
