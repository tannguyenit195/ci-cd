
import { UPDATE_STATUS_CREATE_USER } from 'containers/UserCreate/constants';
import { CREATE_USER_REQUEST } from 'containers/UserCreate/constants';

export function updateStatusCreateUser(status) {
  return {
    type: UPDATE_STATUS_CREATE_USER,
    payload: status
  };
}

export function createUser(payload) {
  return {
    type: CREATE_USER_REQUEST,
    payload
  };
}
