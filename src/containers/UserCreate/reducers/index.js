/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */

import { UPDATE_STATUS_CREATE_USER } from 'containers/UserCreate/constants';

export const initialState = {
  createUserSuccess: false
};

function userCreateReducer(state = initialState, action) {
  switch (action.type) {
    case UPDATE_STATUS_CREATE_USER: {
      return {
        createUserSuccess: !state.createUserSuccess
      };
    }
    default:
      return state;
  }
}

export default userCreateReducer;
