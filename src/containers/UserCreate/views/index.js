/*
 * UsersPage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import { withRouter } from 'react-router-dom';
import { createUser } from 'containers/UserCreate/actions';
import Breadcrumbs from 'components/Breadcrumbs';
import BreadCrumbItem from 'components/BreadcrumbItem';
import messages from 'translations/messagesCommon';
import UserCreateComponent from 'components/UserCreate';
import { updateStatusCreateUser } from 'containers/UserCreate/actions';
import { PersonAdd } from '@material-ui/icons';
import messagesCommon from 'translations/messagesCommon'

export class UserCreate extends React.Component {
  componentWillReceiveProps(nextProps) {
    const { history, status: { createUserSuccess } } = nextProps;
    if (createUserSuccess === true) {
      history.push('/users');
      this.props.updateStatusCreateUser(false);
    }
  }

  render() {
    return [
      <Breadcrumbs icon={<PersonAdd/> } key='Breadcrumbs' headerTitle={intl.formatMessage(messages.users)}>
        <BreadCrumbItem to="/" title={intl.formatMessage(messagesCommon.home)} icon="fa-dashboard"/>
        <BreadCrumbItem to="/users" title={intl.formatMessage(messages.users)}/>
        <BreadCrumbItem active title={intl.formatMessage(messages.create)}/>
      </Breadcrumbs>,
      <UserCreateComponent
        key='UserCreate'
        auth={this.props.auth}
        createUser={this.props.createUser}
      />
    ];
  }
}

UserCreate.propTypes = {
  updateStatusCreateUser: PropTypes.func,
  createUser: PropTypes.func,
  auth: PropTypes.object,
  history: PropTypes.object,
  status: PropTypes.object
};

export function mapDispatchToProps(dispatch) {
  return {
    createUser: (data) => {
      dispatch(createUser(data));
    },
    updateStatusCreateUser: (status) => {
      dispatch(updateStatusCreateUser(status));
    }
  };
}

const mapStateToProps = (state) => ({
  auth: state.global.user,
  status: state.userCreate
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

export default compose(
  withConnect,
  withRouter
)(UserCreate);
