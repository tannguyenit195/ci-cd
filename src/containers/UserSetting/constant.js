const UPDATE_USER_REQUEST = 'USER_SETTING/UPDATE_REQUEST';
const UPDATE_USER_SUCCESS = 'USER_SETTING/UPDATE_SUCCESS';
const UPDATE_USER_FAILED = 'USER_SETTING/UPDATE_FAILED';

export {
  UPDATE_USER_REQUEST,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILED
};
