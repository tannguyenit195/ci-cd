import { call, put, takeLatest } from 'redux-saga/effects';

import ApiService from 'utils/axios';
import { UPDATE_USER_REQUEST } from './constant';
import { getUserInfoRequest } from 'containers/App/action';
import { updateUserSuccess, updateUserFailed } from 'containers/UserSetting/actions';
import toastr from 'helpers/notifications';

function* updateUserSaga(action) {
  try {
    const { id, ...dataBody } = action.payload;
    const { data } = yield call(ApiService.put, `/users/${id}`, dataBody);

    if (data.data) {
      yield put(updateUserSuccess());
      yield put(getUserInfoRequest());
    }
  } catch (e) {
    const { response } = { ...e };
    toastr.error(response.data.error.message);
    yield put(updateUserFailed(response.data.error.message));
  }
}

export default function* userSettingWatcher() {
  yield takeLatest(UPDATE_USER_REQUEST, updateUserSaga);
}
