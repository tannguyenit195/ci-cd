/*
 * Company Container Messages
 *
 * This contains all the text for the Users Container component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({});
