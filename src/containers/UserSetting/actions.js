import {
  UPDATE_USER_REQUEST,
  UPDATE_USER_SUCCESS,
  UPDATE_USER_FAILED
} from './constant';

export const updateUserRequest = (data) => ({
  type: UPDATE_USER_REQUEST,
  payload: { ...data }
});

export const updateUserSuccess = () => ({
  type: UPDATE_USER_SUCCESS
});

export const updateUserFailed = (error) => ({
  type: UPDATE_USER_FAILED,
  payload: { error }
});
