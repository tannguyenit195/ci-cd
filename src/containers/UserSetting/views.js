import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { AccountCircle } from '@material-ui/icons';

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messagesCommon from 'translations/messagesCommon';
import { updateUserRequest } from './actions';
import UserSettingComponent from 'components/UserSettingComponent';
import Breadcrumbs from 'components/Breadcrumbs';
import BreadcrumbItem from 'components/BreadcrumbItem';

class UserSetting extends Component {
  static propTypes = {
    isFetching: PropTypes.bool,
    user: PropTypes.object,
    submitUser: PropTypes.func
  };

  render() {
    const { user, submitUser, isFetching } = this.props;

    return [
      <Breadcrumbs key='Breadcrumbs' icon={<AccountCircle/>} headerTitle={intl.formatMessage(messagesCommon.user_profile)}>
        <BreadcrumbItem to="/owner" title={intl.formatMessage(messagesCommon.home)} icon="fa-dashboard"/>
        <BreadcrumbItem active title={intl.formatMessage(messagesCommon.user_profile)}/>
      </Breadcrumbs>,
      <UserSettingComponent
        key="UserSettingComponent"
        user={user}
        submitUser={submitUser}
        isFetching={isFetching}
      />
    ];
  }
}

const mapStateToProps = ({ global: { isFetching, user } }) => ({
  isFetching,
  user
});

const mapDispatchToProps = dispatch => ({
  submitUser: (data) => dispatch(updateUserRequest(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(UserSetting);
