import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TextField, Button } from '@material-ui/core';
import { Business } from '@material-ui/icons';
import { connect } from 'react-redux';

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messagesCommon from 'translations/messagesCommon';
import messages from './messages';
import { fetchCompanyRequest, updateCompanyRequest } from 'containers/Company/actions';
import Breadcrumbs from 'components/Breadcrumbs';
import BreadCrumbItem from 'components/BreadcrumbItem';
import SectionContent from 'components/SectionContent';

class Company extends Component {
  static propTypes = {
    isFetching: PropTypes.bool,
    company: PropTypes.object,
    user: PropTypes.object,
    fetchCompanyRequest: PropTypes.func,
    submitEditCompany: PropTypes.func
  };

  constructor(props) {
    super(props);
    this.state = {
      ...props.company,
      isEdit: false
    };
    this.isRequested = false;
    this.onChangeInput = this.onChangeInput.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    const { company, user, fetchCompanyRequest } = nextProps;

    if (!!user && !this.isRequested) {
      fetchCompanyRequest(user.organization_id);
      this.isRequested = true;
    }

    !!company && this.setState({ ...company });
    !!user && this.setState({ email: user.email });
  }

  onChangeInput(event) {
    const { id, value } = event.target;
    this.setState({ [id]: value });
  }

  handleCancel() {
    this.setState({
      ...this.props.company,
      isEdit: false
    });
  }

  handleEdit() {
    const {
      isEdit, id: organizationId, name,
      address, country, description, phone, url
    } = this.state;
    const { submitEditCompany } = this.props;

    if (isEdit) {
      submitEditCompany(organizationId, {
        name: name || '',
        address: address || '',
        country: country || '',
        description: description || '',
        phone: phone || '',
        url: url || ''
      });
      this.setState({ isEdit: false });
    } else {
      this.setState({ isEdit: true });
    }
  }

  renderTextArea() {
    const { isEdit, description } = this.state;

    return (
      <div className="inputs form-group">
        <label htmlFor="description">
          {intl.formatMessage(messagesCommon.note)}
        </label>
        <textarea
          id="description"
          className="form-control"
          disabled={!isEdit}
          value={description || ''}
          onChange={this.onChangeInput}
        />
      </div>
    );
  };

  renderInput(id, type, label, value) {
    const { isEdit } = this.state;

    return (
      <div className="inputs form-group">
        <label htmlFor={id}>{label}</label>
        <TextField
          className="form-control"
          disabled={!isEdit || type === 'email'}
          type={type}
          inputProps={{
            className: 'input-cus',
            id: id
          }}
          value={value || ''}
          onChange={this.onChangeInput}
          variant={isEdit ? 'outlined' : 'standard'}
        />
      </div>
    );
  };

  renderActions() {
    const { isEdit } = this.state;
    const { isFetching } = this.props;
    const text = intl.formatMessage(messagesCommon[isEdit ? 'save' : 'edit']);
    const elm = isFetching ? <i className="fa fa-spin fa-spinner"/> : text;

    return (
      <div className="actions">
        <Button
          variant="contained"
          size="large"
          onClick={this.handleCancel}
          disabled={!isEdit}
        >
          {intl.formatMessage(messagesCommon.cancel)}
        </Button>
        <Button
          variant="contained"
          color="primary"
          size="large"
          onClick={this.handleEdit}
          disabled={isFetching}
        >
          {elm}
        </Button>
      </div>
    );
  }

  render() {
    const { name, address, phone, email, url } = this.state;
    return (
      <div>
        <Breadcrumbs icon={<Business/>} headerTitle={intl.formatMessage(messagesCommon.company)}>
          <BreadCrumbItem to="/" title={intl.formatMessage(messagesCommon.home)} icon="fa-dashboard"/>
          <BreadCrumbItem active title={intl.formatMessage(messagesCommon.company)}/>
        </Breadcrumbs>
        <SectionContent title={intl.formatMessage(messagesCommon.company_setting)}>
          <div className="box-body">
            <div className="col-md-6">
              {this.renderInput('name', 'text', intl.formatMessage(messages.company_name), name)}
              {this.renderInput('address', 'text', intl.formatMessage(messages.company_address), address)}
              {this.renderInput('phone', 'text', intl.formatMessage(messages.company_tel), phone)}
              {this.renderInput('email', 'email', intl.formatMessage(messagesCommon.email), email)}
              {this.renderInput('url', 'text', intl.formatMessage(messagesCommon.url), url)}
            </div>
            <div className="col-md-6">
              {this.renderTextArea()}
            </div>
          </div>
          <div className="box-footer text-center">
            {this.renderActions()}
          </div>
        </SectionContent>
      </div>
    );
  }
}

const mapStateToProps = ({ company, global: { isFetching, user } }) => ({
  isFetching,
  company,
  user
});

const mapDispatchToProps = dispatch => ({
  fetchCompanyRequest: (id) => dispatch(fetchCompanyRequest(id)),
  submitEditCompany: (id, data) => dispatch(updateCompanyRequest(id, data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Company);
