import { FETCH_COMPANY_SUCCESS } from './constant';

function companyReducer(state = {}, action) {
  switch (action.type) {
    case FETCH_COMPANY_SUCCESS:
      return {
        ...action.payload.data
      };
    default:
      return state;
  }
}

export default companyReducer;
