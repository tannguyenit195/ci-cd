import {
  FETCH_COMPANY_REQUEST, FETCH_COMPANY_SUCCESS, FETCH_COMPANY_FAILED,
  UPDATE_COMPANY_REQUEST, UPDATE_COMPANY_SUCCESS, UPDATE_COMPANY_FAILED
} from './constant';

export const fetchCompanyRequest = (organizationId) => ({
  type: FETCH_COMPANY_REQUEST,
  payload: { organizationId }
});

export const fetchCompanySuccess = (data) => ({
  type: FETCH_COMPANY_SUCCESS,
  payload: { data }
});

export const fetchCompanyFailed = (error) => ({
  type: FETCH_COMPANY_FAILED,
  payload: { error }
});

export const updateCompanyRequest = (organizationId, data) => ({
  type: UPDATE_COMPANY_REQUEST,
  payload: { organizationId, ...data }
});

export const updateCompanySuccess = () => ({
  type: UPDATE_COMPANY_SUCCESS
});

export const updateCompanyFailed = (error) => ({
  type: UPDATE_COMPANY_FAILED,
  payload: { error }
});
