import { call, put, takeLatest } from 'redux-saga/effects';

import ApiService from 'utils/axios';
import {
  FETCH_COMPANY_REQUEST, UPDATE_COMPANY_REQUEST
} from 'containers/Company/constant';
import {
  fetchCompanyRequest, fetchCompanyFailed, fetchCompanySuccess,
  updateCompanyFailed, updateCompanySuccess
} from 'containers/Company/actions';
import toastr from 'helpers/notifications';

function* fetchCompanySaga(action) {
  try {
    const { organizationId } = action.payload;
    const url = encodeURI(`/organizations/${organizationId}?fields=thumbnail_id,name,type,description,country,address,phone,url,email`);
    const { data } = yield call(ApiService.get, url);

    if (data.data) {
      yield put(fetchCompanySuccess(data.data));
    }
  } catch (e) {
    const { response } = { ...e };
    toastr.error(response.data.error.message);
    yield put(fetchCompanyFailed(response.data.error.message));
  }
}

function* updateCompanySaga(action) {
  try {
    const { organizationId, ...dataBody } = action.payload;
    const url = `/organizations/${organizationId}`;
    const { data } = yield call(ApiService.put, url, dataBody);

    if (data.data) {
      yield put(updateCompanySuccess(data.data));
      yield put(fetchCompanyRequest(organizationId));
    }
  } catch (e) {
    const { response } = { ...e };
    toastr.error(response.data.error.message);
    yield put(updateCompanyFailed(response.data.error.message));
  }
}

export default function* companyWatcher() {
  yield takeLatest(FETCH_COMPANY_REQUEST, fetchCompanySaga);
  yield takeLatest(UPDATE_COMPANY_REQUEST, updateCompanySaga);
}
