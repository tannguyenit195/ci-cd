/*
 * Company Container Messages
 *
 * This contains all the text for the Users Container component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  company_name: {
    id: 'components.Company.company_name',
    defaultMessage: 'Company Name'
  },
  company_address: {
    id: 'components.Company.company_address',
    defaultMessage: 'Company Address'
  },
  company_tel: {
    id: 'components.Company.company_tel',
    defaultMessage: 'Company Tel'
  },
});
