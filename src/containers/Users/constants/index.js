/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const GET_USERS_LIST_REQUEST = 'Users/GET_USERS_LIST_REQUEST';
export const DELETE_USER_REQUEST = 'Users/DELETE_USER_REQUEST';
export const UPDATE_USER_REQUEST = 'Users/UPDATE_USER_REQUEST';
export const OPEN_DIALOG = 'Users/OPEN_DIALOG';
export const USER_DIALOG = 'Users/USER_DIALOG';
export const DELETE_DIALOG = 'Users/DELETE_DIALOG';
export const GET_USERS_LIST_SUCCESS = 'Users/GET_USERS_LIST_SUCCESS';
export const GET_USERS_LIST_ERROR = 'Users/GET_USERS_LIST_ERROR';
