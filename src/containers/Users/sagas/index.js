/**
 * Root saga manages watcher lifecycle
 */
import { DELETE_DIALOG, DELETE_USER_REQUEST, GET_USERS_LIST_REQUEST, UPDATE_USER_REQUEST, USER_DIALOG } from 'containers/Users/constants';
import ApiService from 'utils/axios';
import { call, put, takeLatest } from 'redux-saga/effects';
import { getUserList, getUserListError, getUserListSuccess, openDialog } from 'containers/Users/actions';
import { FIELDS, PAGE_LIMIT } from 'utils/constants';
import toastr from 'helpers/notifications';

function* listUser(action) {
  const { page = 1, limit = PAGE_LIMIT, fields = FIELDS.USERS } = action.request || {};
  const path = `/users?page=${page}&limit=${limit}&fields=${fields}`;
  try {
    const { status, data } = yield call(ApiService.get, path);
    if (status === 200) {
      yield put(getUserListSuccess(data));
    }
  } catch (err) {
    yield put(getUserListError());
  }
}

function* deleteUser(action) {
  const { id, page } = action.payload;
  const path = `/users/${id}`;
  const request = { page };

  try {
    const { status } = yield call(ApiService.delete, path);
    if (status === 200) {
      yield put(getUserList(request));
      yield put(openDialog(DELETE_DIALOG));
    }
  } catch (e) {
    const { response } = { ...e };
    toastr.error(response.data.error.message);
    yield put(getUserListError());
  }
}

function* updateUser(action) {
  const {
    payload: {
      id, firstName, lastName, phone, password, page
    }
  } = action;
  const path = `/users/${id}`;
  const request = {
    first_name: firstName,
    last_name: lastName,
    phone
  };

  if (password !== '') {
    request.password = password;
  }

  try {
    const { status } = yield call(ApiService.put, path, request);
    if (status === 200) {
      const request = { page };
      yield put(getUserList(request));
      yield put(openDialog(USER_DIALOG));
    }
  } catch (err) {
    yield put(getUserListError());
  }
}

export default function* userListWatch() {
  yield takeLatest(GET_USERS_LIST_REQUEST, listUser);
  yield takeLatest(DELETE_USER_REQUEST, deleteUser);
  yield takeLatest(UPDATE_USER_REQUEST, updateUser);
}
