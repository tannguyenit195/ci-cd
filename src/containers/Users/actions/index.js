import {
  DELETE_USER_REQUEST,
  GET_USERS_LIST_ERROR,
  GET_USERS_LIST_REQUEST,
  GET_USERS_LIST_SUCCESS,
  OPEN_DIALOG,
  UPDATE_USER_REQUEST
} from 'containers/Users/constants';

export function getUserList(request) {
  return {
    type: GET_USERS_LIST_REQUEST,
    request
  };
}

export function getUserListSuccess(payload) {
  return {
    type: GET_USERS_LIST_SUCCESS,
    payload
  };
}

export function getUserListError() {
  return {
    type: GET_USERS_LIST_ERROR
  };
}

export function deleteUser(payload) {
  return {
    type: DELETE_USER_REQUEST,
    payload
  };
}

export function updateUser(payload) {
  return {
    type: UPDATE_USER_REQUEST,
    payload
  };
}

export function openDialog(payload) {
  return {
    type: OPEN_DIALOG,
    payload
  };
}
