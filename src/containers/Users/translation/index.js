/*
 * Users Container Messages
 *
 * This contains all the text for the Users Container component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  user_list: {
    id: 'components.Users.user_list',
    defaultMessage: 'User List'
  },
  user_page: {
    id: 'components.Users.user_page',
    defaultMessage: 'User Page'
  },
});
