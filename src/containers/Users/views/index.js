/*
 * UsersPage
 *
 * This is the first thing users see of our App, at the '/' route
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import { deleteUser, getUserList, openDialog, updateUser } from 'containers/Users/actions';
import Breadcrumbs from 'components/Breadcrumbs';
import BreadCrumbItem from 'components/BreadcrumbItem';
import UserList from 'components/Users';
import messages from 'containers/Users/translation';
import { FIELDS } from 'utils/constants';
import { People } from '@material-ui/icons';
import messagesCommon from 'translations/messagesCommon';

export class Users extends React.Component {
  state = {
    search: ''
  };

  componentDidMount() {
    this.props.getUserListRequest();
  }

  onPageChanged = data => {
    const { currentPage } = data;
    const request = {
      page: currentPage
    };

    if (this.state.search) {
      request.fields = `email.search(${this.state.search}),id,first_name,last_name,last_login_at,phone,roles{display_name}`;
    }

    this.props.getUserListRequest(request);
  };

  search = (data) => {
    let fields = FIELDS.USERS;
    this.setState({ search: data });

    if (data) {
      fields = `email.search(${data}),id,first_name,last_name,last_login_at,phone,roles{display_name}`;
    }
    const request = {
      fields: fields
    };
    this.props.getUserListRequest(request);
  };

  render() {
    const {
      users: {
        data,
        total,
        per_page,
        current_page,
        openDialogUser,
        openDialogDelete
      }
    } = this.props;

    return [
      <Breadcrumbs icon={<People/>} key='Breadcrumbs' headerTitle={intl.formatMessage(messages.user_list)}>
        <BreadCrumbItem to="/" title={intl.formatMessage(messagesCommon.home)} icon="fa-dashboard"/>
        <BreadCrumbItem active title={intl.formatMessage(messages.user_list)}/>
      </Breadcrumbs>,
      <UserList
        key='UserList'
        total={total}
        currentPage={current_page}
        perPage={per_page}
        data={data}
        openDialogUser={openDialogUser}
        openDialogDelete={openDialogDelete}
        onPageChanged={this.onPageChanged}
        search={this.search}
        deleteUser={this.props.deleteUser}
        updateUser={this.props.updateUser}
        openDialog={this.props.openDialog}
      />
    ];
  }
}

Users.propTypes = {
  user: PropTypes.object,
  users: PropTypes.object,
  getUserListRequest: PropTypes.func,
  deleteUser: PropTypes.func,
  openDialog: PropTypes.func,
  updateUser: PropTypes.func
};

export function mapDispatchToProps(dispatch) {
  return {
    getUserListRequest: (request) => {
      dispatch(getUserList(request));
    },
    deleteUser: (data) => {
      dispatch(deleteUser(data));
    },
    updateUser: (data) => {
      dispatch(updateUser(data));
    },
    openDialog: (data) => {
      dispatch(openDialog(data));
    }
  };
}

const mapStateToProps = (state) => ({
  auth: state.global.user,
  users: state.users
});

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);


export default compose(
  withConnect
)(Users);
