import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

BreadCrumbItem.propTypes = {
  to: PropTypes.string,
  icon: PropTypes.string,
  active: PropTypes.bool,
  title: PropTypes.node.isRequired
};

export default function BreadCrumbItem(props) {
  const { to, title, icon = '', active } = props;

  if (active) {
    return title;
  }

  return (
    <Link to={to}>
      <i className={`fa ${icon}`}/>
      {title}
    </Link>
  );
}
