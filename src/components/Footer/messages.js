/*
 * Footer Messages
 *
 * This contains all the text for the Footer component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  version: {
    id: 'components.Footer.version',
    defaultMessage: 'Version'
  },
  copy_right: {
    id: 'components.Footer.copy_right',
    defaultMessage: `
      © {year} Terra Drone Corporation.
    `
  },
  company_name: {
    id: 'components.Footer.company_owner',
    defaultMessage: 'Terra Drone'
  }
});
