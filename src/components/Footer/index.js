import React from 'react';
import { FormattedMessage } from 'react-intl';
import messages from 'components/Footer/messages';

export default function Footer() {
  const URL_TERRA_DRONE_ENV = process.env.URL_TERRA_DRONE;
  const SYSTEM_VERSION_ENV = process.env.SYSTEM_VERSION;


  return (
    <footer className="main-footer">
      <div className="pull-right hidden-xs">
        <b><FormattedMessage {...messages.version}/></b> {SYSTEM_VERSION_ENV}
      </div>
      <strong>
        <FormattedMessage
          {...messages.copy_right}
          values={{
            year: 2019
          }}
        />
        &nbsp;
        <a href={URL_TERRA_DRONE_ENV}><FormattedMessage {...messages.company_name}/></a>.
      </strong>
    </footer>
  );
}
