import React from 'react';
import pageLoading from 'styles/image/svg/page-loading.svg';

export default function PageLoading() {
  return (
    <div className="Loading">
      <span><img src={pageLoading} alt=""/></span>
    </div>
  );
}
