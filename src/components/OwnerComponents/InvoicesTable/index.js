import React from 'react';
import PropTypes from 'prop-types';

import { formatNumber } from 'utils/helpers';
import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import msg from 'components/OwnerComponents/InvoicesModal/messages';

function InvoicesTables(props) {
  const { data, onClick } = props;
  const goTo = id => () => onClick(id);
  const row = d => (
    <tr key={d.id} onClick={goTo(d.id)}>
      <td>{d.month}</td>
      <td>¥{formatNumber(d.price)}</td>
      <td>{d.due_date}</td>
    </tr>
  );

  const nodata = () => (
    <tr>
      <td colSpan={3}>{intl.formatMessage(msg.no_data)}</td>
    </tr>
  );

  return (
    <table className="InvoicesTables">
      <thead>
      <tr>
        <th>{intl.formatMessage(msg.month)}</th>
        <th>{intl.formatMessage(msg.price)}</th>
        <th>{intl.formatMessage(msg.due_date)}</th>
      </tr>
      </thead>
      <tbody>
      {data.length ? data.map(d => row(d)) : nodata()}
      </tbody>
    </table>
  );
}

InvoicesTables.propTypes = {
  data: PropTypes.array,
  onClick: PropTypes.func
};

export default InvoicesTables;
