import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import Pagination from 'components/Pagination';
import InvoicesTable from 'components/OwnerComponents/InvoicesTable';
import InvoicesModal from 'components/OwnerComponents/InvoicesModal';
import msg from 'components/OwnerComponents/InvoicesModal/messages';
import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import { PAGE_NEIGHBOURS, INVOICES_TYPE } from 'utils/constants';

const { formatMessage: trans } = intl;

class InvoicesComponent extends React.Component {
  state = {
    invoiceId: null,
    showDetailModal: false,
    currentPage: 1
  };

  onPageChanged = (data) => {
    this.setState({ currentPage: data.currentPage });
    this.props.onPageChanged(data);
  };

  componentWillReceiveProps(nextProps) {
    const { currentPage } = nextProps;
    this.setState({ currentPage });
  }

  goToDetail = id => {
    this.setState({
      showDetailModal: true,
      invoiceId: id
    });
  };

  onClose = () => this.setState({
    showDetailModal: false,
    invoiceId: null
  });

  render() {
    const { total, perPage, data, type } = this.props;
    const { currentPage, showDetailModal } = this.state;
    const title_key = type === INVOICES_TYPE.PAYMENT ? 'payment_history' : 'debit_invoices';

    return (
      <Fragment>
        <InvoicesModal
          title={trans(msg[title_key])}
          open={showDetailModal}
          onClose={this.onClose}
        />
        <section className="content">
          <div className="row">
            <div className="col-xs-12">
              <div className="box box-info">
                <div className="box-body table-responsive">
                  <InvoicesTable data={data} onClick={this.goToDetail}/>
                  <Pagination
                    totalRecords={total}
                    currentPage={currentPage}
                    pageLimit={perPage}
                    pageNeighbours={PAGE_NEIGHBOURS}
                    onPageChanged={this.onPageChanged}
                  />
                </div>
              </div>
            </div>
          </div>
        </section>
      </Fragment>
    );
  }
}

InvoicesComponent.propTypes = {
  type: PropTypes.string,
  data: PropTypes.array,
  total: PropTypes.number,
  perPage: PropTypes.number,
  currentPage: PropTypes.number,
  onPageChanged: PropTypes.func
};

export default InvoicesComponent;
