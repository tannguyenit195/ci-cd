import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Pagination from 'components/Pagination/index';
import { PAGE_NEIGHBOURS } from 'utils/constants';
import messages from './messages';
import { FormattedMessage } from 'react-intl';
import SearchInput from 'components/SearchInput/index';
import { withRouter } from 'react-router-dom';
import { DELETE_DIALOG } from 'containers/Users/constants';
import DialogDelete from 'components/DialogDelete';

class OwnerCompanies extends React.Component {
  state = {
    deleteId: '',
    editing: {
      id: '',
      firstName: '',
      lastName: '',
      email: '',
      phone: ''
    }
  };

  routeChange = (id) => () => {
    const path = `/owner/companies/${id}`;

    this.props.history.push(path);
  };

  search = (data) => {
    this.props.search(data);
  };

  handleDelete = (id) => () => {
    this.setState({ deleteId: id });
    this.props.openDialog(DELETE_DIALOG);
  };

  onPageChanged = (data) => {
    this.props.onPageChanged(data);
  };

  acceptDelete = () => {
    const { data, currentPage } = this.props;
    const { deleteId } = this.state;
    const page = data.length === 1 ? currentPage - 1 : currentPage;
    this.props.acceptDelete({
      id: deleteId,
      page
    });
  };

  renderItem = (id, items) => (
    <td onClick={this.routeChange(id)}>
      {items}
    </td>
  );

  renderOperation = (id) => (
    <td onClick={this.handleDelete(id)}>
      <a href="#"><i className="fa fa-trash" aria-hidden="true"/></a>
    </td>
  );

  render() {
    const { data, total, perPage, openDialogDelete, currentPage } = this.props;

    return (
      <Fragment>
        <section className="content">
          <div className="row">
            <div className="col-xs-12">
              <div className="box box-info">
                <div className="box-header">
                  <h3 className="box-title">
                    <FormattedMessage {...messages.company_list}/>
                  </h3>
                  <div className="box-tools">
                    <SearchInput search={this.search}/>
                  </div>
                </div>
                <div className="box-body table-responsive">
                  <table className="table table-hover table-border">
                    <thead>
                    <tr>
                      <th><FormattedMessage {...messages.index}/></th>
                      <th><FormattedMessage {...messages.company_name}/></th>
                      <th><FormattedMessage {...messages.email}/></th>
                      <th><FormattedMessage {...messages.phone}/></th>
                      <th><FormattedMessage {...messages.plan}/></th>
                      <th><FormattedMessage {...messages.last_login}/></th>
                      <th><FormattedMessage {...messages.last_login}/></th>
                    </tr>
                    </thead>
                    <tbody>
                    {data.length === 0 && <tr>
                      <td colSpan={7}>
                        <FormattedMessage {...messages.no_data}/>
                      </td>
                    </tr>}
                    {
                      data && data.map((item, index) => (
                        <tr key={index}>
                          {this.renderItem(item.id, (currentPage - 1) * 10 + index + 1)}
                          {this.renderItem(item.id, item.name)}
                          {this.renderItem(item.id, item.users.data && item.users.data[0] ? item.users.data[0].email : '')}
                          {this.renderItem(item.id, item.phone)}
                          {this.renderItem(item.id, item.type)}
                          {this.renderItem(item.id, item.last_login_at || '--')}
                          {this.renderOperation(item.id)}
                        </tr>
                      ))
                    }
                    </tbody>
                  </table>
                  <Pagination
                    totalRecords={total || 0}
                    currentPage={currentPage}
                    pageLimit={perPage}
                    pageNeighbours={PAGE_NEIGHBOURS}
                    onPageChanged={this.onPageChanged}
                  />
                </div>
              </div>
            </div>
          </div>
        </section>
        <DialogDelete
          open={openDialogDelete}
          close={() => this.props.openDialog(DELETE_DIALOG)}
          accept={this.acceptDelete}
        />
      </Fragment>
    );
  }
}

OwnerCompanies.propTypes = {
  data: PropTypes.array,
  total: PropTypes.number,
  perPage: PropTypes.number,
  currentPage: PropTypes.number,
  history: PropTypes.object,
  openDialogDelete: PropTypes.bool,
  onPageChanged: PropTypes.func,
  search: PropTypes.func,
  acceptDelete: PropTypes.func,
  updateUser: PropTypes.func,
  openDialog: PropTypes.func
};

export default withRouter(OwnerCompanies);
