/*
 * User Messages
 *
 * This contains all the text for the User component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  index: {
    id: 'commons.index',
    defaultMessage: 'No'
  },
  company_name: {
    id: 'components.Company.company_name',
    defaultMessage: 'Company Name'
  },
  company_address: {
    id: 'components.Company.company_address',
    defaultMessage: 'Company Address'
  },
  company_phone: {
    id: 'components.Company.company_phone',
    defaultMessage: 'Company Phone'
  },
  company_description: {
    id: 'components.Company.company_description',
    defaultMessage: 'Company Description'
  },
  user_first_name: {
    id: 'components.Company.user_first_name',
    defaultMessage: 'User first name'
  },
  user_last_name: {
    id: 'components.Company.user_last_name',
    defaultMessage: 'User last name'
  },
  user_phone: {
    id: 'components.Company.user_phone',
    defaultMessage: 'User phone'
  },
  email: {
    id: 'commons.email',
    defaultMessage: 'Email'
  },
  phone: {
    id: 'components.Company.phone',
    defaultMessage: 'Phone'
  },
  plan: {
    id: 'components.Company.plan',
    defaultMessage: 'Current Plan'
  },
  last_login: {
    id: 'components.Company.last_login',
    defaultMessage: 'Last login'
  },
  company_list: {
    id: 'components.Company.company_list',
    defaultMessage: 'Company List'
  },
  company_detail: {
    id: 'components.Company.company_detail',
    defaultMessage: 'Company Detail'
  },
  company: {
    id: 'components.Company.company',
    defaultMessage: 'Company'
  },
  company_create: {
    id: 'components.Company.company_create',
    defaultMessage: 'New Company'
  },
  no_data: {
    id: 'commons.no_data',
    defaultMessage: 'No data'
  }
});
