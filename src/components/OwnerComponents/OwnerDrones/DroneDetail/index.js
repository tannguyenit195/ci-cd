import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { CheckCircleOutline } from '@material-ui/icons';
import { Button } from '@material-ui/core';

import SectionContentComponent from 'components/SectionContent';
import message from '../messages';

function DroneDetailComponent({ drone, goBack }) {
  const {
    name = 'Phantom 4',
    manufacturer = 'DJI',
    serial_number = '07JDD7F00100LH',
    product_url = 'https://www.dji.com/jp/phantom-4',
    updated = '2019/03/14 16:36'
  } = drone || {};

  function renderRow(label, value) {
    return (
      <div className="lines">
        <div className="col-md-4 col-sm-6 line">
          <span><FormattedMessage {...message[label]}/></span>
        </div>
        <div className="col-md-8 col-sm-6 line underline">
          <span>{value}</span>
        </div>
      </div>
    );
  }

  function backButton() {
    return (
      <div className="box-footer">
        <Button
          className="btn-back"
          variant="contained"
          size="large"
          onClick={goBack}
        >
          <FormattedMessage {...message.back} />
        </Button>
      </div>
    );
  }

  function renderCertification() {
    return (
      <div className="certifications">
        <div className="col-md-6 col-sm-6">
          <CheckCircleOutline/>
          <FormattedMessage {...message.insurance}/>
        </div>
        <div className="col-md-6 col-sm-6">
          <CheckCircleOutline/>
          <FormattedMessage {...message.modified}/>
        </div>
      </div>
    );
  }

  return (
    <SectionContentComponent>
      <div className="DetailComponent">
        {backButton()}
        {renderRow('name', name)}
        {renderRow('manufacturer', manufacturer)}
        {renderRow('serial_number', serial_number)}
        {renderRow('product_url', product_url)}
        {renderRow('updated', updated)}
        {renderCertification()}
        {backButton()}
      </div>
    </SectionContentComponent>
  );
}

DroneDetailComponent.propTypes = {
  drone: PropTypes.object,
  goBack: PropTypes.func
};

export default DroneDetailComponent;
