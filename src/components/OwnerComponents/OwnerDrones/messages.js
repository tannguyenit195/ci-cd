import { defineMessages } from 'react-intl';

export default defineMessages({
  drone_list: {
    id: 'components.DroneDetail.drone_list',
    defaultMessage: 'Drone list'
  },
  name: {
    id: 'commons.name',
    defaultMessage: 'Name'
  },
  back: {
    id: 'commons.back',
    defaultMessage: 'Back'
  },
  drone_detail: {
    id: 'components.DroneDetail.drone_detail',
    defaultMessage: 'Drone detail'
  },
  product_url: {
    id: 'components.DroneDetail.product_url',
    defaultMessage: 'Product URL'
  },
  insurance: {
    id: 'components.DroneDetail.insurance',
    defaultMessage: 'Insurance'
  },
  modified: {
    id: 'components.DroneDetail.modified',
    defaultMessage: 'Modified'
  },
  drone_name: {
    id: 'components.DroneDetail.drone_name',
    defaultMessage: 'Drone name'
  },
  manufacturer: {
    id: 'components.DroneDetail.drone_manufacturer',
    defaultMessage: 'Manufacturer'
  },
  serial_number: {
    id: 'components.DroneDetail.drone_serial_number',
    defaultMessage: 'Serial number'
  },
  updated: {
    id: 'components.DroneDetail.drone_updated',
    defaultMessage: 'Updated'
  },
  no_data: {
    id: 'commons.no_data',
    defaultMessage: 'No data'
  },
});
