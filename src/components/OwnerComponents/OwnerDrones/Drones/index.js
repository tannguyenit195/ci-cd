import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Pagination from 'components/Pagination';
import SearchInput from 'components/SearchInput';
import { PAGE_NEIGHBOURS } from 'utils/constants';
import messages from '../messages';

class Drones extends React.Component {
  state = {
    currentPage: 1
  };

  searchUser = (data) => {
    this.setState({ currentPage: 1 });
    this.props.search(data);
  };

  onPageChanged = (data) => {
    this.setState({ currentPage: data.currentPage });
    this.props.onPageChanged(data);
  };

  componentWillReceiveProps(nextProps) {
    const { currentPage } = nextProps;
    this.setState({ currentPage });
  }

  goToDetail = id => () => this.props.goToDetail(id);

  render() {
    const { data, total, perPage } = this.props;
    const { currentPage } = this.state;

    return (
      <Fragment>
        <section className="content">
          <div className="row">
            <div className="col-xs-12">
              <div className="box box-info">
                <div className="box-header">
                  <h3 className="box-title">
                    <FormattedMessage {...messages.drone_list}/>
                  </h3>
                  <div className="box-tools">
                    <SearchInput search={this.searchUser}/>
                  </div>
                </div>
                <div className="box-body table-responsive">
                  <table className="table table-hover table-border">
                    <thead>
                    <tr>
                      <th><FormattedMessage {...messages.drone_name}/></th>
                      <th><FormattedMessage {...messages.manufacturer}/></th>
                      <th><FormattedMessage {...messages.serial_number}/></th>
                      <th><FormattedMessage {...messages.updated}/></th>
                    </tr>
                    </thead>
                    <tbody>
                    {data.length === 0 && <tr>
                      <td colSpan="4">
                        <FormattedMessage {...messages.no_data}/>
                      </td>
                    </tr>}
                    {
                      data.map((item) => (
                        <tr key={item.id} onClick={this.goToDetail(item.id)}>
                          <td>{item.name}</td>
                          <td>{item.manufacturer}</td>
                          <td>{item.serial_number}</td>
                          <td>{item.updated}</td>
                        </tr>
                      ))
                    }
                    </tbody>
                  </table>
                  <Pagination
                    totalRecords={total}
                    currentPage={currentPage}
                    pageLimit={perPage}
                    pageNeighbours={PAGE_NEIGHBOURS}
                    onPageChanged={this.onPageChanged}
                  />
                </div>
              </div>
            </div>
          </div>
        </section>
      </Fragment>
    );
  }
}

Drones.propTypes = {
  data: PropTypes.array,
  total: PropTypes.number,
  perPage: PropTypes.number,
  currentPage: PropTypes.number,
  onPageChanged: PropTypes.func,
  goToDetail: PropTypes.func,
  search: PropTypes.func
};

export default Drones;
