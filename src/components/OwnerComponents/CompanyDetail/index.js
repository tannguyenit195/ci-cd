import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import messages from './messages';
import { withRouter } from 'react-router-dom';
import { Button, TextField } from '@material-ui/core';
import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messagesCommon from 'translations/messagesCommon';

const initState = {
  name: '',
  address: '',
  phone: '',
  type: '',
  url: '',
  description: '',
  id: '',
  users: {
    email: '',
    phone: ''
  },
  isEdit: false
};

class OwnerCompaniesDetail extends React.Component {
  state = initState;

  onChangeInput = (event, type) => {
    const { target: { name, value } } = event;
    if (type) {
      this.setState({
        users: {
          ...this.state.users,
          [name]: value
        }
      });
    } else {
      this.setState({ [name]: value });
    }
  };

  componentWillReceiveProps(nextProps) {
    const {
      data: {
        address, type, name, url, description, users, id, phone
      }
    } = nextProps;

    this.setState({
      address,
      name,
      url,
      description,
      type,
      id,
      phone
    });

    if (users[0]) {
      this.setState({
        users: {
          email: users[0].email,
          phone: users[0].phone
        }
      });
    }
  }

  handleEdit = () => {
    const {
      isEdit,
      id,
      address,
      description,
      name,
      phone,
      url,
      users
    } = this.state;

    if (isEdit) {
      const data = {
        address,
        description,
        name,
        phone,
        url,
        users
      };
      this.props.submitEditCompany(id, data);
      this.setState({ isEdit: false });
    } else {
      this.setState({ isEdit: true });
    }
  };

  renderInput(id, type, label, value, users = false) {
    const { isEdit } = this.state;
    return (
      <div className="inputs form-group">
        <label htmlFor={id}>{label}</label>
        <TextField
          className="form-control"
          type={type}
          name={id}
          value={value || ''}
          onChange={(e) => this.onChangeInput(e, users)}
          variant={isEdit ? 'outlined' : 'standard'}
          disabled={!isEdit}
        />
      </div>
    );
  }

  renderTextArea() {
    const { isEdit, description } = this.state;

    return (
      <div className="inputs form-group">
        <label htmlFor="description">
          {intl.formatMessage(messages.company_description)}
        </label>
        <textarea
          id="description"
          name="description"
          className="form-control"
          disabled={!isEdit}
          value={description || ''}
          onChange={this.onChangeInput}
        />
      </div>
    );
  };

  renderActions() {
    const { isEdit } = this.state;
    const { isFetching } = this.props;
    const text = intl.formatMessage(messagesCommon[isEdit ? 'save' : 'edit']);
    const elm = isFetching ? <i className="fa fa-spin fa-spinner"/> : text;

    return (
      <div className="actions">
        <Button
          variant="contained"
          size="large"
          onClick={this.handleCancel}
          disabled={!isEdit}
        >
          {intl.formatMessage(messagesCommon.cancel)}
        </Button>
        <Button
          variant="contained"
          color="primary"
          size="large"
          onClick={this.handleEdit}
          disabled={isFetching}
        >
          {elm}
        </Button>
      </div>
    );
  }

  render() {
    const {
      address,
      name,
      url,
      type,
      phone,
      users
    } = this.state;

    return (
      <Fragment>
        <section className="content">
          <div className="box box-info">
            <div className="box-header with-border">
              <h3 className="box-title">{intl.formatMessage(messages.company_detail)}</h3>
            </div>
            <div className="box-body">
              <div className="row">
                <div className="col-md-6">
                  {this.renderInput('name', 'text', intl.formatMessage(messages.company_name), name)}
                  {this.renderInput('url', 'text', intl.formatMessage(messages.company_url), url)}
                  {this.renderInput('address', 'text', intl.formatMessage(messages.company_address), address)}
                  {this.renderInput('phone', 'text', intl.formatMessage(messages.company_phone), phone)}
                </div>
                <div className="col-md-6">
                  {this.renderInput('__', 'text', intl.formatMessage(messages.company_type), type)}
                  {this.renderInput('email', 'text', intl.formatMessage(messagesCommon.email), users.email, true)}
                  {this.renderInput('phone', 'text', intl.formatMessage(messagesCommon.phone), users.phone, true)}
                </div>
                <div className="col-md-12">
                  {this.renderTextArea()}
                </div>
              </div>
            </div>
            <div className="box-footer text-center">
              {this.renderActions()}
            </div>
          </div>
        </section>
      </Fragment>
    );
  }
}

OwnerCompaniesDetail.propTypes = {
  data: PropTypes.object,
  isFetching: PropTypes.bool,
  submitEditCompany: PropTypes.func,
};

export default withRouter(OwnerCompaniesDetail);
