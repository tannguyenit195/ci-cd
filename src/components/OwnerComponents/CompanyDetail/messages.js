/*
 * User Messages
 *
 * This contains all the text for the User component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  index: {
    id: 'commons.index',
    defaultMessage: 'No'
  },
  company_name: {
    id: 'components.Company.company_name',
    defaultMessage: 'Company Name'
  },
  email: {
    id: 'commons.email',
    defaultMessage: 'Email'
  },
  phone: {
    id: 'components.Company.phone',
    defaultMessage: 'Phone'
  },
  plan: {
    id: 'components.Company.plan',
    defaultMessage: 'Current Plan'
  },
  last_login: {
    id: 'components.Company.last_login',
    defaultMessage: 'Last login'
  },
  company_url: {
    id: 'components.Company.company_url',
    defaultMessage: 'URL'
  },
  company_address: {
    id: 'components.Company.company_address',
    defaultMessage: 'Company Address'
  },
  company_description: {
    id: 'components.Company.company_description',
    defaultMessage: 'Company Description'
  },
  company_phone: {
    id: 'components.Company.company_phone',
    defaultMessage: 'Company Phone'
  },
  company_type: {
    id: 'components.Company.company_type',
    defaultMessage: 'Company Type'
  },
  company_detail: {
    id: 'components.Company.company_detail',
    defaultMessage: 'Company Detail'
  }
});
