import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Button, TextField } from '@material-ui/core';
import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messagesCommon from 'translations/messagesCommon';
import message from 'components/OwnerComponents/Company/messages';

const initState = {
  error_password: false,
  name: '',
  address: '',
  phone: '',
  description: '',
  user: {
    email: '',
    first_name: '',
    last_name: '',
    password: '',
    password_confirm: '',
    phone: ''
  }
};

class OwnerCompaniesCreate extends React.Component {
  state = initState;

  onChangeInput = (event, type) => {
    const { target: { name, value } } = event;
    if (type) {
      this.setState({
        user: {
          ...this.state.user,
          [name]: value
        }
      });

      if (name === 'password_confirm') {
        if (this.state.user.password !== value) {
          this.setState({ error_password: true });
        } else {
          this.setState({ error_password: false });
        }
      }
    } else {
      this.setState({ [name]: value });
    }
  };

  handleCancel = () => {
    this.setState(initState);
  };

  handleSave = () => {
    this.props.createCompany(this.state);
  };

  renderInput(id, type, label, value, rank = false) {
    return (
      <div className="inputs form-group">
        <label htmlFor={id}>{label}</label>
        <TextField
          className="form-control"
          type={type}
          name={id}
          value={value || ''}
          onChange={(e) => this.onChangeInput(e, rank)}
          variant="outlined"
          error={id === 'password_confirm' && this.state.error_password}
        />
        {
          id === 'password_confirm' && this.state.error_password && (
            <div className="validate-error">{intl.formatMessage(messagesCommon.password_not_match)}</div>
          )
        }
        {
          id === 'password' && !value.match(/^([゠a-zA-Z0-9@%!#$.)(-=<>~+]+)$/) && (
            <div className="validate-error">{intl.formatMessage(messagesCommon.password_not_valid)}</div>
          )
        }
      </div>
    );
  }

  renderActions() {
    return (
      <div className="user-create-box">
        <Button
          variant="contained"
          size="large"
          onClick={this.handleCancel}
        >
          {intl.formatMessage(messagesCommon.cancel)}
        </Button>
        <Button
          variant="contained"
          size="large"
          color="primary"
          onClick={this.handleSave}
        >
          {intl.formatMessage(messagesCommon.save)}
        </Button>
      </div>
    );
  }

  render() {
    const { name, address, description, phone, user } = this.state;

    return (
      <Fragment>
        <section className="content">
          <div className="box box-info">
            <div className="box-header with-border">
              <h3 className="box-title">
                {intl.formatMessage(message.company_create)}
              </h3>
            </div>
            <div className="box-body">
              <div className="row">
                <div className="col-md-6">
                  {this.renderInput('name', 'text', intl.formatMessage(message.company_name), name)}
                  {this.renderInput('address', 'text', intl.formatMessage(message.company_address), address)}
                  {this.renderInput('phone', 'text', intl.formatMessage(message.company_phone), phone)}
                  {this.renderInput('description', 'text', intl.formatMessage(message.company_description), description)}
                  {this.renderInput('email', 'text', intl.formatMessage(messagesCommon.email), user.email, true)}
                </div>
                <div className="col-md-6">
                  {this.renderInput('first_name', 'text', intl.formatMessage(message.user_first_name), user.first_name, true)}
                  {this.renderInput('last_name', 'text', intl.formatMessage(message.user_last_name), user.last_name, true)}
                  {this.renderInput('password', 'password', intl.formatMessage(messagesCommon.password), user.password, true)}
                  {this.renderInput('password_confirm', 'password', intl.formatMessage(messagesCommon.password_confirm), user.password_confirm, true)}
                  {this.renderInput('phone', 'text', intl.formatMessage(message.user_phone), user.phone, true)}
                </div>
              </div>
            </div>
            <div className="box-footer text-center">
              {this.renderActions()}
            </div>
          </div>
        </section>
      </Fragment>
    );
  }
}

OwnerCompaniesCreate.propTypes = {
  createCompany: PropTypes.func
};

export default withRouter(OwnerCompaniesCreate);
