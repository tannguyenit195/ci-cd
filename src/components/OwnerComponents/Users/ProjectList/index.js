import React from 'react';
import PropTypes from 'prop-types';
import cls from 'classnames';
import { FormattedMessage } from 'react-intl';

import Pagination from 'components/Pagination';
import { PAGE_NEIGHBOURS } from 'utils/constants';
import message from 'components/OwnerComponents/Users/messages';

const LIMIT = 5;

class ProjectList extends React.Component {
  state = {
    currentPage: 1
  };

  onPageChanged = (data) => {
    this.setState({ currentPage: data.currentPage });
  };

  render() {
    const { projects } = this.props;
    const { currentPage } = this.state;
    const amountProjects = currentPage * LIMIT;
    const projectsOnPage = projects.slice(amountProjects - LIMIT, amountProjects);

    return (
      <div className="ProjectList">
        <p className="title"><FormattedMessage {...message.assignment_list}/></p>
        <div className={cls('ProjectList--content', { 'fixed-height': projects.length > LIMIT })}>
          <div className="table-header">
            <div className="col-md-8"><FormattedMessage {...message.project_name}/></div>
            <div className="col-md-4"><FormattedMessage {...message.role}/></div>
          </div>
          {projectsOnPage.length ? projectsOnPage.map(item => (
            <div key={item.id} className="table-row">
              <div className="col-md-8">{item.name}</div>
              <div className="col-md-4">{item.role}</div>
            </div>
          )) : (
            <div className="table-row">
              <FormattedMessage {...message.no_data}/>
            </div>
          )}
        </div>
        <div className="ProjectList--pagination">
          <Pagination
            totalRecords={projects.length}
            currentPage={currentPage}
            pageLimit={LIMIT}
            pageNeighbours={PAGE_NEIGHBOURS}
            onPageChanged={this.onPageChanged}
          />
        </div>
      </div>
    );
  }
}

ProjectList.propTypes = {
  projects: PropTypes.array
};

export default ProjectList;
