import { defineMessages } from 'react-intl';

export default defineMessages({
  user_list: {
    id: 'components.Users.user_list',
    defaultMessage: 'User list'
  },
  name: {
    id: 'commons.name',
    defaultMessage: 'Name'
  },
  back: {
    id: 'commons.back',
    defaultMessage: 'Back'
  },
  user_detail: {
    id: 'components.Users.user_detail',
    defaultMessage: 'User detail'
  },
  authority: {
    id: 'components.Users.authority',
    defaultMessage: 'Authority'
  },
  email: {
    id: 'commons.email_address',
    defaultMessage: 'Email Address'
  },
  tel: {
    id: 'commons.tel',
    defaultMessage: 'Tel'
  },
  project_name: {
    id: 'commons.project_name',
    defaultMessage: 'Project Name'
  },
  role: {
    id: 'commons.role',
    defaultMessage: 'Role'
  },
  user_name: {
    id: 'commons.user_name',
    defaultMessage: 'User Name'
  },
  last_login: {
    id: 'components.Users.last_login',
    defaultMessage: 'Last Login'
  },
  assignment_list: {
    id: 'components.Users.assignment_list',
    defaultMessage: 'Assignment List'
  },
  payment_histories: {
    id: 'components.Sidebar.payment_histories',
    defaultMessage: 'Payment Histories'
  },
  no_data: {
    id: 'commons.no_data',
    defaultMessage: 'No data'
  }
});
