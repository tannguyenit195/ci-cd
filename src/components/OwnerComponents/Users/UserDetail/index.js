import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Button } from '@material-ui/core';

import SectionContentComponent from 'components/SectionContent';
import ProjectList from '../ProjectList';
import message from '../messages';

const projects = [
  {
    id: 1,
    name: 'Project 1',
    role: 'admin'
  },
  {
    id: 2,
    name: 'Project 2',
    role: 'admin'
  },
  {
    id: 3,
    name: 'Project 3',
    role: 'admin'
  },
  {
    id: 4,
    name: 'Project 4',
    role: 'admin'
  },
  {
    id: 5,
    name: 'Project 5',
    role: 'admin'
  },
  {
    id: 6,
    name: 'Project 6',
    role: 'admin'
  },
  {
    id: 7,
    name: 'Project 7',
    role: 'admin'
  },
  {
    id: 8,
    name: 'Project 8',
    role: 'admin'
  },
  {
    id: 9,
    name: 'Project 9',
    role: 'admin'
  }
];

function UserDetailComponent({ user, goBack }) {
  const {
    first_name = '',
    email = '',
    phone = '',
    display_name = '',
    last_login_at = ''
  } = user || {};

  function renderRow(label, value) {
    return (
      <div className="lines">
        <div className="col-md-4 col-sm-6 line">
          <span><FormattedMessage {...message[label]}/></span>
        </div>
        <div className="col-md-8 col-sm-6 line underline">
          <span>{value}</span>
        </div>
      </div>
    );
  }

  return (
    <SectionContentComponent>
      <div className="DetailComponent">
        <div className="col-md-6 ">
          {renderRow('user_name', first_name)}
          {renderRow('email', email)}
          {renderRow('tel', phone)}
          {renderRow('authority', display_name)}
          {renderRow('last_login', last_login_at || '---')}
        </div>
        <div className="col-md-6 User">
          <ProjectList projects={projects}/>
        </div>
        <div className="box-footer">
          <Button
            variant="contained"
            className="btn-back"
            size="large"
            onClick={goBack}
          >
            <FormattedMessage {...message.back}/>
          </Button>
        </div>
      </div>
    </SectionContentComponent>
  );
}

UserDetailComponent.propTypes = {
  user: PropTypes.object,
  goBack: PropTypes.func
};

export default UserDetailComponent;
