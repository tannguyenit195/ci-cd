import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Pagination from 'components/Pagination';
import SearchInput from 'components/SearchInput';
import { PAGE_NEIGHBOURS } from 'utils/constants';
import messages from '../messages';

class UserList extends React.Component {
  search = (data) => {
    this.props.search(data);
  };

  onPageChanged = (data) => {
    this.props.onPageChanged(data);
  };

  goToDetail = id => () => {
    this.props.gotoDetail(id);
  };

  render() {
    const { data, total, perPage, currentPage } = this.props;
    return (
      <Fragment>
        <section className="content">
          <div className="row">
            <div className="col-xs-12">
              <div className="box box-info">
                <div className="box-header">
                  <h3 className="box-title"/>
                  <div className="box-tools">
                    <SearchInput search={this.search}/>
                  </div>
                </div>
                <div className="box-body table-responsive">
                  <table className="table table-hover table-border">
                    <thead>
                    <tr>
                      <th><FormattedMessage {...messages.user_name}/></th>
                      <th><FormattedMessage {...messages.email}/></th>
                      <th><FormattedMessage {...messages.authority}/></th>
                      <th><FormattedMessage {...messages.last_login}/></th>
                    </tr>
                    </thead>
                    <tbody>
                    {data.length === 0 && <tr>
                      <td colSpan={4}>
                        <FormattedMessage {...messages.no_data}/>
                      </td>
                    </tr>}
                    {
                      data.map((item) => (
                        <tr key={item.id} onClick={this.goToDetail(item.id)}>
                          <td>{item.first_name}</td>
                          <td>{item.email}</td>
                          <td>{item.id}</td>
                          <td>{item.last_login_at || '---'}</td>
                        </tr>
                      ))
                    }
                    </tbody>
                  </table>
                  <Pagination
                    totalRecords={total}
                    currentPage={currentPage}
                    pageLimit={perPage}
                    pageNeighbours={PAGE_NEIGHBOURS}
                    onPageChanged={this.onPageChanged}
                  />
                </div>
              </div>
            </div>
          </div>
        </section>
      </Fragment>
    );
  }
}

UserList.propTypes = {
  data: PropTypes.array,
  total: PropTypes.number,
  perPage: PropTypes.number,
  currentPage: PropTypes.number,
  onPageChanged: PropTypes.func,
  gotoDetail: PropTypes.func,
  search: PropTypes.func
};

export default UserList;
