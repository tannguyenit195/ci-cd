import React from 'react';
import PropTypes from 'prop-types';
import {
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  Slide
} from '@material-ui/core';
import msg from './messages';
import { formatNumber } from 'utils/helpers';
import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';

function Transition(props) {
  return <Slide direction="down" {...props} />;
}

const DATA = [
  {
    id: 1,
    product_service_name: 'Terra UTM',
    usage_plan: 'Drone UAV\'s operation management system',
    project_name: 'flight_plan',
    admin: 'asdfsadf',
    created_date: '2019/03/01',
    amount: 5000
  },
  {
    id: 2,
    product_service_name: 'Terra UTM',
    usage_plan: 'Drone UAV\'s operation management system',
    project_name: 'flight_plan',
    admin: 'asdfsadf',
    created_date: '2019/03/01',
    amount: 5000
  },
  {
    id: 3,
    product_service_name: 'Terra UTM',
    usage_plan: 'Drone UAV\'s operation management system',
    project_name: 'flight_plan',
    admin: 'asdfsadf',
    created_date: '2019/03/01',
    amount: 5000
  },
  {
    id: 4,
    product_service_name: 'Terra UTM',
    usage_plan: 'Drone UAV\'s operation management system',
    project_name: 'flight_plan',
    admin: 'asdfsadf',
    created_date: '2019/03/01',
    amount: 5000
  },
  {
    id: 5,
    product_service_name: 'Terra UTM',
    usage_plan: 'Drone UAV\'s operation management system',
    project_name: 'flight_plan',
    admin: 'asdfsadf',
    created_date: '2019/03/01',
    amount: 5000
  },
  {
    id: 6,
    product_service_name: 'Terra UTM',
    usage_plan: 'Drone UAV\'s operation management system',
    project_name: 'flight_plan',
    admin: 'asdfsadf',
    created_date: '2019/03/01',
    amount: 5000
  }
];

function InvoicesModal(props) {
  const {
    open, onClose, title,
    time = '2019/2',
    data = DATA
  } = props;
  const total = data.reduce((total, item) => total + item.amount, 0);

  return (
    <Dialog
      fullWidth
      maxWidth="xl"
      className="InvoicesModal"
      TransitionComponent={Transition}
      open={open}
      onClose={onClose}
      aria-labelledby="max-width-dialog-title"
    >
      <div className="modal-title">
        <h1>{title}</h1>
      </div>
      <DialogContent>
        <div className="modal-content">
          <div className="modal-top">
            <h4>{time} {intl.formatMessage(msg.invoice)}</h4>
            <div className="modal-sum">
              <span>¥{formatNumber(total)}</span>
            </div>
          </div>
          <div className="modal-table">
            <table>
              <thead>
              <tr>
                <th>{intl.formatMessage(msg.product_service)}</th>
                <th>{intl.formatMessage(msg.usage_plan)}</th>
                <th>{intl.formatMessage(msg.project_name)}</th>
                <th>{intl.formatMessage(msg.admin)}</th>
                <th className="center">{intl.formatMessage(msg.created_date)}</th>
                <th className="right">{intl.formatMessage(msg.amount)}(¥)</th>
              </tr>
              </thead>
              <tbody>
              {data.map(d => (
                <tr key={d.id}>
                  <td>{d.product_service_name}</td>
                  <td>{d.usage_plan}</td>
                  <td>{d.project_name}</td>
                  <td>{d.admin}</td>
                  <td className="center">{d.created_date}</td>
                  <td className="right">{formatNumber(d.amount)}</td>
                </tr>
              ))}
              </tbody>
            </table>
            <div className="total">
              {intl.formatMessage(msg.total)}: ¥{formatNumber(total)}
            </div>
            <div className="note">
              {intl.formatMessage(msg.invoice_note)}
            </div>
          </div>
        </div>
      </DialogContent>
      <DialogActions className="modal-footer">
        <Button onClick={onClose} color="primary" variant="contained">
          {intl.formatMessage(msg.close)}
        </Button>
      </DialogActions>
    </Dialog>
  );
}

InvoicesModal.propTypes = {
  title: PropTypes.string,
  time: PropTypes.string,
  data: PropTypes.array,
  open: PropTypes.bool,
  onClose: PropTypes.func,
  onChangeTime: PropTypes.func
};

export default InvoicesModal;
