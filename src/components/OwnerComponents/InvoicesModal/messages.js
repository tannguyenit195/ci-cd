import { defineMessages } from 'react-intl';

export default defineMessages({
  product_service: {
    id: 'components.Invoices.product_service',
    defaultMessage: 'Product Service'
  },
  usage_plan: {
    id: 'components.Invoices.usage_plan',
    defaultMessage: 'Usage plan'
  },
  project_name: {
    id: 'commons.project_name',
    defaultMessage: 'Project Name'
  },
  admin: {
    id: 'commons.admin',
    defaultMessage: 'Admin'
  },
  created_date: {
    id: 'components.Invoices.created_date',
    defaultMessage: 'Created date'
  },
  amount: {
    id: 'commons.amount',
    defaultMessage: 'Amount'
  },
  total: {
    id: 'commons.total',
    defaultMessage: 'Total'
  },
  close: {
    id: 'commons.close',
    defaultMessage: 'Close'
  },
  month: {
    id: 'commons.month',
    defaultMessage: 'Month'
  },
  price: {
    id: 'commons.price',
    defaultMessage: 'Price'
  },
  due_date: {
    id: 'commons.due_date',
    defaultMessage: 'Due date'
  },
  no_data: {
    id: 'commons.no_data',
    defaultMessage: 'No data'
  },
  invoice_note: {
    id: 'components.Invoices.note',
    defaultMessage: '* The payment date will be end of month'
  },
  payment_history: {
    id: 'components.Invoices.payment_history',
    defaultMessage: 'Payment History'
  },
  debit_invoices: {
    id: 'components.Invoices.debit_invoices',
    defaultMessage: 'Debit invoices'
  },
  invoice: {
    id: 'components.Invoices.invoice',
    defaultMessage: 'Invoice'
  },
});
