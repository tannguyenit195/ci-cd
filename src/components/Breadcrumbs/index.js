import React from 'react';
import PropTypes from 'prop-types';

Breadcrumbs.propTypes = {
  headerTitle: PropTypes.string,
  children: PropTypes.node,
  icon: PropTypes.node
};
export default function Breadcrumbs(props) {
  const { headerTitle, children, icon = '' } = props;

  return (
    <section className="content-header">
      <h1>
        {icon} {headerTitle}
      </h1>
      <ol className="breadcrumb">
        {
          children.map((item, index) => (
            <li key={index}>{item}</li>
          ))
        }
      </ol>
    </section>
  );
}
