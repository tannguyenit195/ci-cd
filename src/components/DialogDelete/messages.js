/*
 * SearchInput Messages
 *
 * This contains all the text for the SearchInput component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  notification: {
    id: 'components.DialogDelete.notification',
    defaultMessage: 'Notification',
  },
  message: {
    id: 'components.DialogDelete.message',
    defaultMessage: 'You definitely want to take this action',
  },
  disagree: {
    id: 'components.DialogDelete.disagree',
    defaultMessage: 'Disagree',
  },
  agree: {
    id: 'components.DialogDelete.agree',
    defaultMessage: 'Agree',
  },
});
