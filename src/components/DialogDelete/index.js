import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import PropTypes from 'prop-types';
import messages from 'components/DialogDelete/messages';
import { FormattedMessage } from 'react-intl';

class DialogDelete extends React.Component {
  static propTypes = {
    open: PropTypes.bool,
    accept: PropTypes.func,
    close: PropTypes.func
  };

  handleClose = () => {
    this.props.close();
  };

  render() {
    const { open } = this.props;
    return (
      <div>
        <Dialog
          open={open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          className="dialog-wrapper"
        >
          <DialogTitle className="dialog-title">
            <FormattedMessage {...messages.notification} />
          </DialogTitle>
          <DialogContent>
            <FormattedMessage {...messages.message} />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              <FormattedMessage {...messages.disagree} />
            </Button>
            <Button onClick={this.props.accept} color="primary" autoFocus>
              <FormattedMessage {...messages.agree} />
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default DialogDelete;
