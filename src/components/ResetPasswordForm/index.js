import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messages from 'containers/Author/messages';

class ResetPasswordForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      password_confirmation: '',
      submitted: false
    };
    this.submitCallback = this.submitCallback.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeInput = this.onChangeInput.bind(this);
  }

  onChangeInput(event) {
    const { name, value } = event.target;
    this.setState({ [name]: value });
  }

  renderInput(id, type, placeholder) {
    return (
      <div className="form-group" key={id}>
        <div className="controls row">
          <div className="input-group">
            <input
              id={id}
              type={type}
              name={id}
              className="form-control"
              value={this.state[id]}
              placeholder={intl.formatMessage(messages[placeholder])}
              onChange={this.onChangeInput}
            />
          </div>
        </div>
      </div>
    );
  }

  submitCallback() {
    const { isInputPassword, history } = this.props;
    if (isInputPassword) {
      history.push('/');
    } else {
      this.setState({
        submitted: true
      });
    }
  }

  getParamsFromUrl(url = '') {
    const urlArr = url.split('?');
    const email = urlArr[2];

    return {
      token: urlArr[1],
      email: email ? email.substring(email.indexOf('=') + 1) : ''
    };
  }

  handleSubmit(event) {
    event.preventDefault();
    const { isFetching, isInputPassword, submit, history } = this.props;
    if (isFetching || this.isDisabledSubmitting()) {
      return;
    }

    const { token, email: emailUrl } = this.getParamsFromUrl(history.location.search);
    const { email, password, password_confirmation } = this.state;
    const data = isInputPassword ? {
      token,
      email: emailUrl,
      password,
      password_confirmation
    } : email;
    submit(data, this.submitCallback);
  }

  isDisabledSubmitting = () => {
    const { isInputPassword } = this.props;
    const { email, password, password_confirmation } = this.state;
    const newPw = password.trim();
    const newConfirmPw = password_confirmation.trim();

    return isInputPassword
      ? (!newPw || !newConfirmPw || newPw !== newConfirmPw)
      : !email.trim();
  };

  renderAlert(type) {
    return [
      <div className="alert alert-success" key="alert">
        {`「${this.state.email}」 ${intl.formatMessage(messages.reset_password_success)}`}
      </div>,
      this.renderLoginBtn(type)
    ];
  }

  renderSubmitButton = () => {
    const { isFetching } = this.props;
    const isDisabled = this.isDisabledSubmitting();

    return (
      <div className="form-group buttonbox">
        <div className="controls row">
          <button
            type="submit"
            className="btn btn-lg btn-primary col-xs-12"
            disabled={isFetching || isDisabled}
          >
            {isFetching
              ? <span className="fa fa-spinner fa-spin"/>
              : intl.formatMessage(messages.send_button)
                .toUpperCase()}
          </button>
        </div>
      </div>
    );
  };

  renderForm() {
    const { error, isInputPassword } = this.props;
    const message = messages[error] ? intl.formatMessage(messages[error]) : error;

    return (
      <form className="form-horizontal" onSubmit={this.handleSubmit}>
        <fieldset className="col-sm-12">
          {isInputPassword
            ? [
              this.renderInput('password', 'password', 'password'),
              this.renderInput('password_confirmation', 'password', 'password_confirmation')
            ]
            : this.renderInput('email', 'email', 'email_address')
          }
          <span className="has-error">{error ? message : ''}</span>
          {this.renderSubmitButton()}
        </fieldset>
      </form>
    );
  }

  renderLoginBtn(type = '') {
    return (
      <div className="button-link" key="login-btn">
        <Link to={`/${type ? 'owner/' : ''}login`}>{intl.formatMessage(messages.login_title)}</Link>
      </div>
    );
  }

  render() {
    const { submitted } = this.state;
    const { type } = this.props;
    return (
      <div className="resetpassword-box">
        <div className="header">{intl.formatMessage(messages.reset_password)}</div>
        {submitted ? this.renderAlert(type) : this.renderForm()}
      </div>
    );
  }
}

ResetPasswordForm.propTypes = {
  isInputPassword: PropTypes.bool,
  isFetching: PropTypes.bool,
  type: PropTypes.string,
  error: PropTypes.string,
  history: PropTypes.object,
  submit: PropTypes.func
};

export default ResetPasswordForm;
