import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

const LEFT_PAGE = 'LEFT';
const RIGHT_PAGE = 'RIGHT';

const range = (from, to, step = 1) => {
  let i = from;
  const range = [];

  while (i <= to) {
    range.push(i);
    i += step;
  }

  return range;
};

class Pagination extends Component {
  constructor(props) {
    super(props);
    this.state = this.getStateFromProps(props);
  }

  calculatorTotalPage = (totalRecords, pageLimit) => Math.ceil(totalRecords / pageLimit);

  calculatorPageNeighbours = (pageNeighbours) => Math.max(0, Math.min(pageNeighbours, 2));

  componentWillReceiveProps(nextProps) {
    const newState = this.getStateFromProps(nextProps);
    this.setState({ ...newState });
  }

  getStateFromProps = (props) => {
    const { totalRecords, pageLimit, currentPage = 1, pageNeighbours = 0 } = props;

    return {
      currentPage,
      pageLimit,
      totalRecords,
      pageNeighbours: this.calculatorPageNeighbours(pageNeighbours),
      totalPages: this.calculatorTotalPage(totalRecords, pageLimit) || 0
    };
  };

  gotoPage = page => {
    const { onPageChanged = f => f } = this.props;
    const currentPage = Math.max(0, Math.min(page, this.state.totalPages));

    const paginationData = {
      currentPage,
      totalPages: this.state.totalPages,
      pageLimit: this.state.pageLimit,
      totalRecords: this.state.totalRecords
    };

    this.setState({ currentPage }, () => onPageChanged(paginationData));
  };

  handlePageClick = (page) => (evt) => {
    evt.preventDefault();
    this.gotoPage(page);
  };

  fetchPageNumbers = () => {
    const { currentPage, pageNeighbours, totalPages } = this.state;
    const totalNumbers = pageNeighbours * 2 + 3;
    const totalBlocks = totalNumbers + 2;

    if (totalPages > totalBlocks) {
      const leftBound = currentPage - pageNeighbours;
      const rightBound = currentPage + pageNeighbours;
      const beforeLastPage = totalPages - 1;

      const startPage = leftBound > 2 ? leftBound : 2;
      const endPage = rightBound < beforeLastPage ? rightBound : beforeLastPage;
      let pages = range(startPage, endPage);

      const pagesCount = pages.length;
      const singleSpillOffset = totalNumbers - pagesCount - 1;

      const leftSpill = startPage > 2;
      const rightSpill = endPage < beforeLastPage;

      const leftSpillPage = LEFT_PAGE;
      const rightSpillPage = RIGHT_PAGE;

      if (leftSpill && !rightSpill) {
        const extraPages = range(startPage - singleSpillOffset, startPage - 1);
        pages = [leftSpillPage, ...extraPages, ...pages];
      } else if (!leftSpill && rightSpill) {
        const extraPages = range(endPage + 1, endPage + singleSpillOffset);
        pages = [...pages, ...extraPages, rightSpillPage];
      } else if (leftSpill && rightSpill) {
        pages = [leftSpillPage, ...pages, rightSpillPage];
      }

      return [1, ...pages, totalPages];
    }

    return range(1, totalPages);
  };

  renderPage(page, index) {
    const { pageNeighbours, currentPage } = this.state;
    let item = page;
    let pageClick = page;

    if (page === RIGHT_PAGE) {
      item = <span aria-hidden="true">&raquo;</span>;
      pageClick = currentPage + pageNeighbours * 2 + 1;
    }

    if (page === LEFT_PAGE) {
      item = <span aria-hidden="true">&laquo;</span>;
      pageClick = currentPage - pageNeighbours * 2 - 1;
    }

    return (
      <li key={index} className={`page-item ${currentPage === page ? 'active' : ''}`}>
        <a
          className="page-link"
          href="#"
          onClick={this.handlePageClick(pageClick)}
        >
          {item}
        </a>
      </li>
    );
  }

  render() {
    if (!this.state.totalRecords || this.state.totalPages === 1) {
      return null;
    }

    const pages = this.fetchPageNumbers();

    return (
      <Fragment>
        <nav aria-label="pagination">
          <ul className="pagination pagination-content-center">
            {pages.map((page, index) => this.renderPage(page, index))}
          </ul>
        </nav>
      </Fragment>
    );
  }
}

Pagination.propTypes = {
  totalRecords: PropTypes.number.isRequired,
  pageLimit: PropTypes.number,
  pageNeighbours: PropTypes.number,
  onPageChanged: PropTypes.func,
  currentPage: PropTypes.number
};

export default Pagination;
