import React from 'react';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import messages from 'components/Sidebar/messages';
const classNames = require('classnames');

export default function Sidebar() {
  const { location: { pathname } } = window;
  const activeUser = pathname === '/users' || pathname === '/users-create';
  return (
    <aside className="main-sidebar">
      <div className="slimScrollDiv">
        <section className="sidebar">
          <ul className="sidebar-menu tree" data-widget="tree">
            <li className="header">
              <FormattedMessage {...messages.main_navigation}/>
            </li>
            <li className={classNames({ active: pathname === '/' })}>
              <Link to='/'>
                <i className="fa fa-dashboard"/>
                <span><FormattedMessage {...messages.dashboard}/></span>
              </Link>
            </li>
            <li className={classNames('treeview', { active: activeUser })}>
              <a href="#">
                <i className="fa fa-user"/>
                <span><FormattedMessage {...messages.users}/></span>
                <span className="pull-right-container">
                  <i className="fa fa-angle-left pull-right"/>
                </span>
              </a>
              <ul className="treeview-menu">
                <li className={classNames({ active: pathname === '/users' })}>
                  <Link to='/users'>
                    <i className="fa fa-circle-o"/> <FormattedMessage {...messages.list}/>
                  </Link>
                </li>
                <li className={classNames({ active: pathname === '/users-create' })}>
                  <Link to='/users-create'>
                    <i className="fa fa-circle-o"/> <FormattedMessage {...messages.create}/>
                  </Link>
                </li>
              </ul>
            </li>
            <li className={classNames({ active: pathname === '/company' })}>
              <Link to='/company'>
                <i className="fa fa-building"/>
                <span><FormattedMessage {...messages.company}/></span>
              </Link>
            </li>
          </ul>
        </section>
      </div>
    </aside>
  );
}
