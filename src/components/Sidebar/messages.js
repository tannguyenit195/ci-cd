/*
 * Footer Messages
 *
 * This contains all the text for the Footer component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  dashboard: {
    id: 'components.Sidebar.dashboard',
    defaultMessage: 'Dashboard',
  },
  users: {
    id: 'components.Sidebar.users',
    defaultMessage: 'Users',
  },
  company: {
    id: 'components.Sidebar.company',
    defaultMessage: 'Company',
  },
  main_navigation: {
    id: 'components.Sidebar.main_navigation',
    defaultMessage: 'MAIN NAVIGATION',
  },
  list: {
    id: 'components.Sidebar.list',
    defaultMessage: 'List',
  },
  create: {
    id: 'components.Sidebar.create',
    defaultMessage: 'Create',
  },
});
