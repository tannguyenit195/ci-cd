import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import messagesCommon from 'translations/messagesCommon';
import { Button, MenuItem, Select, TextField } from '@material-ui/core';
import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import { KeyboardArrowDown } from '@material-ui/icons';
import { LANGUAGES, TIME_ZONE_ALL } from 'utils/constants';

class UserSettingComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...props.user,
      new_password: '',
      confirm_password: '',
      isEdit: false
    };
    this.updatedUser = false;
    this.onChangeInput = this.onChangeInput.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }

  static propTypes = {
    isFetching: PropTypes.bool,
    user: PropTypes.object,
    submitUser: PropTypes.func
  };

  handleCancel = () => {
    this.setState({
      ...this.props.user,
      error: null,
      new_password: '',
      confirm_password: '',
      isEdit: false
    });
  };

  componentWillReceiveProps(nextProps) {
    if (!!nextProps.user && !this.updatedUser) {
      this.setState({ ...nextProps.user });
      this.updatedUser = true;
    }
  }

  handleEdit = () => {
    const {
      isEdit, id, first_name, last_name, timezone,
      email, new_password, confirm_password, phone, language
    } = this.state;
    const { submitUser } = this.props;

    if (isEdit) {
      const data = {
        id,
        email: email || '',
        first_name: first_name || '',
        last_name: last_name || '',
        phone: phone || '',
        timezone: timezone || '',
        language
      };

      if (!!new_password && new_password !== confirm_password) {
        this.setState({ error: 'Password is not matching' });
        return;
      }

      if (!!new_password) {
        data.password = new_password;
      }
      submitUser(data);
      this.setState({
        isEdit: false,
        error: null
      });
    } else {
      this.setState({ isEdit: true });
    }
  };

  renderActions() {
    const { isEdit } = this.state;
    const { isFetching } = this.props;
    const text = intl.formatMessage(messagesCommon[isEdit ? 'save' : 'edit']);
    const elm = isFetching ? <i className="fa fa-spin fa-spinner"/> : text;

    return (
      <div className="actions">
        <Button
          variant="contained"
          size="large"
          onClick={this.handleCancel}
          disabled={!isEdit}
        >
          {intl.formatMessage(messagesCommon.cancel)}
        </Button>
        <Button
          variant="contained"
          color="primary"
          size="large"
          onClick={this.handleEdit}
          disabled={isFetching}
        >
          {elm}
        </Button>
      </div>
    );
  }

  onChangeInput = (event) => {
    const { target: { name, value } } = event;
    this.setState({ [name]: value });
  };

  onChangeSelect = (id) => (event) => {
    this.setState({ [id]: event.target.value });
  };

  renderInput(id, type, label, value) {
    const { isEdit } = this.state;

    return (
      <div className="inputs form-group">
        <label htmlFor={id}>{label}</label>
        <TextField
          className="form-control"
          disabled={!isEdit}
          type={type}
          name={id}
          inputProps={{
            id,
            className: 'input-cus',
            autoComplete: 'off'
          }}
          value={value || ''}
          onChange={this.onChangeInput}
          variant={isEdit ? 'outlined' : 'standard'}
        />
      </div>
    );
  };

  renderSelectBox(id, label, value = '') {
    const { isEdit } = this.state;
    const isDisable = this.props.isFetching || !isEdit;
    const list = id === 'language' ? LANGUAGES : TIME_ZONE_ALL;

    return (
      <div className="inputs form-group selects">
        <label>{label}</label>
        <Select
          value={value}
          onChange={this.onChangeSelect(id)}
          inputProps={{
            id,
            name: id
          }}
          className={`form-control select-box ${!isDisable ? 'active' : 'disabled'}`}
          disabled={isDisable}
          disableUnderline={isEdit}
          variant={isEdit ? 'outlined' : 'standard'}
          IconComponent={KeyboardArrowDown}
        >
          {list.map(({ id, text }) => <MenuItem key={id} value={id}>{text}</MenuItem>)}
        </Select>
      </div>
    );
  };

  renderError(error) {
    return (
      <div className="has-error">
        <span>{error}</span>
      </div>
    );
  }

  render() {
    const {
      isEdit, first_name, last_name, phone, email, error = null,
      language, new_password, confirm_password, timezone = 'Asia/Tokyo'
    } = this.state;

    return (
      <Fragment>
        <section className="content">
          <div className="row">
            <div className="col-xs-12">
              <div className="box box-info">
                <div className="box-header with-border">
                  <h3 className="box-title">
                    {intl.formatMessage(messagesCommon.profile_setting)}
                  </h3>
                </div>
                <div className="box-body">
                  <div className="col-md-6">
                    {!isEdit && this.renderInput('user_name', 'text', intl.formatMessage(messagesCommon.user_name), `${first_name} ${last_name}`)}
                    {isEdit && this.renderInput('first_name', 'text', intl.formatMessage(messagesCommon.user_first_name), first_name)}
                    {isEdit && this.renderInput('last_name', 'text', intl.formatMessage(messagesCommon.user_last_name), last_name)}
                    {this.renderInput('email', 'email', intl.formatMessage(messagesCommon.email_address), email)}
                    {isEdit && this.renderInput('new_password', 'password', intl.formatMessage(messagesCommon.new_password), new_password)}
                    {isEdit && this.renderInput('confirm_password', 'password', intl.formatMessage(messagesCommon.password_confirmation), confirm_password)}
                    {error && this.renderError(error)}
                    {this.renderInput('phone', 'text', intl.formatMessage(messagesCommon.tel), phone)}
                  </div>
                  <div className="col-md-6">
                    {this.renderSelectBox('language', intl.formatMessage(messagesCommon.language), language)}
                    {this.renderSelectBox('timezone', intl.formatMessage(messagesCommon.time_zone), timezone)}
                  </div>
                </div>
                <div className="box-footer text-center">
                  {this.renderActions()}
                </div>
              </div>
            </div>
          </div>
        </section>
      </Fragment>
    );
  }
}

UserSettingComponent.propTypes = {
  createUser: PropTypes.func,
  auth: PropTypes.object
};

export default UserSettingComponent;
