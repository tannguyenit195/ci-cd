/*
 * User Messages
 *
 * This contains all the text for the User component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  first_name: {
    id: 'components.Users.first_name',
    defaultMessage: 'First name'
  },
  last_name: {
    id: 'components.Users.last_name',
    defaultMessage: 'Last name'
  },
  user_name: {
    id: 'components.Users.user_name',
    defaultMessage: 'Username'
  },
  authority: {
    id: 'components.Users.authority',
    defaultMessage: 'Authority'
  },
  last_login: {
    id: 'components.Users.last_login',
    defaultMessage: 'Last login'
  },
  operating: {
    id: 'components.Users.operating',
    defaultMessage: 'Operating'
  },
  user_list: {
    id: 'components.Users.user_list',
    defaultMessage: 'User List'
  },
  user_information: {
    id: 'components.Users.user_information',
    defaultMessage: 'User Information'
  },
  general_user: {
    id: 'components.Users.general_user',
    defaultMessage: 'General user'
  },
  corporate_manager: {
    id: 'components.Users.corporate_manager',
    defaultMessage: 'Corporate manager'
  },
});
