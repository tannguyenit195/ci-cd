import React, { Component } from 'react';
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent/DialogContent';
import TextField from '@material-ui/core/TextField/TextField';
import DialogActions from '@material-ui/core/DialogActions/DialogActions';
import Button from '@material-ui/core/Button/Button';
import Dialog from '@material-ui/core/Dialog/Dialog';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import messages from 'components/Users/messages';
import messagesCommon from 'translations/messagesCommon';
import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import RadioGroup from '@material-ui/core/RadioGroup/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel/FormControlLabel';
import Radio from '@material-ui/core/Radio/Radio';

const classNames = require('classnames');

export default class UserDialog extends Component {
  state = {
    open: false,
    edit: false,
    editing: {
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      password: '',
      passwordConfirm: '',
      authority: '1'
    }
  };

  static propTypes = {
    open: PropTypes.bool,
    editing: PropTypes.object,
    close: PropTypes.func,
    update: PropTypes.func
  };

  handleClose = () => {
    this.setState({ edit: false });
    this.props.close();
  };

  componentWillReceiveProps(nextProps) {
    const { editing } = nextProps;
    this.setState({
      editing: {
        ...this.state.editing,
        ...editing
      }
    });
  }

  handleChange = (e) => {
    const { target: { value, name } } = e;
    this.setState({
      editing: {
        ...this.state.editing,
        [name]: value
      }
    });
  };

  handleSave = () => {
    const { editing } = this.state;
    this.props.update(editing);
  };

  handleEditForm = () => {
    this.setState({ edit: true });
  };

  renderRadioInput(edit) {
    return (
      <div className="form-group">
        <label htmlFor="authority" className={classNames({
          disable_label: !edit,
          enabled_label: edit
        })}>
          {intl.formatMessage(messages.authority)}
        </label>
        <RadioGroup className="radio-group" name="authority" value={this.state.editing.authority} onChange={this.handleChange}>
          <FormControlLabel value="1" disabled={!edit} control={<Radio/>} label={intl.formatMessage(messages.general_user)}/>
          <FormControlLabel value="0" disabled={!edit} control={<Radio/>} label={intl.formatMessage(messages.corporate_manager)}/>
        </RadioGroup>
      </div>
    );
  }

  renderInput(edit, value, name, label) {
    return (
      <TextField
        disabled={!edit}
        margin="dense"
        label={label}
        name={name}
        className="text-field"
        type="text"
        onChange={this.handleChange}
        defaultValue={value}
        fullWidth
      />
    );
  }

  render() {
    const { open } = this.props;
    const { edit, editing } = this.state;
    return (
      <Dialog
        open={open}
        onClose={this.handleClose}
        maxWidth="lg"
        aria-labelledby="form-dialog-title"
        className="dialog-wrapper"
      >
        <DialogTitle className="dialog-title">
          <FormattedMessage {...messages.user_information}/>
        </DialogTitle>
        <DialogContent>
          {this.renderInput(edit, editing.firstName, 'firstName', intl.formatMessage(messages.first_name))}
          {this.renderInput(edit, editing.lastName, 'lastName', intl.formatMessage(messages.last_name))}
          {this.renderInput(edit, editing.email, 'email', intl.formatMessage(messagesCommon.email))}
          {edit && this.renderInput(edit, '', 'password', intl.formatMessage(messagesCommon.password))}
          {edit && this.renderInput(edit, '', 'passwordConfirm', intl.formatMessage(messagesCommon.password_confirm))}
          {this.renderInput(edit, editing.phone, 'phone', intl.formatMessage(messagesCommon.phone))}
          {this.renderRadioInput(edit)}
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleClose} color="default" variant="contained">
            <FormattedMessage {...messagesCommon.cancel}/>
          </Button>
          {
            !edit
            && <Button onClick={this.handleEditForm} color="primary" variant="contained">
              <FormattedMessage {...messagesCommon.edit}/>
            </Button>
          }
          {
            edit
            && <Button onClick={this.handleSave} color="primary" variant="contained">
              <FormattedMessage {...messagesCommon.save}/>
            </Button>
          }
        </DialogActions>
      </Dialog>
    );
  }
}
