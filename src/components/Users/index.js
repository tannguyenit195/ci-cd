import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Pagination from 'components/Pagination';
import { PAGE_NEIGHBOURS } from 'utils/constants';
import UserDialog from 'components/Users/Dialog';
import messages from 'components/Users/messages';
import { FormattedMessage } from 'react-intl';
import SearchInput from 'components/SearchInput';
import DialogDelete from 'components/DialogDelete';
import { DELETE_DIALOG, USER_DIALOG } from 'containers/Users/constants';
import messagesCommon from 'translations/messagesCommon';

class User extends React.Component {
  state = {
    currentPage: 1,
    deleteId: '',
    editing: {
      id: '',
      firstName: '',
      lastName: '',
      email: '',
      phone: ''
    }
  };

  handleClickOpen = (id) => () => {
    const { data } = this.props;
    const user = data.find(item => item.id === id);
    this.setState({
      editing: {
        id,
        firstName: user.first_name,
        lastName: user.last_name,
        email: user.email,
        phone: user.phone
      }
    }, () => this.props.openDialog(USER_DIALOG));
  };


  handleClose = () => {
    this.props.openDialog(USER_DIALOG);
  };

  searchUser = (data) => {
    this.setState({ currentPage: 1 });
    this.props.search(data);
  };

  handleDelete = (id) => () => {
    this.setState({ deleteId: id });
    this.props.openDialog(DELETE_DIALOG);
  };

  acceptDelete = () => {
    const { data } = this.props;
    const { currentPage, deleteId } = this.state;
    const page = data.length === 1 ? currentPage - 1 : currentPage;
    this.props.deleteUser({
      id: deleteId,
      page
    });
  };

  onPageChanged = (data) => {
    const { currentPage } = data;
    this.setState({
      currentPage
    });
    this.props.onPageChanged(data);
  };

  updateUser = (data) => {
    const { currentPage } = this.state;
    data.page = currentPage;
    this.props.updateUser(data);
  };

  componentWillReceiveProps(nextProps) {
    const { currentPage } = nextProps;
    this.setState({ currentPage });
  }

  renderItem(id, items) {
    let item = items;
    if (Array.isArray(items)) {
      item = items.map((item) => `<span class="label label-success">${item.display_name}</span>`);
    }

    return (
      <td onClick={this.handleClickOpen(id)}>
        <div dangerouslySetInnerHTML={{ __html: item }}/>
      </td>
    );
  }

  render() {
    const { data, total, perPage, openDialogUser, openDialogDelete } = this.props;
    const { currentPage } = this.state;

    return (
      <Fragment>
        <section className="content">
          <div className="row">
            <div className="col-xs-12">
              <div className="box box-info">
                <div className="box-header">
                  <h3 className="box-title">
                    <FormattedMessage {...messages.user_list}/>
                  </h3>
                  <div className="box-tools">
                    <SearchInput search={this.searchUser}/>
                  </div>
                </div>
                <div className="box-body table-responsive">
                  <table className="table table-hover table-border">
                    <thead>
                    <tr>
                      <th><FormattedMessage {...messagesCommon.index}/></th>
                      <th><FormattedMessage {...messages.user_name}/></th>
                      <th><FormattedMessage {...messagesCommon.email}/></th>
                      <th><FormattedMessage {...messages.authority}/></th>
                      <th><FormattedMessage {...messages.last_login}/></th>
                      <th><FormattedMessage {...messages.operating}/></th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                      data.map((item, index) => (
                        <tr key={index}>
                          {this.renderItem(item.id, (currentPage - 1) * 10 + index + 1)}
                          {this.renderItem(item.id, `${item.first_name} ${item.last_name}`)}
                          {this.renderItem(item.id, item.email)}
                          {this.renderItem(item.id, item.roles.data)}
                          {this.renderItem(item.id, item.last_login_at || '--')}
                          <td onClick={this.handleDelete(item.id)}>
                            <a href="#"><i className="fa fa-trash" aria-hidden="true"/></a>
                          </td>
                        </tr>
                      ))
                    }
                    </tbody>
                  </table>
                  <Pagination
                    totalRecords={total}
                    currentPage={currentPage}
                    pageLimit={perPage}
                    pageNeighbours={PAGE_NEIGHBOURS}
                    onPageChanged={this.onPageChanged}
                  />
                </div>
              </div>
            </div>
          </div>
        </section>
        <UserDialog
          open={openDialogUser}
          close={this.handleClose}
          editing={this.state.editing}
          update={this.updateUser}
        />
        <DialogDelete
          open={openDialogDelete}
          close={() => this.props.openDialog(DELETE_DIALOG)}
          accept={this.acceptDelete}
        />
      </Fragment>
    );
  }
}

User.propTypes = {
  data: PropTypes.array,
  total: PropTypes.number,
  perPage: PropTypes.number,
  currentPage: PropTypes.number,
  openDialogUser: PropTypes.bool,
  openDialogDelete: PropTypes.bool,
  onPageChanged: PropTypes.func,
  search: PropTypes.func,
  deleteUser: PropTypes.func,
  updateUser: PropTypes.func,
  openDialog: PropTypes.func
};

export default User;
