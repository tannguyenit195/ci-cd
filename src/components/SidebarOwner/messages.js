/*
 * Footer Messages
 *
 * This contains all the text for the Footer component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  dashboard: {
    id: 'components.Sidebar.dashboard',
    defaultMessage: 'Dashboard',
  },
  users: {
    id: 'components.Sidebar.users',
    defaultMessage: 'Users',
  },
  company: {
    id: 'components.Sidebar.company',
    defaultMessage: 'Company',
  },
  company_detail: {
    id: 'components.Sidebar.company_detail',
    defaultMessage: 'Company Detail',
  },
  drones: {
    id: 'components.Sidebar.drones',
    defaultMessage: 'Drones',
  },
  setting: {
    id: 'components.Sidebar.setting',
    defaultMessage: 'Setting',
  },
  missions: {
    id: 'components.Sidebar.missions',
    defaultMessage: 'Missions',
  },
  payment_histories: {
    id: 'components.Sidebar.payment_histories',
    defaultMessage: 'Payment Histories',
  },
  debit_invoices: {
    id: 'components.Sidebar.debit_invoices',
    defaultMessage: 'Debit Invoices',
  },
  flight: {
    id: 'components.Sidebar.flight',
    defaultMessage: 'Flights',
  },
  main_navigation: {
    id: 'components.Sidebar.main_navigation',
    defaultMessage: 'MAIN NAVIGATION',
  },
  list: {
    id: 'components.Sidebar.list',
    defaultMessage: 'List',
  },
  create: {
    id: 'components.Sidebar.create',
    defaultMessage: 'Create',
  },
});
