import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import cls from 'classnames';
import { union } from 'lodash'

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import msg from './messages';
import messages from 'components/Sidebar/messages';
import { FormattedMessage } from 'react-intl';

const { formatMessage: trans } = intl;

function Sidebar({ history }) {
  const { location: { pathname } } = history;
  const companyActive = pathname === '/owner/companies'
    || pathname === '/owner' || pathname === '/owner/companies/create';
  const isCompanyDetail = pathname.includes('/owner/companies/')
    && pathname !== '/owner/companies/create';
  const companyId = pathname.split('/')[3];
  const missionId = pathname.split('/')[5];

  return (
    <aside className="main-sidebar">
      <div className="slimScrollDiv">
        <section className="sidebar">
          <ul className="sidebar-menu tree" data-widget="tree">
            <li className="header">
              <FormattedMessage {...messages.main_navigation}/>
            </li>
            <li className={cls('treeview', { active: companyActive })}>
              <Link to="#">
                <i className="fa fa-building"/>
                <span>{trans(msg.company)}</span>
                <span className="pull-right-container">
                  <i className="fa fa-angle-left pull-right"/>
                </span>
              </Link>
              <ul className="treeview-menu">
                <li className={cls({ active: pathname === '/companies' || pathname === '/' })}>
                  <Link to='/owner/companies'>
                    <i className="fa fa-circle-o"/> <FormattedMessage {...messages.list}/>
                  </Link>
                </li>
                <li className={cls({ active: pathname === '/companies/create' })}>
                  <Link to='/owner/companies/create'>
                    <i className="fa fa-circle-o"/> <FormattedMessage {...messages.create}/>
                  </Link>
                </li>
              </ul>
            </li>
            {isCompanyDetail && (
              <li className={cls('treeview', 'active')}>
                <Link to={`/owner/companies/${companyId}`}>
                  <i className="fa fa-building"/>
                  <span>{trans(msg.company_detail)}</span>
                  <span className="pull-right-container">
                  <i className="fa fa-angle-left pull-right"/>
                </span>
                </Link>
                <ul className="treeview-menu">
                  <li className={cls({ active: pathname === `/companies/${companyId}` })}>
                    <Link to={union(`/owner/companies/${companyId}`.split('/')).join('/')}>
                      <i className="fa fa-circle-o"/>
                      {trans(msg.setting)}
                    </Link>
                  </li>
                  <li className={cls({ active: pathname.includes('/users') })}>
                    <Link to={union(`/owner/companies/${companyId}/users`.split('/')).join('/')}>
                      <i className="fa fa-circle-o"/>
                      {trans(msg.users)}
                    </Link>
                  </li>
                  <li className={cls({ active: pathname.includes('/drones') })}>
                    <Link to={union(`/owner/companies/${companyId}/drones`.split('/')).join('/')}>
                      <i className="fa fa-circle-o"/>
                      {trans(msg.drones)}
                    </Link>
                  </li>
                  <li className={cls({ active: (pathname.includes('/missions') || pathname.includes('/flights') || pathname.includes('/logs')) })}>
                    <Link to={union(`/owner/companies/${companyId}/missions`.split('/')).join('/')}>
                      <i className="fa fa-circle-o"/>
                      <i className="fa fa-angle-left pull-right"/>
                      {trans(msg.missions)}
                    </Link>
                    <ul className={cls('treeview-menu', { active: (pathname.includes('/flights') || pathname.includes('/logs')) })}>
                      <li className={cls({ active: (pathname.includes('/flights') || pathname.includes('/logs')) })}>
                        <Link to={union(`/owner/companies/${companyId}/flights/${missionId}`.split('/')).join('/')}>
                          <i className="fa fa-circle-o"/>
                          {trans(msg.flight)}
                        </Link>
                      </li>
                    </ul>
                  </li>

                  <li className={cls({ active: pathname.includes('/payment-histories') })}>
                    <Link to={union(`/owner/companies/${companyId}/payment-histories`.split('/')).join('/')}>
                      <i className="fa fa-circle-o"/>
                      {trans(msg.payment_histories)}
                    </Link>
                  </li>
                  <li className={cls({ active: pathname.includes('/debit-invoices') })}>
                    <Link to={union(`/owner/companies/${companyId}/debit-invoices`.split('/')).join('/')}>
                      <i className="fa fa-circle-o"/>
                      {trans(msg.debit_invoices)}
                    </Link>
                  </li>
                </ul>
              </li>
            )}
          </ul>
        </section>
      </div>
    </aside>
  );
}

Sidebar.propTypes = {
  history: PropTypes.object
};

export default Sidebar;
