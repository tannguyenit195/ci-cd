import FormControl from '@material-ui/core/FormControl/FormControl';
import InputLabel from '@material-ui/core/InputLabel/InputLabel';
import React, { Component } from 'react';
import InputAdornment from '@material-ui/core/InputAdornment/InputAdornment';
import IconButton from '@material-ui/core/IconButton/IconButton';
import Input from '@material-ui/core/Input';
import { FormattedMessage } from 'react-intl';
import messages from 'components/SearchInput/messages';
import PropTypes from 'prop-types';

export default class SearchInput extends Component {
  state = {
    text: ''
  };

  static propTypes = {
    search: PropTypes.func
  };

  handleSearch = () => {
    this.props.search(this.state.text);
  };

  handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.props.search(this.state.text);
    }
  };

  handleChange = (e) => {
    const { target: { value } } = e;
    this.setState({ text: value });
  };

  render() {
    return (
      <FormControl className="form-search-user">
        <InputLabel htmlFor="adornment-search" className="label-search">
          <FormattedMessage {...messages.search} />
        </InputLabel>
        <Input
          id="adornment-search"
          type="text"
          onChange={this.handleChange}
          onKeyPress={this.handleKeyPress}
          endAdornment={
            <InputAdornment position="end" style={{ marginBottom: 15 }} onClick={this.handleSearch}>
              <IconButton aria-label="Toggle password visibility">
                <i className="fa fa-search" aria-hidden="true"/>
              </IconButton>
            </InputAdornment>
          }
        />
      </FormControl>
    );
  }
}
