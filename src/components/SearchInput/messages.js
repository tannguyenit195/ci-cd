/*
 * SearchInput Messages
 *
 * This contains all the text for the SearchInput component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  search: {
    id: 'components.SearchInput.search',
    defaultMessage: 'Search',
  }
});
