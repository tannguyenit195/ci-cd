import React from 'react';
import PropTypes from 'prop-types';

SectionContent.propTypes = {
  title: PropTypes.any,
  children: PropTypes.node
};
export default function SectionContent(props) {
  const { title, children } = props;

  return (
    <section className="content">
      <div className="row">
        <div className="col-xs-12">
          <div className="box box-info">
            <div className="box-header">
              <h3 className="box-title">
                {title}
              </h3>
            </div>
            <div className="box-body">
              {children}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
