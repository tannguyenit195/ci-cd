import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messages from 'containers/Author/messages';
import { ROLE_TYPE } from 'utils/constants';

class LoginForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      remember: false
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onChangeInput = this.onChangeInput.bind(this);
  }

  onChangeInput(event) {
    const { name, value, checked } = event.target;
    this.setState({ [name]: name === 'remember' ? checked : value });
  }

  renderInput(id, type) {
    return (
      <div className="form-group">
        <div className="controls row">
          <div className="input-group">
            <input
              id={id}
              type={type}
              name={id}
              className="form-control"
              value={this.state[id]}
              placeholder={intl.formatMessage(messages[id])}
              onChange={this.onChangeInput}
            />
          </div>
        </div>
      </div>
    );
  }

  renderRememberInput() {
    const { remember } = this.state;
    return (
      <div className="form-group">
        <div className="controls row">
          <div className="remember-session">
            <div className="custom-checkbox">
              <label className="custom-checkbox">
                {intl.formatMessage(messages.remember)}
                <input
                  type="checkbox"
                  name="remember"
                  checked={remember}
                  onChange={this.onChangeInput}
                />
                <span className="checkmark"/>
              </label>
            </div>
          </div>
        </div>
      </div>
    );
  }

  handleSubmit(event) {
    event.preventDefault();
    const { email, password } = this.state;
    if (this.props.isFetching) {
      return;
    }
    if (email && password) {
      this.props.submit({ ...this.state });
    }
  }

  render() {
    const { email, password } = this.state;
    const { error, isFetching, type } = this.props;
    const message = messages[error] ? intl.formatMessage(messages[error]) : error;

    return (
      <div className="login-box">
        <div className="header">{intl.formatMessage(messages.login_title)}</div>
        <form className="form-horizontal" onSubmit={this.handleSubmit}>
          <fieldset className="col-sm-12">
            {this.renderInput('email', 'email')}
            {this.renderInput('password', 'password')}
            {error && <span className="has-error">{message}</span>}
            {this.renderRememberInput()}
            <div className="form-group buttonbox">
              <div className="controls row">
                <button
                  type="submit"
                  className="btn btn-lg btn-primary col-xs-12"
                  disabled={isFetching || !email || !password}
                >
                  {isFetching
                    ? <span className="fa fa-spin fa-spinner"/>
                    : intl.formatMessage(messages.login_title)
                      .toUpperCase()}
                </button>
              </div>
            </div>

            <div className="form-group">
              <div className="controls row">
                <div className="col-sm-12 resetpassword">
                  <Link to={`/${type ? 'owner/' : ''}forgot-password`}>
                    {intl.formatMessage(messages.forgot_password)}
                  </Link>
                </div>
              </div>
            </div>
            {type !== ROLE_TYPE.OWNER && (
              <div className="form-group">
                <div className="controls row">
                  <div className="col-sm-12 static-link resetpassword">
                    <Link to="/terms" target="_blank">
                      {intl.formatMessage(messages.term)}
                    </Link>
                    <span className="space"> | </span>
                    <Link to="/privacy" target="_blank">
                      {intl.formatMessage(messages.privacy)}
                    </Link>
                    <span className="space"> | </span>
                    <Link to="http://www.terra-drone.net/" target="_blank">
                      {intl.formatMessage(messages.operation)}
                    </Link>
                  </div>
                </div>
              </div>
            )}
          </fieldset>
        </form>
      </div>
    );
  }
}

LoginForm.propTypes = {
  isFetching: PropTypes.bool,
  type: PropTypes.string,
  error: PropTypes.string,
  submit: PropTypes.func
};

export default LoginForm;
