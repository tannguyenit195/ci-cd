import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Button, Popper, Grow, Paper, ClickAwayListener
} from '@material-ui/core';

import LOGO from 'styles/image/svg/logo_utm.svg';
import { ROLE_TYPE } from 'utils/constants';
import { changeLocale } from 'containers/LanguageProvider/actions';

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import commonsMessage from 'translations/messagesCommon'

class AuthHeader extends React.Component {
  state = {
    openLanguage: false
  };

  handleToggle = () => {
    this.setState(state => ({ openLanguage: !state.openLanguage }));
  };

  handleClose = event => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }

    this.setState({ openLanguage: false });
  };

  handleChangeLanguage = (language) => () => {
    this.props.changeLanguage(language);
    this.setState({ openLanguage: false });
  };

  renderLanguage() {
    const { openLanguage } = this.state;
    const { language } = this.props;
    const newLanguage = language === 'en' ? 'ja' : 'en';

    return (
      <Popper
        className="language-popper"
        open={openLanguage}
        anchorEl={this.anchorEl}
        transition
        disablePortal
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
          >
            <Paper>
              <ClickAwayListener onClickAway={this.handleClose}>
                <Button
                  className="language-btn"
                  onClick={this.handleChangeLanguage(newLanguage)}
                >{newLanguage.toUpperCase()}</Button>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    );
  }

  render() {
    const { type = '', language } = this.props;
    const { openLanguage } = this.state;

    return (
      <header className={`AuthHeader ${type}`}>
        <div className="AuthHeader--logo">
          <Link to="/">
            <img src={LOGO} alt="UTM logo"/>
          </Link>
        </div>
        {type === ROLE_TYPE.OWNER && (
          <div className="AuthHeader--language">
            <h4>{intl.formatMessage(commonsMessage.language)}</h4>
            <Button
              buttonRef={node => {
                this.anchorEl = node;
              }}
              className="language-btn"
              aria-owns={openLanguage ? 'menu-list-grow' : undefined}
              aria-haspopup="true"
              onClick={this.handleToggle}
            >{language}</Button>
            {this.renderLanguage()}
          </div>
        )}
      </header>
    );
  }
}

AuthHeader.propTypes = {
  type: PropTypes.string,
  language: PropTypes.string,
  changeLanguage: PropTypes.func
};

const withConnect = connect(
  state => ({ language: state.language.locale }),
  dispatch => ({
    changeLanguage: (language) => dispatch(changeLocale(language))
  })
);

export default withConnect(AuthHeader);
