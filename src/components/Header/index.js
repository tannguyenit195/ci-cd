import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import Cookie from 'js-cookie';

import logoUTM from 'styles/image/svg/logo_utm.svg';
import favicon from 'styles/image/favicon/favicon.ico';
import userDefault from 'styles/image/default/user.png';
import messages from './messages';
import { userLogoutRequest } from 'containers/App/action';
import { ROLE_TYPE } from 'utils/constants';

class Header extends React.Component {
  handleLogout = () => {
    const { userLogout, history } = this.props;
    Cookie.remove('micro_token');
    Cookie.remove('micro_role');
    userLogout();
    history.push('/login');
  };

  render() {
    const { user } = this.props;
    const username = user ? `${user.first_name}  ${user.last_name}` : '';
    const role = Cookie.get('micro_role');
    const profileUrl = role === ROLE_TYPE.OWNER ? '/owner/user-profile' : '/user-profile';

    return (
      <div>
        <header className="main-header">
          <a href="/" className="logo">
            <span className="logo-mini"><img src={favicon} alt="logo" className="logo-terra-drone"/></span>
            <span className="logo-lg"><img src={logoUTM} alt="logo" height="30" className="logo-terra-drone"/></span>
          </a>
          <nav className="navbar navbar-static-top">
            <a href="/" className="sidebar-toggle" data-toggle="push-menu" role="button">
              <span className="sr-only"/>
            </a>
            <div className="navbar-custom-menu">
              <ul className="nav navbar-nav">
                <li className="dropdown user user-menu">
                  <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                    <img src={userDefault} className="user-image" alt="User Image"/>
                    <span className="hidden-xs">{username}</span>
                  </a>
                  <ul className="dropdown-menu">
                    <li className="user-header">
                      <img src={userDefault} className="img-circle" alt="User Image"/>
                      <p>
                        {username}
                      </p>
                    </li>
                    <li className="user-footer">
                      <div className="pull-left">
                        <Link to={profileUrl} className="btn btn-default btn-flat">
                          <FormattedMessage {...messages.profile}/>
                        </Link>
                      </div>
                      <div className="pull-right">
                        <button onClick={this.handleLogout} className="btn btn-default btn-flat">
                          <FormattedMessage {...messages.signOut}/>
                        </button>
                      </div>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </nav>
        </header>
      </div>
    );
  }
}

Header.propTypes = {
  user: PropTypes.object,
  userLogout: PropTypes.func,
  history: PropTypes.object
};

const mapStateToProps = (state) => ({
  user: state.global.user
});

const mapDispatchToProps = dispatch => ({
  userLogout: () => dispatch(userLogoutRequest())
});

const withConnect = connect(
  mapStateToProps, mapDispatchToProps
);

export default compose(
  withConnect
)(Header);
