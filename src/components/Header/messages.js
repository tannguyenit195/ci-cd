/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  signOut: {
    id: 'components.Header.signOut',
    defaultMessage: 'Sign out',
  },
  profile: {
    id: 'components.Header.profile',
    defaultMessage: 'Profile',
  }
});
