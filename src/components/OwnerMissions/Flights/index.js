import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import Pagination from 'components/Pagination';
import SearchInput from 'components/SearchInput';
import { PAGE_NEIGHBOURS } from 'utils/constants';
import messages from '../messages';

class Missions extends React.Component {
  state = {
    currentPage: 1
  };

  searchFlight = (data) => {
    this.setState({ currentPage: 1 });
    this.props.search(data);
  };

  onPageChanged = (data) => {
    this.setState({ currentPage: data.currentPage });
    this.props.onPageChanged(data);
  };

  componentWillReceiveProps(nextProps) {
    const { currentPage } = nextProps;
    this.setState({ currentPage });
  }

  goToDetail = logId => () => {
    this.props.goDetail(logId);
  };

  render() {
    const { data, total, perPage } = this.props;
    const { currentPage } = this.state;
    return (
      <Fragment>
        <section className="content">
          <div className="row">
            <div className="col-xs-12">
              <div className="box box-info">
                <div className="box-header">
                  <h3 className="box-title">
                    <FormattedMessage {...messages.flights_list}/>
                  </h3>
                  <div className="box-tools">
                    <SearchInput search={this.searchFlight}/>
                  </div>
                </div>
                <div className="box-body table-responsive">
                  <table className="table table-hover table-border">
                    <thead>
                    <tr>
                      <th><FormattedMessage {...messages.create_time}/></th>
                      <th><FormattedMessage {...messages.flight_time}/></th>
                      <th><FormattedMessage {...messages.distance}/></th>
                      <th><FormattedMessage {...messages.status}/></th>
                    </tr>
                    </thead>
                    <tbody>
                    {data.length === 0 && <tr>
                      <td>
                        <FormattedMessage {...messages.no_data}/>
                      </td>
                    </tr>}
                    {
                      data.map((item, index) => (
                        <tr key={index} onClick={this.goToDetail(item.id)}>
                          <td>{item.name}</td>
                          <td>{item.time}</td>
                          <td>{item.distance}</td>
                          <td>{item.status}</td>
                        </tr>
                      ))
                    }
                    </tbody>
                  </table>
                  <Pagination
                    totalRecords={total}
                    currentPage={currentPage}
                    pageLimit={perPage}
                    pageNeighbours={PAGE_NEIGHBOURS}
                    onPageChanged={this.onPageChanged}
                  />
                </div>
              </div>
            </div>
          </div>
        </section>
      </Fragment>
    );
  }
}

Missions.propTypes = {
  data: PropTypes.array,
  total: PropTypes.number,
  perPage: PropTypes.number,
  currentPage: PropTypes.number,
  history: PropTypes.object,
  goDetail: PropTypes.func,
  onPageChanged: PropTypes.func,
  search: PropTypes.func
};

export default Missions;
