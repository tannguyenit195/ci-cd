import React from 'react';
import PropTypes from 'prop-types';

import SectionContentComponent from 'components/SectionContent';
import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messages from '../messages';

function FlightLogComponent(props) {
  console.log('>>> FlightLogComponent', props);

  function renderRow(label, value) {
    return (
      <div className="row">
        <div className="col-md-4 col-sm-6">
          <span>{label}</span>
        </div>
        <div className="col-md-8 col-sm-6">
          <span>{value}</span>
        </div>
      </div>
    );
  }

  return (
    <SectionContentComponent>
      <div>
        {renderRow(intl.formatMessage(messages.created), '2019/03/13 17:22')}
        {renderRow(intl.formatMessage(messages.flight_time), '2 minute 20 second')}
        {renderRow(intl.formatMessage(messages.distance), '50m')}
        {renderRow(intl.formatMessage(messages.navigation_point), '5')}
        {renderRow(intl.formatMessage(messages.shooting_point), '5')}
        {renderRow(intl.formatMessage(messages.right_angle), '0')}
        {renderRow(intl.formatMessage(messages.weather), '-')}
        {renderRow(intl.formatMessage(messages.wind_speed), '-')}
        {renderRow(intl.formatMessage(messages.image_upload), '-')}
        {renderRow(intl.formatMessage(messages.status), '-')}
      </div>
    </SectionContentComponent>
  );
}

FlightLogComponent.propTypes = {
  drone: PropTypes.object
};

export default FlightLogComponent;
