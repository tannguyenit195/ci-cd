import { defineMessages } from 'react-intl';

export default defineMessages({
  mission_list: {
    id: 'components.Mission.mission_list',
    defaultMessage: 'Mission list'
  },
  mission_detail: {
    id: 'components.Mission.mission_detail',
    defaultMessage: 'Mission detail'
  },
  mission_name: {
    id: 'components.Mission.mission_name',
    defaultMessage: 'Mission name'
  },
  flights_list: {
    id: 'components.Mission.flights_list',
    defaultMessage: 'Flight list'
  },
  flights_detail: {
    id: 'components.Mission.flights_detail',
    defaultMessage: 'Flight detail'
  },
  flights_name: {
    id: 'components.Mission.flights_name',
    defaultMessage: 'Flight name'
  },
  number_of_flight: {
    id: 'components.Mission.number_of_flight',
    defaultMessage: 'Number of flight'
  },
  log_list: {
    id: 'components.Mission.log_list',
    defaultMessage: 'Log list'
  },
  updated: {
    id: 'components.Mission.drone_updated',
    defaultMessage: 'Updated'
  },
  flight_time: {
    id: 'components.Mission.flight_time',
    defaultMessage: 'Flight time'
  },
  create_time: {
    id: 'components.Mission.create_time',
    defaultMessage: 'Create time'
  },
  distance: {
    id: 'components.Mission.distance',
    defaultMessage: 'Distance'
  },
  status: {
    id: 'components.Mission.status',
    defaultMessage: 'Status'
  },
  log_detail: {
    id: 'components.Mission.log_detail',
    defaultMessage: 'Log detail'
  },
  no_data: {
    id: 'commons.no_data',
    defaultMessage: 'No data'
  },
  created: {
    id: 'components.Mission.create',
    defaultMessage: 'Create'
  },
  navigation_point: {
    id: 'components.Mission.navigation_point',
    defaultMessage: 'Navigation points'
  },
  shooting_point: {
    id: 'components.Mission.shooting_point',
    defaultMessage: 'Shooting points'
  },
  right_angle: {
    id: 'components.Mission.right_angle',
    defaultMessage: 'Right angle flight points'
  },
  weather: {
    id: 'components.Mission.weather',
    defaultMessage: 'Weather'
  },
  wind_speed: {
    id: 'components.Mission.wind_speed',
    defaultMessage: 'Wind speed'
  },
  image_upload: {
    id: 'components.Mission.image_upload',
    defaultMessage: 'Image upload mode'
  },
});
