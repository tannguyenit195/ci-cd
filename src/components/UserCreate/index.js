import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import RadioGroup from '@material-ui/core/RadioGroup/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel/FormControlLabel';
import Radio from '@material-ui/core/Radio/Radio';
import messagesCommon from 'translations/messagesCommon';
import { Button, TextField } from '@material-ui/core';
import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messages from './messages';

class UserCreateComponent extends React.Component {
  state = {
    first_name: '',
    last_name: '',
    email: '',
    password: '',
    password_confirm: '',
    phone: '',
    authority: '1',
    organization_id: ''
  };

  handleCancel = () => {
    this.setState({
      first_name: '',
      last_name: '',
      email: '',
      password: '',
      password_confirm: '',
      phone: '',
      authority: '1'
    });
  };

  componentWillReceiveProps(nextProps) {
    const { auth: { organization_id } } = nextProps;
    this.setState({ organization_id });
  }

  handleSave = () => {
    const data = this.state;
    this.props.createUser(data);
  };

  renderActions() {
    return (
      <div className="user-create-box">
        <Button
          variant="contained"
          size="large"
          onClick={this.handleCancel}
        >
          {intl.formatMessage(messagesCommon.cancel)}
        </Button>
        <Button
          variant="contained"
          size="large"
          color="primary"
          onClick={this.handleSave}
        >
          {intl.formatMessage(messagesCommon.save)}
        </Button>
      </div>
    );
  }

  onChangeInput = (event) => {
    const { target: { name, value } } = event;
    this.setState({ [name]: value });
  };

  renderInput(id, type, label, value) {
    return (
      <div className="inputs form-group">
        <label htmlFor={id}>{label}</label>
        <TextField
          className="form-control"
          type={type}
          name={id}
          value={value || ''}
          onChange={this.onChangeInput}
          variant="outlined"
        />
      </div>
    );
  }

  renderRadioInput() {
    return (
      <div className="form-group">
        <label htmlFor="authority">
          {intl.formatMessage(messages.authority)}
        </label>
        <RadioGroup
          aria-label="Gender"
          name="authority"
          value={this.state.authority}
          onChange={this.onChangeInput}
          className="radio-group"
        >
          <FormControlLabel value="1" control={<Radio/>} label={intl.formatMessage(messages.general_user)}/>
          <FormControlLabel value="0" control={<Radio/>} label={intl.formatMessage(messages.corporate_manager)}/>
        </RadioGroup>
      </div>
    );
  }

  render() {
    const {
      first_name,
      last_name,
      phone,
      email,
      password,
      password_confirm
    } = this.state;
    return (
      <Fragment>
        <section className="content">
          <div className="row">
            <div className="col-xs-12">
              <div className="box box-info">
                <div className="box-header with-border">
                  <h3 className="box-title">User Create</h3>
                </div>
                <div className="box-body">
                  <div className="col-md-6">
                    {this.renderInput('first_name', 'text', intl.formatMessage(messagesCommon.user_first_name), first_name)}
                    {this.renderInput('last_name', 'text', intl.formatMessage(messagesCommon.user_last_name), last_name)}
                    {this.renderInput('email', 'email', intl.formatMessage(messagesCommon.email), email)}
                    {this.renderInput('password', 'password', intl.formatMessage(messagesCommon.password), password)}
                    {this.renderInput('password_confirm', 'password', intl.formatMessage(messagesCommon.password_confirm), password_confirm)}
                    {this.renderInput('phone', 'text', intl.formatMessage(messagesCommon.phone), phone)}
                  </div>
                  <div className="col-md-6">
                    {this.renderRadioInput()}
                  </div>
                </div>
                <div className="box-footer text-center">
                  {this.renderActions()}
                </div>
              </div>
            </div>
          </div>
        </section>
      </Fragment>
    );
  }
}

UserCreateComponent.propTypes = {
  createUser: PropTypes.func,
  auth: PropTypes.object
};

export default UserCreateComponent;
