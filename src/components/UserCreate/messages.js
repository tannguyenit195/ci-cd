/*
 * User Messages
 *
 * This contains all the text for the User component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  authority: {
    id: 'components.Users.authority',
    defaultMessage: 'Authority'
  },
  general_user: {
    id: 'components.Users.general_user',
    defaultMessage: 'General user'
  },
  corporate_manager: {
    id: 'components.Users.corporate_manager',
    defaultMessage: 'Corporate manager'
  },
});
