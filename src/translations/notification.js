/*
 * Users Container Messages
 *
 * This contains all the text for the Users Container component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  success: {
    id: 'notification.success',
    defaultMessage: 'Success'
  },
  error: {
    id: 'notification.error',
    defaultMessage: 'Error'
  },
  info: {
    id: 'notification.info',
    defaultMessage: 'Error'
  },
  warning: {
    id: 'notification.warning',
    defaultMessage: 'Warning'
  },
  get_data_error: {
    id: 'notification.get_data_error',
    defaultMessage: 'Get data error. Please try again later'
  },
  something_error_request: {
    id: 'notification.something_error_request',
    defaultMessage: 'Something error when send request. Please try again later'
  }
});
