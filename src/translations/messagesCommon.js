/*
 * Users Container Messages
 *
 * This contains all the text for the Users Container component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  index: {
    id: 'commons.index',
    defaultMessage: 'No'
  },
  company_setting: {
    id: 'components.Company.company_setting',
    defaultMessage: 'Company Setting'
  },
  profile_setting: {
    id: 'commons.profile_setting',
    defaultMessage: 'Profile Setting'
  },
  user_profile: {
    id: 'commons.user_profile',
    defaultMessage: 'User Profile'
  },
  user_setting: {
    id: 'commons.user_setting',
    defaultMessage: 'User Setting'
  },
  email: {
    id: 'commons.email',
    defaultMessage: 'Email'
  },
  email_address: {
    id: 'commons.email_address',
    defaultMessage: 'Email Address'
  },
  old_password: {
    id: 'commons.old_password',
    defaultMessage: 'Old Password'
  },
  new_password: {
    id: 'commons.new_password',
    defaultMessage: 'New Password'
  },
  password_confirmation: {
    id: 'commons.password_confirmation',
    defaultMessage: 'Confirm Password'
  },
  url: {
    id: 'commons.url',
    defaultMessage: 'URL'
  },
  note: {
    id: 'commons.note',
    defaultMessage: 'Note'
  },
  save: {
    id: 'commons.save',
    defaultMessage: 'Save'
  },
  edit: {
    id: 'commons.edit',
    defaultMessage: 'Edit'
  },
  cancel: {
    id: 'commons.cancel',
    defaultMessage: 'Cancel'
  },
  users: {
    id: 'commons.users',
    defaultMessage: 'Users'
  },
  create: {
    id: 'commons.create',
    defaultMessage: 'Create'
  },
  user_name: {
    id: 'commons.user_name',
    defaultMessage: 'User Name'
  },
  password: {
    id: 'commons.password',
    defaultMessage: 'Password'
  },
  password_confirm: {
    id: 'commons.password_confirm',
    defaultMessage: 'Password confirm'
  },
  password_not_match: {
    id: 'commons.password_not_match',
    defaultMessage: 'Your password does not match. Please try again'
  },
  password_not_valid: {
    id: 'commons.password_not_valid',
    defaultMessage: 'Your password is not valid'
  },
  phone: {
    id: 'commons.phone',
    defaultMessage: 'Phone'
  },
  user_first_name: {
    id: 'commons.user_first_name',
    defaultMessage: 'First Name'
  },
  user_last_name: {
    id: 'commons.user_last_name',
    defaultMessage: 'Last Name'
  },
  language: {
    id: 'commons.language',
    defaultMessage: 'Language'
  },
  tel: {
    id: 'commons.tel',
    defaultMessage: 'Tel'
  },
  time_zone: {
    id: 'commons.time_zone',
    defaultMessage: 'Time Zone'
  },
  company: {
    id: 'components.Company.company',
    defaultMessage: 'Company'
  },
  home: {
    id: 'commons.breadcrumb.home',
    defaultMessage: 'Home'
  }
});
