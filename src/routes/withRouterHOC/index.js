import React from 'react';
import PropTypes from 'prop-types';
import Cookie from 'js-cookie';
import { ROLE_TYPE } from 'utils/constants';

function withRouterHOC(WrappedComponent, role) {
  return class WithRouterHOC extends React.Component {
    static propTypes = {
      history: PropTypes.object
    };

    componentDidMount() {
      const { history } = this.props;
      const micro_role = Cookie.get('micro_role');

      if (micro_role === undefined) {
        history.push('/login');
        return;
      }

      if (history.location.pathname === '/' && micro_role === ROLE_TYPE.OWNER) {
        history.push('/owner');
        return;
      }

      if (micro_role !== role) {
        history.push('/page-404');
      }
    }

    render() {
      return <WrappedComponent {...this.props}/>;
    }
  };
}

export default withRouterHOC;
