import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Author from 'containers/Author';
import HomeWrapper from './AdminRouter';
import PageNotFound from 'components/NotFound';

export default class AdminRoutes extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/login" component={Author}/>
        <Route exact path="/forgot-password" component={Author}/>
        <Route exact path="/reset-password" component={Author}/>
        <Route exact path="/page-404" component={PageNotFound}/>
        <Route path="/" component={HomeWrapper}/>
      </Switch>
    );
  }
}
