import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet';
import { Redirect, Route, Router, Switch } from 'react-router-dom';

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messages from 'containers/Users/translation';
import OwnerCompany from 'containers/Owner/Company';
import OwnerProfile from 'containers/Owner/Profile';
import OwnerLayout from 'layouts/OwnerLayout';
import OwnerCompanyDetail from 'containers/Owner/CompanyDetail';
import OwnerCompanyCreate from 'containers/Owner/CompanyCreate';
import OwnerDrones from 'containers/Owner/Drones';
import DroneDetail from 'containers/Owner/DroneDetail';
import UserList from 'containers/Owner/UserList';
import UserDetail from 'containers/Owner/UserDetail';
import OwnerMissions from 'containers/Owner/Missions';
import OwnerFlights from 'containers/Owner/Flights';
import OwnerLogs from 'containers/Owner/Logs';
import PaymentHistories from 'containers/Owner/PaymentHistories';
import DebitInvoices from 'containers/Owner/DebitInvoices';

function OwnerContent({ history }) {
  return (
    <OwnerLayout history={history}>
      <Helmet>
        <title>
          {intl.formatMessage(messages.user_page)}
        </title>
        <meta
          name="description"
          content="A React.js Boilerplate application homepage"
        />
      </Helmet>
      <Router history={history}>
        <Switch>
          <Route exact path='/owner' component={OwnerCompany}/>
          <Route exact path='/owner/user-profile' component={OwnerProfile}/>
          <Route exact path='/owner/companies' component={OwnerCompany}/>
          <Route exact path='/owner/companies/create' component={OwnerCompanyCreate}/>
          <Route exact path='/owner/companies/:id' component={OwnerCompanyDetail}/>
          <Route exact path='/owner/companies/:id/users' component={UserList}/>
          <Route exact path='/owner/companies/:id/users/:userId' component={UserDetail}/>
          <Route exact path='/owner/companies/:id/drones' component={OwnerDrones}/>
          <Route exact path='/owner/companies/:id/drones/:droneId' component={DroneDetail}/>
          <Route exact path='/owner/companies/:id/missions' component={OwnerMissions}/>
          <Route exact path='/owner/companies/:id/logs/:logId' component={OwnerLogs}/>
          <Route exact path='/owner/companies/:id/flights' component={OwnerFlights}/>
          <Route exact path='/owner/companies/:id/flights/:missionId' component={OwnerFlights}/>
          <Route exact path='/owner/companies/:id/payment-histories' component={PaymentHistories}/>
          <Route exact path='/owner/companies/:id/debit-invoices' component={DebitInvoices}/>
          <Redirect to="/page-404"/>
        </Switch>
      </Router>
    </OwnerLayout>
  );
}

OwnerContent.propTypes = {
  history: PropTypes.object,
  match: PropTypes.object
};

export default OwnerContent;
