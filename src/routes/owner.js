import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import OwnerAuthor from 'containers/Owner/OwnerAuthor';
import PageNotFound from 'components/NotFound';
import OwnerContentRoutes from './OwnerRouter';

export default class AppRoutes extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/login" component={OwnerAuthor}/>
        <Route exact path="/forgot-password" component={OwnerAuthor}/>
        <Route exact path="/reset-password" component={OwnerAuthor}/>
        <Route exact path="/page-404" component={PageNotFound}/>
        <Route path="/" component={OwnerContentRoutes}/>
      </Switch>
    );
  }
}
