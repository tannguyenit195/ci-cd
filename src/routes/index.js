import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Author from 'containers/Author';
import PageNotFound from 'components/NotFound';
import OwnerRouter from 'routes/OwnerRouter';
import AdminRouter from 'routes/AdminRouter';
import withRouterHOC from 'routes/withRouterHOC';
import { ROLE_TYPE } from 'utils/constants';

export default class AppRoutes extends Component {
  render() {
    const OWNER = withRouterHOC(OwnerRouter, ROLE_TYPE.OWNER);
    const ADMIN = withRouterHOC(AdminRouter, ROLE_TYPE.ADMIN);

    return (
      <Switch>
        <Route exact path="/login" component={Author}/>
        <Route exact path="/forgot-password" component={Author}/>
        <Route exact path="/reset-password" component={Author}/>
        <Route exact path="/page-404" component={PageNotFound}/>
        <Route path="/owner" component={OWNER}/>
        <Route path="/" component={ADMIN}/>
      </Switch>
    );
  }
}
