import React from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import { Route, Switch, Redirect } from 'react-router-dom';

import { intl } from 'containers/LanguageProvider/IntlGlobalProvider';
import messages from 'containers/Users/translation';
import Home from 'containers/Home';
import Company from 'containers/Company';
import Users from 'containers/Users';
import UserSetting from 'containers/UserSetting';
import MainLayout from 'layouts/MainLayout';
import UserCreate from 'containers/UserCreate';

AdminRouter.propTypes = {
  history: PropTypes.object
};

export default function AdminRouter(props) {
  return (
    <MainLayout history={props.history}>
      <Helmet>
        <title>
          {intl.formatMessage(messages.user_page)}
        </title>
        <meta
          name="description"
          content="A React.js Boilerplate application homepage"
        />
      </Helmet>
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/users" component={Users}/>
        <Route exact path="/users-create" component={UserCreate}/>
        <Route exact path="/company" component={Company}/>
        <Route exact path="/user-profile" component={UserSetting}/>
        <Route exact path="/global-setting" render={() => <div>Global setting</div>}/>
        <Redirect to="/page-404"/>
      </Switch>
    </MainLayout>
  );
}
