import jwtDecode from 'jwt-decode';

const checkTokenExpirationMiddleware = () => next => action => {
  const token = localStorage.getItem('token');
  if (!!token && jwtDecode(token).exp < Date.now() / 1000) {
    next(action);
    localStorage.clear();
  }
  next(action);
};

export default checkTokenExpirationMiddleware;
