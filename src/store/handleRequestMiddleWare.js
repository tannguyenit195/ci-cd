import { toggleFetching, requestHasError } from 'containers/App/action';

const handleRequestMiddleware = store => next => action => {
  const { type, payload } = action;
  if (type.indexOf('_REQUEST') > 0) {
    store.dispatch(toggleFetching(true));
  }

  if (type.indexOf('_SUCCESS') > 0 || type.indexOf('_FAILED') > 0) {
    store.dispatch(toggleFetching(false));
  }

  if (type.indexOf('_FAILED') > 0) {
    store.dispatch(requestHasError(payload.error));
  }

  return next(action);
};

export default handleRequestMiddleware;
