'use strict'

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const merge = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const baseWebpackConfig = require('./webpack.base.conf');

const webpackConfig = merge(baseWebpackConfig, {
  name: 'owner-page',
  entry: './src/owner.js',
  devServer: {
    hot: true,
    watchOptions: {
      poll: true
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'アプリで自宅の外壁点検 テラリフォーム(Terra Reform)｜テラドローン株式会社',
      filename: 'index.html',
      template: 'src/index.html',
      inject: true,
    }),
    new webpack.HotModuleReplacementPlugin(),
  ],
  output: {
    path: path.resolve(__dirname, '../dist/owner'),
  },
  devtool: 'eval-source-map',
  externals: {
    'createjs': 'createjs'
  }
});

if (process.env.NODE_ENV === 'production') {
  console.log('PROD BUILD');

  webpackConfig.plugins.push(
    new MiniCssExtractPlugin({
      filename: 'css/[name].[hash].css',
      chunkFilename: '[id].[hash].css'
    })
  );

  webpackConfig.output.filename = 'js/[name].[hash].js';

  // Compression Gzip
  const CompressionWebpackPlugin = require('compression-webpack-plugin');
  webpackConfig.plugins.push(
    new CompressionWebpackPlugin({
      // asset: '[path]',
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$|\.svg$/,
      threshold: 10240,
      minRatio: 0.8
    })
  );
} else {
  webpackConfig.plugins.push(
    new MiniCssExtractPlugin({
      filename: 'css/[name].css',
      chunkFilename: '[id].css'
    })
  );

  webpackConfig.output.filename = 'js/[name].js';
}

module.exports = webpackConfig
